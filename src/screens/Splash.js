
import React from 'react';
import { colors,urls } from '../Globals';
import { Button } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import { addUser } from '../actions/actions';

 class Splash extends React.Component {

	constructor(props) {
		super(props);
	}

  home = async () =>{

    await AsyncStorage.multiGet(["user_id", "name","image","email","investor","entrepreneur"]).then(response => {
      console.log(response[0][0]) // Key1
      console.log(response[0][1]) // Value1
      console.log(response[1][0]) // Key2
      console.log("Investor",response[4][1]) // Value2

      let user_id = response[0][1]
      let name = response[1][1]
      let image = response[2][1]
      let email = response[3][1]
      let investor = response[4][1]
      let entrepreneur = response[5][1]

        this.props.add({ user_id, name, image, email,investor,entrepreneur });
  })

   
   this.props.navigation.navigate('Home')
  }


xyz = async () =>{
 
 
            AsyncStorage.getItem('user_id')
            .then((value) => {
              if(value){
                  this.home()
              }

              else{
                this.props.navigation.navigate('Login')

              }
            });


}




check = async () =>{
   setTimeout(() => {
           
      this.xyz()
      // this.props.navigation.navigate('HomePage')


        }, 1000)


  }

  componentDidMount() {
    StatusBar.setBackgroundColor(colors.status_bar_color)
    this.check()
}

   
    

	render() {
	
        const d = Dimensions.get("window")
        return (
            <ImageBackground
            source={require('../assets/splash-new.jpg')}
            resizeMode='stretch'
            style={{ position: 'absolute',
                flex: 1,
                backgroundColor:'rgba(0,0,0,0.45)',
                width: d.width,
                height: d.height,zIndex:-2}}>
  
           <View style={styles.container}>

           {/**
  
           <Image style={{height:'30%',width:240,marginTop:-200}}
           source={require('../assets/logo.png')}
           resizeMode='contain'
           ></Image>

            */}


            <View style={{height:'30%',width:240,marginTop:-200}}></View>





         
           </View>
           
           </ImageBackground>

          )
	}
}

const mapDispatchToProps = dispatch => {
  return {
    // dispatching plain actions
    add: (userinfo) => dispatch(addUser(userinfo)),
  
    
  }
}


export default connect(null, mapDispatchToProps)(Splash);

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent:'center',
      alignItems:'center'
    },
    buttonLargeContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height:40
      },
    primaryButton: {
        backgroundColor: '#FF0017',
        height:45
      },
    buttonText: {
        color: 'white',
        fontSize: 14,
        fontWeight: 'bold'
      }

  });