import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { Dropdown } from 'react-native-material-dropdown';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import DateTimePicker from 'react-native-modal-datetime-picker';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback,Keyboard
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import Video from 'react-native-video';
//import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import { throwStatement } from '@babel/types';
import ChatTextInput from '../components/ChatTextInput';

import FastImage from 'react-native-fast-image'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


 class PostImageDetail extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      smiley_status:false,
      item:null,
      loading_status:false,

       comment:'',
       make_comment:'',
       bottom:0,
       comment_list:[],
       image_width:0,
       image_height:'',
       userId:'',
     
    };


  }

  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };
  
  hideMenu = () => {
    this._menu.hide();
  };
  
  showMenu = () => {
    this._menu.show();
  };
  

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'black',
    headerStyle: {
      backgroundColor: 'black',
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
      
            
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                 <TouchableOpacity onPress={()=>{  navigation.navigate('Home');
                
                const resetAction = StackActions.reset({
                index: 0,
                key: 'HomePage',
                actions: [NavigationActions.navigate({ routeName: 'Home' })],
              });
  
          navigation.dispatch(resetAction); }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

          
     </View>
    ),
  });




  showReaction(){
    
  }

  hideReaction(){
             
  }

  next(){
    
  }


  myPostDelete=(item)=>{
    this._menu.hide();
    var formData = new FormData();
    formData.append('post_id',item.id);
    
  
                       fetch(urls.base_url +'api/delete_post', {
                       method: 'POST',
                       headers: {
                         'Accept': 'application/json',
                         'Content-Type': 'multipart/form-data',
                       },
                      body: formData
                     }).then((response) => response.json())
                           .then((responseJson) => {
                 
                    console.log('delete-->>',responseJson)
                    if(!responseJson.error){
                      console.log("hi")
                      this.props.navigation.navigate('Home');//Home MyProfile EditPost
                                    }
                  
                           }).catch((error) => {
                              console.log(error)
                           
                                  });
  }
  
  myPostEdit=(item)=>{
    this._menu.hide();
    var data={'post':item}
   
    this.props.navigation.navigate('EditPost',{result:data})
  
   
  }
   
  getComment(){

     AsyncStorage.getItem("user_id").then((item) => {
       if (item) {
   this.setState({userId:item})
         var formData = new FormData();
         formData.append('post_id',this.state.item.id);
       
                       let url = urls.base_url +'api/get_post_comments'
                       fetch(url, {
                       method: 'POST',
                       headers: {
                         'Accept': 'application/json',
                         'Content-Type': 'multipart/form-data',
                       },
                   body: formData
   
                     }).then((response) => response.json())
                           .then((responseJson) => {
                     this.setState({loading_status:false ,comment:'',make_comment:''})
 
   
   
                               if(!responseJson.error){

                                 this.setState({comment_list:responseJson.data})
                             }
                             else{
   
                              //  Snackbar.show({
                              //    title: responseJson.message,
                              //    duration: Snackbar.LENGTH_SHORT,
                              //    backgroundColor:'black',
                              //    color:'red',
                              //    action: {
                              //      title: 'OK',
                              //      color: 'green',
                              //      onPress: () => { /* Do something. */ },
                              //    },
                              //  });
                                 
                             }
                           }).catch((error) => {
                             this.setState({loading_status:false})
                             
                             Snackbar.show({
                               title: error.message,
                               duration: Snackbar.LENGTH_SHORT,
                               backgroundColor:'black',
                               color:'red',
                               action: {
                                 title: 'OK',
                                 color: 'green',
                                 onPress: () => { /* Do something. */ },
                               },
                             });
   
   
                           });
       }
       else{
         ToastAndroid.show("User not found !",ToastAndroid.LONG)
       }
     });
    
   }

  makeComment(){
    if(this.state.comment.toString().trim().length > 0){
     
     Keyboard.dismiss()
 
     this.setState({make_comment:this.state.comment,comment:''})
 
     AsyncStorage.getItem("user_id").then((item) => {
       if (item) {
   
        Keyboard.dismiss()
     
        this.setState({comment:''})
   
         var formData = new FormData();
   
         formData.append('user_id',item);
         formData.append('post_id',this.state.item.id);
         formData.append('comment',this.state.make_comment);
 
         
   
                       this.setState({loading_status:true})
                       let url = urls.base_url +'api/comment_post'
                       fetch(url, {
                       method: 'POST',
                       headers: {
                         'Accept': 'application/json',
                         'Content-Type': 'multipart/form-data',
                       },
                   body: formData
   
                     }).then((response) => response.json())
                           .then((responseJson) => {
                     this.setState({loading_status:false ,comment:'',make_comment:''})
 
                     console.log('Comment',JSON.stringify(responseJson))
 
                   if(responseJson.error == false){
 
                   }
            
                               if(!responseJson.error){

                                this.setState({
                                  comment_list: [...this.state.comment_list, responseJson.comments]
                                });
   
                                //  Snackbar.show({
                                //   title: responseJson.message,
                                //   duration: Snackbar.LENGTH_SHORT,
                                //   backgroundColor:'black',
                                //   color:'red',
                                //   action: {
                                //     title: 'OK',
                                //     color: 'green',
                                //     onPress: () => { /* Do something. */ },
                                //   },
                                // });
                            
                             }
                             else{
   
                               Snackbar.show({
                                 title: responseJson.message,
                                 duration: Snackbar.LENGTH_SHORT,
                                 backgroundColor:'black',
                                 color:'red',
                                 action: {
                                   title: 'OK',
                                   color: 'green',
                                   onPress: () => { /* Do something. */ },
                                 },
                               });
                                 
                             }
                           }).catch((error) => {
                             this.setState({loading_status:false})
                             
                             Snackbar.show({
                               title: error.message,
                               duration: Snackbar.LENGTH_SHORT,
                               backgroundColor:'black',
                               color:'red',
                               action: {
                                 title: 'OK',
                                 color: 'green',
                                 onPress: () => { /* Do something. */ },
                               },
                             });
   
   
                           });
       }
       else{
         ToastAndroid.show("User not found !",ToastAndroid.LONG)
       }
     });
    }else{
      return
    }
   }


  likePosts(post_id,like_id){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
        var formData = new FormData();

        formData.append('user_id',item);
        formData.append('post_id',post_id);
        formData.append('like_type',like_id);
        

                      this.setState({loading_status:true,smiley_status:false})
                      let url = urls.base_url +'api/like_post'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                              console.log(JSON.stringify(responseJson))
                              if(!responseJson.error){

                                Platform.OS === 'android' 
                                ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                : Alert.alert(responseJson.message)

                                var com = {...this.state.item}
                                 com.like_flag = like_id

                                 var like_update = responseJson.like_update
                                 if(like_update == 0){
                                   //means first time like the post so increse count
                                   var com = {...this.state.item}
                                   com.likes_count  = com.likes_count  + 1
                                 }
                  
                                  this.setState({item:com})


                     
                            }
                            else{

                             
                               

                        Snackbar.show({
                          title: responseJson.message,
                          duration: Snackbar.LENGTH_SHORT,
                          backgroundColor:'black',
                          color:'red',
                          action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                          },
                        });
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                           
                        Snackbar.show({
                          title: 'Try Again !',
                          duration: Snackbar.LENGTH_SHORT,
                          backgroundColor:'black',
                          color:'red',
                          action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                          },
                        });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }


  checkBuyPackagesChat(user){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {


       var formData = new FormData();

        formData.append('from_id',item);
        formData.append('to_id',user.id);

       

                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/check_chat_status'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                           

                              let obj={
                                'to_id':user.id,
                                'to_name':user.name,
                                'to_image':user.user_image,
                                'to_status':'offline',
                                'to_typing':false
                                
                              }
                              this.props.navigation.navigate("ChatOutside",{result:obj})
                              

                                 
                              }


                           
                               else{

                                   
                                Snackbar.show({
                                  title: responseJson.message,
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });



                                 if(responseJson.request_status == ''){

                                  
                               if(responseJson.to_buy == 0){
                                      //means samne wale ne chat ka package nahi kharida hai
                               }
                                else{
                                    let obj={
                                      'to_id':user.id,
                                      'to_name':user.name,
                                      'to_image':user.user_image,
                                      'to_status':'offline'
                                    }
                                    this.props.navigation.navigate("ChatPackages",{result:obj})

                               }
                             

                                 
                                 }
                                 else{
                                  
                                 }
                             
                            
                                }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                          
                            Snackbar.show({
                              title: error.message,
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }


  chatStatus(user){

 
    if(user.seller_flag == 1){
   

    this.checkBuyPackagesChat(user)

    }
    else{
      Snackbar.show({
        title: 'This User Is Not Seller',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
      });
    }
    
  }


    
  renderItem = ({item, index}) => {
    return(
      <View style={{width:'90%'}}>
     

          <View style={{height:null ,margin:1,
          padding:10,flexDirection:'row',justifyContent:'flex-start',alignItems:'center'}}>

          <Image source={{uri:urls.base_url + item.user.user_image}} 
          style={{width:30,height:30,marginRight:10,borderRadius:25,overflow:'hidden'}}/>

                <View style={{backgroundColor:'#CDCDCD'
                ,paddingRight:12,paddingTop:5,paddingLeft:12,
                paddingBottom:5,borderRadius:20,width:item.comment.length > 10 ? '80%' : '60%'}}>

                  
                  <Text style={{fontWeight:'bold'}}>{item.user.name}</Text>
                  <Text style={{fontSize:12,margin:3}}>{item.comment} </Text>


                  
                <TouchableOpacity onPress={()=> {
                  if (item.user.id == this.props.user.user_id) {
                          Snackbar.show({
                                         title: "You can't chat with yourself",
                                         duration: Snackbar.LENGTH_SHORT,
                                         backgroundColor:'black',
                                         color:'red',
                                         action: {
                                           title: 'OK',
                                           color: 'green',
                                           onPress: () => { /* Do something. */ },
                                         },
                                       });
                  }
                  else{
                    this.chatStatus(item.user)
                  }
                }}
                 style={{position:'absolute',top:10,right:item.comment.length > 10 ? 5 : 5}}>
                 <Image source={require('../assets/chatcomment.png')}
                 style={{width:20,height:20}} resizeMode='contain'/>
                 </TouchableOpacity>
                </View>





         </View>


       

         {/* 
         <Text 
         onPress={()=> this.commentBox.focus()}
         style={{marginTop:-15,alignSelf:'flex-end',
         textDecorationLine: 'underline',fontWeight: 'bold',
         fontStyle: 'italic'}}>Reply</Text>

         */}

          
      </View>
    )
   }

   

  _keyboardDidShow = () => {
    // alert('Keyboard Shown');
     this.setState({bottom:Platform.OS == 'android'? 0 :
       Dimensions.get('window').height * 0.38})
 
 
   }
 
   _keyboardDidHide = () => {
     //alert('Keyboard Hidden');
     this.setState({bottom:0})
   }
 



 _checkCondition(){
    let img;
    const {item} = this.state
    if(item.like_flag == 1){
        img = require('../assets/p1.png')
    }else if (item.like_flag == 2){
      img = require('../assets/p2.png')
    }else if (item.like_flag == 3){
      img = require('../assets/p3.png')
    }else  if (item.like_flag == 4){
      img = require('../assets/p4.png')
    }
    else  if (item.like_flag == 5){
      img = require('../assets/p5.png')
    }
    else{
      img = require('../assets/p1.png')
    }

    
    return img;   
};








  componentWillMount(){

    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
    this.setState({userId:item})
      }})

    this.getComment()
    let result = this.props.navigation.getParam('result')
    let item =  result['item']
    this.setState(prevState => ({
      item: result['item']
    })); 

    if(item.media_type == 1){
      Image.getSize(urls.base_url+ item.post_img, (width, height) => {
        console.log(`The image dimensions are ${width}x${height}`);
           var h_percentage = (windowWidth*100)/width
           var h_img = (height*h_percentage)/100
           this.setState({image_width:width, image_height:h_img})
        // this.setState({image_width:width, image_height:height})
      }, (error) => {
        console.log(`Couldn't get the image size: ${error.message}`);
        //this.setState({image_width:0, image_height:0})
      });
    } 

    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );

  
  }

  componentWillUnmount(){
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }


    render()
    {

      const {item} = this.state
  
      
        return(
          <SafeAreaView style={styles.container}>
    
            <ScrollView style={{backgroundColor:'black',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

          <View style={{ width: '100%',
           justifyContent: 'center', alignSelf: 'center' }}>

          <View style={{flexDirection:'row',
            justifyContent:'flex-start',alignItems:'center',width:'100%'}}>
    
                 <Image source={{uri:urls.base_url+item.user_pic}}
                    style={{height:40,width:40,
                    overflow:'hidden',borderRadius:20,margin:10}}  />
    
                    <View style={{marginLeft:8}}>
                    <Text style={{marginBottom:5,fontWeight:'bold',color:'white'}}>{item.user_name}</Text>
                    
                    </View>
     
{
      this.state.userId==item.user_id 
      ?
    <View style={{zIndex:999,position:'absolute',right:10,top:3}}>
    <Menu
    ref={this.setMenuRef}
    button={
    <TouchableOpacity onPress={this.showMenu} style={{width:30,height:30,top:7}}>
    <Image source={require('../assets/threeDotWhite.png')}
                    style={[styles.menuImage,{alignSelf:'center'}]} resizeMode='contain'/>
    </TouchableOpacity>
    } >
    <MenuItem onPress={ ()=>{
              Alert.alert(
              "Alert", 
              'Are you sure want to delete ?',
              [
                { text: "OK", onPress: this.myPostDelete(item)},
                { text: "CANCEL", onPress: () => console.log("cancel Pressed") }
              ],
              { cancelable: false } 
               ) }
            } >Delete</MenuItem>
    <MenuItem onPress={() => {
         this.myPostEdit(item)
    }
        }>Edit</MenuItem> 
    </Menu>
    </View>
      : null 
     } 

        
            </View> 


              <Text style={{margin:15,color:'white',alignSelf:'center'
                }}>{item.description}</Text>

                <View style={{height:  item.media_type == '2'? Dimensions.get('window').height * 0.45 : this.state.image_height - 0}}>
                {
                  item.media_type == '2'
                    ?
                    <Video source={{uri:urls.base_url + item.post_video}}
                     ref={(ref) => {
                            this.player = ref
                          }} 
                          onLoad={response => {
                          
                            const { width, height } = response.naturalSize;
                            const heightScaled = height * (Dimensions.get('window').width / width);
                            console.log('heightScaled',heightScaled)
                            console.log('response.naturalSize',response.naturalSize)
                          }}
                        resizeMode='cover'
                        controls={true}
                        paused={true}
                        onError={e => console.log(e)}
                       
                        onBuffer={()=> {}}   
                        style={{width:'100%',height:'100%',backgroundColor:'grey'}}/>
                    : 

                  
                    <FastImage source={{uri:urls.base_url+ item.post_img}}
                     resizeMode={FastImage.resizeMode.stretch}
                      style={{alignSelf:'center',height:'99%',width:Dimensions.get('window').width*0.98,
                      overflow:'hidden'}
                    }  
                      />
                   
                   
                  } 
                  <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>

                  <TouchableOpacity activeOpacity={0.6}
                  style={{flexDirection:'row',alignItems:'center',width:50,marginBottom:25}}
                 // onPress={()=> this.likePosts(this.props.posts[this.props.index].id,1)}
                  onLongPress={()=> {
                    //console.log("sdfhsdbfjhdsfbjhdsfbdhjfdhj")
                  this.setState({smiley_status:true})
                  }}
                  onPress={()=>  this.likePosts(item.id,1)}
                  >
  
                  <Image source={this._checkCondition()}
                      style={{height:25,width:25,
                      overflow:'hidden',borderRadius:12,margin:10}}  />
      
                      <Text style={{color:'white'}}>{item.likes_count} likes</Text>
      
               </TouchableOpacity>

               <Text onPress={()=>{
                 let obj ={ 'post_id' : this.state.item.id}
                 this.props.navigation.navigate('PeopleReacted',{result:obj})
               }}
               style={{color:colors.color_primary,fontWeight:'bold',fontSize:17,marginBottom:25}}>See Who Reacted   </Text>

               </View>


                

               {
                this.state.smiley_status
                ?
                <View style={{position:'absolute',
                bottom:10,left:10,
                backgroundColor:'white',
                width:null,flexDirection:'row',
                 alignItems:'center',borderRadius:20,
                padding:5,borderWidth:1}}>
  

                <TouchableOpacity  onPress={()=> {
                 this.likePosts(item.id,1)
                }}>
                <View style={{justifyContent:'center',alignItems:'center'}}>
                <Image source={require('../assets/p1.png')}
                style={{width:40,height:40}} resizeMode='contain'/>
                  <Text>{item.likes_thumb}</Text>
                </View>
                </TouchableOpacity>
              
                <TouchableOpacity onPress={()=> {
                   this.likePosts(item.id,2)
                //  this.setState({posts:  this.state.posts})
                }}>
                <View style={{justifyContent:'center',alignItems:'center',marginLeft:5}}>
              <Image source={require('../assets/p2.png')}
                style={{width:40,height:40}} resizeMode='contain'/>
                <Text>{item.likes_cool}</Text>

                </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=> {
                  this.likePosts(item.id,3)
                }}>
                <View style={{justifyContent:'center',alignItems:'center',marginLeft:5}}>
               <Image source={require('../assets/p3.png')}
                style={{width:40,height:40}} resizeMode='contain'/>
                <Text>{item.likes_awesome}</Text>

                </View>
                </TouchableOpacity>


                <TouchableOpacity onPress={()=> {
                  
                  this.likePosts(item.id,4)
                 
                }}>
                <View style={{justifyContent:'center',alignItems:'center',marginLeft:5}}>
               <Image source={require('../assets/p4.png')}
                style={{width:40,height:40}} resizeMode='contain'/>
                <Text>{item.likes_other}</Text>
                </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=> {
                  
                  this.likePosts(item.id,5)
                 
                }}>
                <View style={{justifyContent:'center',alignItems:'center',marginLeft:5}}>
               <Image source={require('../assets/p5.png')}
                style={{width:40,height:40}} resizeMode='contain'/>
                <Text>{item.likes_fantastic}</Text>

                </View>
                </TouchableOpacity>


                </View>
                : null 
              }

                </View>

                {
                  this.state.comment_list.length == 0
                  ? null 
                  : 
                  <View style={{paddingBottom:40, width:Dimensions.get('window').width * 0.99,marginTop:50}}>
                  <Text style={{margin:10,color:'white',fontWeight:'bold',fontSize:17}}>Comments</Text>
                  <FlatList
                   style={{marginBottom:10,width:Dimensions.get('window').width * 0.9,padding:4}}
                    data={this.state.comment_list}
                   showsVerticalScrollIndicator={false}
                   renderItem={(item,index) => this.renderItem(item,index)}
                 />
                 </View>
                }
    
            </View>
            <View style={{width:100,height:120}}></View>
            </ScrollView>

             {/* comment section */}
                     
             <View style={{position:'absolute',bottom:this.state.bottom,right:0,
             left:0,height:60,backgroundColor:'white',elevation:10,shadowOpacity: 5,paddingRight:10,
             paddingLeft:10,paddingTop:7,paddingBottom:7}}>

                  <View style={{flexDirection:'row',alignItems:'center',
                  flex:4,borderColor:'black',borderWidth:0.4,marginLeft:4,marginRight:2,borderRadius:20,backgroundColor:'#F2F2F2'}}>


                  <View style={{flex:2.5}}>
                  <TextInput
                  value={this.state.comment}
                  onSubmitEditing={() => {this.makeComment()}}
                  multiline={true}
                  ref={(input) => { this.commentBox = input; }}
                  placeholder={'Write Comment Here..'}
                  placeholderTextColor={colors.color_primary}
                  onFocus={()=> {}}
                  onChangeText={(comment) => {
                    // const regex = /\uD83C\uDFF4(?:\uDB40\uDC67\uDB40\uDC62(?:\uDB40\uDC65\uDB40\uDC6E\uDB40\uDC67|\uDB40\uDC77\uDB40\uDC6C\uDB40\uDC73|\uDB40\uDC73\uDB40\uDC63\uDB40\uDC74)\uDB40\uDC7F|\u200D\u2620\uFE0F)|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC68(?:\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D)?\uD83D\uDC68|(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3]))|\uD83D\uDC69\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D(?:\uD83D[\uDC68\uDC69])|\uD83D[\uDC68\uDC69])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66\u200D\uD83D\uDC66|(?:\uD83D\uDC41\uFE0F\u200D\uD83D\uDDE8|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\uD83D\uDC68(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\u200D[\u2695\u2696\u2708])|(?:(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)\uFE0F|\uD83D\uDC6F|\uD83E[\uDD3C\uDDDE\uDDDF])\u200D[\u2640\u2642]|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|\u200D[\u2640\u2642])|\uD83D\uDC69\u200D[\u2695\u2696\u2708])\uFE0F|\uD83D\uDC69\u200D\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC68(?:\u200D(?:(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D[\uDC66\uDC67])|\uD83C[\uDFFB-\uDFFF])|\uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08|\uD83D\uDC69\u200D\uD83D\uDC67|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66|\uD83C\uDDF6\uD83C\uDDE6|\uD83C\uDDFD\uD83C\uDDF0|\uD83C\uDDF4\uD83C\uDDF2|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])|\uD83C\uDDED(?:\uD83C[\uDDF0\uDDF2\uDDF3\uDDF7\uDDF9\uDDFA])|\uD83C\uDDEC(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEE\uDDF1-\uDDF3\uDDF5-\uDDFA\uDDFC\uDDFE])|\uD83C\uDDEA(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDED\uDDF7-\uDDFA])|\uD83C\uDDE8(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDEE\uDDF0-\uDDF5\uDDF7\uDDFA-\uDDFF])|\uD83C\uDDF2(?:\uD83C[\uDDE6\uDDE8-\uDDED\uDDF0-\uDDFF])|\uD83C\uDDF3(?:\uD83C[\uDDE6\uDDE8\uDDEA-\uDDEC\uDDEE\uDDF1\uDDF4\uDDF5\uDDF7\uDDFA\uDDFF])|\uD83C\uDDFC(?:\uD83C[\uDDEB\uDDF8])|\uD83C\uDDFA(?:\uD83C[\uDDE6\uDDEC\uDDF2\uDDF3\uDDF8\uDDFE\uDDFF])|\uD83C\uDDF0(?:\uD83C[\uDDEA\uDDEC-\uDDEE\uDDF2\uDDF3\uDDF5\uDDF7\uDDFC\uDDFE\uDDFF])|\uD83C\uDDEF(?:\uD83C[\uDDEA\uDDF2\uDDF4\uDDF5])|\uD83C\uDDF8(?:\uD83C[\uDDE6-\uDDEA\uDDEC-\uDDF4\uDDF7-\uDDF9\uDDFB\uDDFD-\uDDFF])|\uD83C\uDDEE(?:\uD83C[\uDDE8-\uDDEA\uDDF1-\uDDF4\uDDF6-\uDDF9])|\uD83C\uDDFF(?:\uD83C[\uDDE6\uDDF2\uDDFC])|\uD83C\uDDEB(?:\uD83C[\uDDEE-\uDDF0\uDDF2\uDDF4\uDDF7])|\uD83C\uDDF5(?:\uD83C[\uDDE6\uDDEA-\uDDED\uDDF0-\uDDF3\uDDF7-\uDDF9\uDDFC\uDDFE])|\uD83C\uDDE9(?:\uD83C[\uDDEA\uDDEC\uDDEF\uDDF0\uDDF2\uDDF4\uDDFF])|\uD83C\uDDF9(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDED\uDDEF-\uDDF4\uDDF7\uDDF9\uDDFB\uDDFC\uDDFF])|\uD83C\uDDE7(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEF\uDDF1-\uDDF4\uDDF6-\uDDF9\uDDFB\uDDFC\uDDFE\uDDFF])|[#\*0-9]\uFE0F\u20E3|\uD83C\uDDF1(?:\uD83C[\uDDE6-\uDDE8\uDDEE\uDDF0\uDDF7-\uDDFB\uDDFE])|\uD83C\uDDE6(?:\uD83C[\uDDE8-\uDDEC\uDDEE\uDDF1\uDDF2\uDDF4\uDDF6-\uDDFA\uDDFC\uDDFD\uDDFF])|\uD83C\uDDF7(?:\uD83C[\uDDEA\uDDF4\uDDF8\uDDFA\uDDFC])|\uD83C\uDDFB(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDEE\uDDF3\uDDFA])|\uD83C\uDDFE(?:\uD83C[\uDDEA\uDDF9])|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u261D\u270A-\u270D]|\uD83C[\uDF85\uDFC2\uDFC7]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66\uDC67\uDC70\uDC72\uDC74-\uDC76\uDC78\uDC7C\uDC83\uDC85\uDCAA\uDD74\uDD7A\uDD90\uDD95\uDD96\uDE4C\uDE4F\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD30-\uDD36\uDDB5\uDDB6\uDDD1-\uDDD5])(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u231A\u231B\u23E9-\u23EC\u23F0\u23F3\u25FD\u25FE\u2614\u2615\u2648-\u2653\u267F\u2693\u26A1\u26AA\u26AB\u26BD\u26BE\u26C4\u26C5\u26CE\u26D4\u26EA\u26F2\u26F3\u26F5\u26FA\u26FD\u2705\u270A\u270B\u2728\u274C\u274E\u2753-\u2755\u2757\u2795-\u2797\u27B0\u27BF\u2B1B\u2B1C\u2B50\u2B55]|\uD83C[\uDC04\uDCCF\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE1A\uDE2F\uDE32-\uDE36\uDE38-\uDE3A\uDE50\uDE51\uDF00-\uDF20\uDF2D-\uDF35\uDF37-\uDF7C\uDF7E-\uDF93\uDFA0-\uDFCA\uDFCF-\uDFD3\uDFE0-\uDFF0\uDFF4\uDFF8-\uDFFF]|\uD83D[\uDC00-\uDC3E\uDC40\uDC42-\uDCFC\uDCFF-\uDD3D\uDD4B-\uDD4E\uDD50-\uDD67\uDD7A\uDD95\uDD96\uDDA4\uDDFB-\uDE4F\uDE80-\uDEC5\uDECC\uDED0-\uDED2\uDEEB\uDEEC\uDEF4-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])|(?:[#*0-9\xA9\xAE\u203C\u2049\u2122\u2139\u2194-\u2199\u21A9\u21AA\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA\u24C2\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE\u2600-\u2604\u260E\u2611\u2614\u2615\u2618\u261D\u2620\u2622\u2623\u2626\u262A\u262E\u262F\u2638-\u263A\u2640\u2642\u2648-\u2653\u265F\u2660\u2663\u2665\u2666\u2668\u267B\u267E\u267F\u2692-\u2697\u2699\u269B\u269C\u26A0\u26A1\u26AA\u26AB\u26B0\u26B1\u26BD\u26BE\u26C4\u26C5\u26C8\u26CE\u26CF\u26D1\u26D3\u26D4\u26E9\u26EA\u26F0-\u26F5\u26F7-\u26FA\u26FD\u2702\u2705\u2708-\u270D\u270F\u2712\u2714\u2716\u271D\u2721\u2728\u2733\u2734\u2744\u2747\u274C\u274E\u2753-\u2755\u2757\u2763\u2764\u2795-\u2797\u27A1\u27B0\u27BF\u2934\u2935\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55\u3030\u303D\u3297\u3299]|\uD83C[\uDC04\uDCCF\uDD70\uDD71\uDD7E\uDD7F\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE02\uDE1A\uDE2F\uDE32-\uDE3A\uDE50\uDE51\uDF00-\uDF21\uDF24-\uDF93\uDF96\uDF97\uDF99-\uDF9B\uDF9E-\uDFF0\uDFF3-\uDFF5\uDFF7-\uDFFF]|\uD83D[\uDC00-\uDCFD\uDCFF-\uDD3D\uDD49-\uDD4E\uDD50-\uDD67\uDD6F\uDD70\uDD73-\uDD7A\uDD87\uDD8A-\uDD8D\uDD90\uDD95\uDD96\uDDA4\uDDA5\uDDA8\uDDB1\uDDB2\uDDBC\uDDC2-\uDDC4\uDDD1-\uDDD3\uDDDC-\uDDDE\uDDE1\uDDE3\uDDE8\uDDEF\uDDF3\uDDFA-\uDE4F\uDE80-\uDEC5\uDECB-\uDED2\uDEE0-\uDEE5\uDEE9\uDEEB\uDEEC\uDEF0\uDEF3-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])\uFE0F|(?:[\u261D\u26F9\u270A-\u270D]|\uD83C[\uDF85\uDFC2-\uDFC4\uDFC7\uDFCA-\uDFCC]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66-\uDC69\uDC6E\uDC70-\uDC78\uDC7C\uDC81-\uDC83\uDC85-\uDC87\uDCAA\uDD74\uDD75\uDD7A\uDD90\uDD95\uDD96\uDE45-\uDE47\uDE4B-\uDE4F\uDEA3\uDEB4-\uDEB6\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD26\uDD30-\uDD39\uDD3D\uDD3E\uDDB5\uDDB6\uDDB8\uDDB9\uDDD1-\uDDDD])/g
                    //   var comme = comment.replace(regex, '')
                    // this.setState({ comment : comme })
                      this.setState({ comment : comment })
                  }
                  
                  }
                  style={{width:'100%',height:'100%',padding:10}}
                 // keyboardType={Platform.OS === 'ios' ? 'ascii-capable' : 'visible-password'}
                
                />

                  {/* onSubmitEditing={() => { this.commentBox.focus(); }} */}

                  </View>


                  <View style={{flex:0.5}}>
                    
                    <TouchableOpacity onPress={() =>{this.makeComment()}}>
                        <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/send-button-grey.png')} />
                        </TouchableOpacity>
    </View>






</View>
             
              </View>

              {/**commnets */}


             
    
            {this.state.loading_status &&
              <View style={[
                StyleSheet.absoluteFill,
                { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
              ]}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,

	};
};

const mapDispatchToProps = dispatch => {
  return {

    change: (userinfo) => dispatch(changeUser(userinfo)),
    enterpreneurUser: () => dispatch(enterpreneurUser()),


  }
}


export default connect(mapStateToProps, mapDispatchToProps)(PostImageDetail);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  },
  menuImage:{width:17,height:17,marginRight:6,flex:1},
  });