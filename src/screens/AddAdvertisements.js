import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import Video from 'react-native-video';
//import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import { Slider } from 'react-native-elements';
import HTML from 'react-native-render-html';
import stripe from 'tipsi-stripe'



import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import { throwStatement } from '@babel/types';
import { StackActions, NavigationActions} from 'react-navigation';
import RNThumbnail from 'react-native-thumbnail';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';



stripe.setOptions({
    publishableKey: 'pk_test_7BfmJjAx9dBFO2eGC4GGd1za',
   //sandox:pk_test_lH3nLCADTo6rIyxvns0P8igB008LHXo6qB
   //live : pk_live_j40fxUTzTRVqL0D8SWIIvdBy00NU75hBT2
  
  androidPayMode: 'test',
  //androidPayMode:'production'
  })

const htmlContent = `
    <h1>This HTML snippet is now rendered with native components !</h1>
    <h2>Enjoy a webview-free and blazing fast application</h2>
    <img src="https://i.imgur.com/dHLmxfO.jpg?2" />
    <em style="textAlign: center;">Look at how happy this native cat is</em>
`;
 
class AddAdvertisements extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      budget_value:5,
      duration_value:1,
      name:'',
      description:'',
      url:'',
      video:null,
      videoSource:null,
      videoThumbnail:null,
      image:null,
      imageSource:null
      
      
     
    };


  }

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
      
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                 <TouchableOpacity onPress={()=>{
                    navigation.goBack();
                  }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Advertise</Text>
     </View>
    ),
  });


  stripeApi(token_id,advertId,price){


    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
        var formData = new FormData();
        
        formData.append('user_id',item);
        formData.append('stripe_token',token_id);
          formData.append('advert_id',advertId)
          formData.append('price',price)
       
        
     
          this.setState({loading_status:true})


          let url = urls.base_url +'api/make_advert_payment'
        fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type':  'multipart/form-data',
        },
        body: formData

      }).then((response) => response.json())
            .then((responseJson) => {
              this.setState({loading_status:false})
           //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
         //  console.log(JSON.stringify(responseJson))

           Snackbar.show({
            title: responseJson.message,
            duration: Snackbar.LENGTH_SHORT,
            backgroundColor:'black',
            color:'red',
            action: {
              title: 'OK',
              color: 'green',
              onPress: () => { /* Do something. */ },
            },
          });


            if(!responseJson.error){
                this.props.navigation.navigate('HomePage');
                // const resetAction = StackActions.reset({
                //         index: 0,
                //         key: 'MyProfile',
                //         actions: [NavigationActions.navigate({ routeName: 'MyProfile' })],
                //   });

                //   this.props.navigation.dispatch(resetAction);

                    
                 

          }else{

            }


            }).catch((error) => {
             // ToastAndroid.show("Connection Error !", ToastAndroid.SHORT);
             
                      Snackbar.show({
                        title: 'Try Again',
                        duration: Snackbar.LENGTH_SHORT,
                        backgroundColor:'black',
                        color:'red',
                        action: {
                          title: 'OK',
                          color: 'green',
                          onPress: () => { /* Do something. */ },
                        },
                      });

            });
      }
      else{
        //ToastAndroid.show("User not found !",ToastAndroid.LONG)
        // Platform.OS === 'android'
        // ?  ToastAndroid.show("User not found !", ToastAndroid.SHORT)
        // : Alert.alert("User not found !")
      }
    });

    //  ToastAndroid.show(price+"...."+bid+"..."+pid+"...", ToastAndroid.SHORT);

    }

  async stripe(advertId,price){

    //console.log(JSON.stringify(item))
    const options = {
      requiredBillingAddressFields: 'full',
      prefilledInformation: {
        email:this.props.user.email,
        billingAddress: {
          name: this.props.user.name,
          line1: '',
          line2: '',
          city: '',
          state: '',
          country: '',
          postalCode: '',
        },
      },
    }

    const token = await stripe.paymentRequestWithCardForm(options)
  // ToastAndroid.show(JSON.stringify(token.tokenId),ToastAndroid.LONG)
    var cardId = token.card.cardId
    var tokenId = token.tokenId
    this.stripeApi(tokenId,advertId,price)
    //console.log("carddd",JSON.stringify(token))
  }


   validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator

    
    return !!pattern.test(str);
  }

  isValid(){
    const { name, description , imageSource,videoSource ,url} = this.state;

      let valid = false;

        if (name.toString().trim().length > 0 && 
        description.toString().trim().length > 0 && 
        description.toString().trim().length < 181 && 
       ( imageSource != null || videoSource !=null)
        ) {
              valid = true;
             
          }

          if(name.toString().trim().length == 0){
            Snackbar.show({
              title: "Enter Name",
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });
            return false;
          }

          else  if(description.toString().trim().length == 0){
            Snackbar.show({
              title: "Enter description",
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });


            return false;
          }
          else  if(description.toString().trim().length > 180){
            Snackbar.show({
              title: "Description should be less than 180 characters",
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });


            return false;
          }


          else  if(url.toString().trim().length == 0){
            Snackbar.show({
              title: "Enter url",
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });


            return false;
          }


          else  if(!(this.validURL(url))){
            Snackbar.show({
              title: "Enter valid url",
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });


            return false;
          }

          else  if(imageSource == null  && videoSource == null){

    
            Snackbar.show({
              title: "Enter Advertisement Media",
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });


           return false;
          }

        
        return valid

  }

 addAdvertisement(){
    if(this.isValid()){
     
 
     AsyncStorage.getItem("user_id").then((item) => {
       if (item) {
         var formData = new FormData();
 
         formData.append('user_id',item);
         formData.append('budget',parseInt(this.state.budget_value));
         formData.append('duration',parseInt(this.state.duration_value));
         formData.append('name',this.state.name);
         formData.append('description',this.state.description);
         formData.append('url',this.state.url);
         {
             this.state.video != null?
           formData.append('video',this.state.video)
             : null

         }
         {
            this.state.image != null?
            formData.append('img',this.state.image)
            : null

        }
        {
            this.state.video != null?
            formData.append('media_type',2)
            : formData.append('media_type',1)

        }

 
                       this.setState({loading_status:true})
                       let url = urls.base_url +'api/add_advertise'
                       fetch(url, {
                       method: 'POST',
                       headers: {
                         'Accept': 'application/json',
                         'Content-Type': 'multipart/form-data',
                       },
                   body: formData
 
                     }).then((response) => response.json())
                           .then((responseJson) => {
                   this.setState({loading_status:false})

               //    console.log('response----',responseJson)
                
             
              
 
              
                         if(!responseJson.error){

                          Snackbar.show({
                            title: "Advertisement added Successfully and will we posted after approval from admin",
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                          });
            

                            var advertId = responseJson.advert.id
                              var price =parseInt(this.state.budget_value) * parseInt(this.state.duration_value)
                              this.stripe(advertId,price)
                             } else {
                              Snackbar.show( {
                                title: responseJson.message,
                                duration: Snackbar.LENGTH_SHORT,
                                backgroundColor:'black',
                                color:'red',
                                action: {
                                  title: 'OK',
                                  color: 'green',
                                  onPress: () => { /* Do something. */ },
                                },
                              });
                
                              
                             }
                           }).catch((error) => {
                             this.setState({loading_status:false})
                 

                      Snackbar.show({
                        title: "Try Again !",
                        duration: Snackbar.LENGTH_SHORT,
                        backgroundColor:'black',
                        color:'red',
                        action: {
                          title: 'OK',
                          color: 'green',
                          onPress: () => { /* Do something. */ },
                        },
                      });
 
 
                           });
       }
       else{
         ToastAndroid.show("User not found !",ToastAndroid.LONG)
       }
     });
    }
  }
  
  addPhoto() {
    const options = {
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };
 
    //launchCamera
    //showImagePicker
    //launchImageLibrary
    ImagePicker.showImagePicker(options, (response) => {
     // console.log('Response = ', response);
 
      if (response.didCancel) {
       // console.log('User cancelled photo picker');
       // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
      }
      else if (response.error) {
       // console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
       // console.log('User tapped custom button: ', response.customButton);
      }
      else {
       //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
        let source = { uri: response.uri};

        var photo = {
          uri: response.uri,
          type:"image/jpeg",
          name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
        };
       

        this.setState({
          imageSource: source ,
          image:photo,
          video:null,
          videoSource:null,
          videoThumbnail:null
 
        }); 

        this.MediaSheetChoose.close()
        
   
       
      }
    });
 }

 addVideo() {
    const options = {
      title: 'Video Picker',
      mediaType: 'video',
      videoQuality: 'medium',
      durationLimit: 30,
      takePhotoButtonTitle: 'Take Video...',
      allowsEditing: true,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options,async (response) =>{

      if (response.didCancel) {
      }
      else if (response.error) {

       // ToastAndroid.show(JSON.stringify(response),ToastAndroid.LONG)
      }
      else if (response.customButton) {
       // console.log('User tapped custom button: ', response.customButton);
      }
      else {
       //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
        let source = { uri: response.uri};
        //console.log("VIDEO>>>>",JSON.stringify(response))

        // if(Platform.OS === 'android'){
        //     await RNThumbnail.get(response.path).then((result) => {
        //       this.setState({videoThumbnail:result.path})
        //     }).then(error =>   console.log("THUMBNAIL Error :",error))
    
        //   }


        var video={
          name: 'name.mp4',
          uri: source.uri,
          type: 'video/mp4'
        }



        this.setState({
            imageSource: null ,
            image:null,
            video:video,
            videoSource:source,
          
   
          }); 
          

        this.MediaSheetChoose.close();





      }
    });
 }

 addPhotoCamera() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  //launchCamera
  //showImagePicker
  //launchImageLibrary
  ImagePicker.launchCamera(options, (response) => {
   // console.log('Response = ', response);

    if (response.didCancel) {
    //  console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
    //  console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
    //  console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };
     

      this.setState({
        imageSource: source ,
        photo:photo,

      }); 
      
      this.changeUserImage(photo)

     
    }
  });
}
addPhotoGallery() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  //launchCamera
  //showImagePicker
  //launchImageLibrary
  ImagePicker.launchImageLibrary(options, (response) => {
  //  console.log('Response = ', response);

    if (response.didCancel) {
    //  console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
    //  console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
    //  console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };
     

      this.setState({
        imageSource: source ,
        photo:photo,

      }); 
      
      this.changeUserImage(photo)

     
    }
  });
}

 
next(){
  let obj={
    'name':this.state.name,
    'dob':this.state.dob,
    'country_id':this.state.country_id,
    'country_name':this.state.country_name,
    'state_id':this.state.state_id,
    'state_name':this.state.state_name,
    'city_id':this.state.city_id,
    'city_name':this.state.city_name
  }
  this.props.navigation.navigate("EditProfile",{result:obj})
}


  componentWillMount(){

    
  }

 

  checkMedia =()=>{
      if(this.state.image != null){
          return (
            <View style={{width:'60%',height:Dimensions.get('window').height * 0.2,
            backgroundColor:'white'}}>
                <Image style={{width:'100%',height:'100%'}}
                resizeMode='stretch'
                source={this.state.imageSource}/>
                </View>
            )
      }
      else if (this.state.video != null){
        return (
            <View style={{width:'60%',height:Dimensions.get('window').height * 0.2,
            backgroundColor:'white'}}>

            <Video
              source={this.state.videoSource}
            resizeMode='stretch'
                thumbnail={this.state.videoThumbnail}
           style={{width:'99%',height:'100%',alignSelf:'center'}} />
             
              </View>
          )
    }
  }

  checkMediaSheet =()=>{
    if(this.state.image != null){
        return (
            <View style={{width:'50%',height:Dimensions.get('window').height * 0.17,
            backgroundColor:'white',marginLeft:10}}>
              <Image style={{width:'100%',height:'100%'}}
              resizeMode='stretch'
              source={this.state.imageSource}/>
            </View>
          )
    }
    else if (this.state.video != null){
      return (
          <View style={{width:'60%',height:Dimensions.get('window').height * 0.17,
          marginLeft:10}}>

          <Video
          source={this.state.videoSource}
        resizeMode='stretch'
            thumbnail={this.state.videoThumbnail}
       style={{width:'99%',height:'100%',alignSelf:'center'}} />
           
            </View>
        )
  }
}


  _confirmSelection =()=>{
    if(this.isValid()){
      this.SelectSheet.open()
   }
  }
 

    render(){

        const MediaSheetChoose = () =>{

            return (
              <RBSheet
              ref={ref => {
                this.MediaSheetChoose = ref;
              }}
              height={200}
              duration={0}
              customStyles={{
                container: {
                  justifyContent: "center",
                  alignItems: "center",
                  borderTopLeftRadius:15,
                  borderTopRightRadius:15
                },
                // wrapper:{
                //   backgroundColor:'grey'
                // }
              }}
              animationType='fade'
              closeOnDragDown={true}
              minClosingHeight={10}
      
      >
                <View style={{width:'80%',height:'80%',backgroundColor:'white',padding:10}}>
      
                    <View style={{flexDirection:'row',justifyContent:'space-around',alignItems:'center',marginBottom:20}}>
                          <Image style={{ height: 40, width: 40}}
                          resizeMode='contain'
                          source={require('../assets/logo.png')}>
                          </Image>
      
                          <Text style={{fontWeight:'bold',fontSize:16}}>Choose Media Source ! </Text>
      
                    </View>
      
      
                    <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',
                  flex:2}}>
      
                      <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                                  shadowOffset: {
                                    width: 0,
                                    height: 3
                                  },
                                shadowRadius: 10,
                                shadowOpacity: 0.6}}>
                      <TouchableOpacity onPress={()=> this.addPhoto()}
                      style={{width:'100%',backgroundColor:colors.color_primary,padding:20,
                        justifyContent:'center',alignItems:'center',elevation:10}}>
                      <Text style={{color:'white'}}>Image</Text>
                      </TouchableOpacity>
                      </View>
      
      
                      <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                                  shadowOffset: {
                                    width: 0,
                                    height: 3
                                  },
                                shadowRadius: 10,
                                shadowOpacity: 0.6}}>
                      <TouchableOpacity onPress={()=> this.addVideo()}
                      style={{width:'100%',backgroundColor:'white',padding:20,
                        justifyContent:'center',alignItems:'center',elevation:10}}>
                      <Text style={{color:colors.color_primary}}>Video</Text>
                      </TouchableOpacity>
                      </View>
      
                  </View>
                </View>
      </RBSheet>
      
            )
          }


          const SelectSheet = () =>{

            return (
              <RBSheet
              ref={ref => {
                this.SelectSheet = ref;
              }}
             height={420}
              duration={0}
              customStyles={{
                container: {
                  backgroundColor:'white',
                  alignItems: "center",
                  borderTopLeftRadius:15,
                  borderTopRightRadius:15
                },
                // wrapper:{
                //   backgroundColor:' #D3D3D3',
                // }
              }}
              animationType='fade'
              closeOnDragDown={true}
              minClosingHeight={10}
      
      >
                <View style={{width:'90%',height:'100%',
               padding:10}}>

               <View style={styles.main}>
               <Text style={styles.headingLeft}>Price : </Text>
               <Text style={styles.headingRight}> ${parseInt(this.state.budget_value) * parseInt(this.state.duration_value)}</Text>
               </View>


               <View style={styles.main}>
               <Text style={styles.headingLeft}>Name : </Text>
               <Text style={styles.headingRight}> {this.state.name}</Text>
               </View>

               <View style={styles.main}>
               <Text style={styles.headingLeft}>Description : </Text>
               <Text style={[styles.headingRight,{width:'78%'}]}> {this.state.description}</Text>
               </View>


               <View style={styles.main}>
               <Text style={styles.headingLeft}>Advertisement URL : </Text>
               <Text style={styles.headingRight}> {this.state.url}</Text>
               </View>


              
                <Text style={[styles.headingLeft,{marginVertical:2}]}> Media Added : </Text>
                {this.checkMediaSheet()}

                <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',
                  flex:2}}>
      
                      <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                                  shadowOffset: {
                                    width: 0,
                                    height: 3
                                  },
                                shadowRadius: 10,
                                shadowOpacity: 0.6}}>
                      <TouchableOpacity onPress={()=> this.SelectSheet.close()}
                      style={{width:'100%',backgroundColor:colors.color_primary,padding:20,
                        justifyContent:'center',alignItems:'center',elevation:10}}>
                      <Text style={{color:'white'}}>Cancel</Text>
                      </TouchableOpacity>
                      </View>
      
      
                      <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                                  shadowOffset: {
                                    width: 0,
                                    height: 3
                                  },
                                shadowRadius: 10,
                                shadowOpacity: 0.6}}>
                      <TouchableOpacity onPress={()=> this.addAdvertisement()}
                      style={{width:'100%',backgroundColor:'white',padding:20,
                        justifyContent:'center',alignItems:'center',elevation:10}}>
                      <Text style={{color:colors.color_primary}}>Add</Text>
                      </TouchableOpacity>
                      </View>
      
                  </View>
      
                  
                </View>
                <View style={{width:100,height:100}}></View>
      </RBSheet>
      
            )
          }
      
        return(
            <ImageBackground
            source={require('../assets/news_bg.jpg')}
            resizeMode="cover"
            style={styles.imagebackground}>
            <MediaSheetChoose/>
            <SelectSheet/>
  
            <View style={styles.container}>
  
          <KeyboardAwareScrollView style={{width:'100%',flex:1}}
              showsVerticalScrollIndicator={false}
             contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

             <Text style={{color:'white',marginVertical:24,fontSize:17,fontWeight:'bold'}}>Your total spend is {' '}
             ${parseInt(this.state.budget_value) * parseInt(this.state.duration_value)} over{' '}
               { parseInt(this.state.duration_value)} {
                parseInt(this.state.duration_value) > 1 ? 'days' : 'day'
               }</Text>


             <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center' ,width:'90%',marginTop:15}}>
             <Text style={{color:'white',marginVertical:3,fontSize:15}}>Budget 
             <Text style={{fontSize:11,margin:3}}> : $ {parseInt(this.state.budget_value)} value</Text>
             </Text>
                   <Slider
                       value={this.state.budget_value}
                       onValueChange={budget_value => this.setState({ budget_value })}
                       thumbTintColor={'blue'}
                       minimumValue={5}
                       maximumValue={100}
                   />
            </View>

            <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center' ,width:'90%'}}>
            <Text style={{color:'white',marginVertical:3,fontSize:15}}>Duration 
            <Text style={{fontSize:11,margin:3}}> : {parseInt(this.state.duration_value)}{
              parseInt(this.state.duration_value) > 1 ? 'days' : 'day'
             }</Text>
            </Text>
            <Slider
                value={this.state.duration_value}
                onValueChange={duration_value => this.setState({ duration_value })}
                thumbTintColor={'blue'}
                minimumValue={1}
                step={1}
               maximumValue={90}
               animationType='spring'
            />
            </View>
  
         <View style={styles.outsideview}>
  
  
               <TextField
                label='Name'
                value={this.state.name}
                tintColor={colors.color_primary}
                baseColor='white'
                textColor='white'
                onChangeText={(name) => {
                  const regex = /\uD83C\uDFF4(?:\uDB40\uDC67\uDB40\uDC62(?:\uDB40\uDC65\uDB40\uDC6E\uDB40\uDC67|\uDB40\uDC77\uDB40\uDC6C\uDB40\uDC73|\uDB40\uDC73\uDB40\uDC63\uDB40\uDC74)\uDB40\uDC7F|\u200D\u2620\uFE0F)|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC68(?:\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D)?\uD83D\uDC68|(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3]))|\uD83D\uDC69\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D(?:\uD83D[\uDC68\uDC69])|\uD83D[\uDC68\uDC69])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66\u200D\uD83D\uDC66|(?:\uD83D\uDC41\uFE0F\u200D\uD83D\uDDE8|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\uD83D\uDC68(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\u200D[\u2695\u2696\u2708])|(?:(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)\uFE0F|\uD83D\uDC6F|\uD83E[\uDD3C\uDDDE\uDDDF])\u200D[\u2640\u2642]|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|\u200D[\u2640\u2642])|\uD83D\uDC69\u200D[\u2695\u2696\u2708])\uFE0F|\uD83D\uDC69\u200D\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC68(?:\u200D(?:(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D[\uDC66\uDC67])|\uD83C[\uDFFB-\uDFFF])|\uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08|\uD83D\uDC69\u200D\uD83D\uDC67|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66|\uD83C\uDDF6\uD83C\uDDE6|\uD83C\uDDFD\uD83C\uDDF0|\uD83C\uDDF4\uD83C\uDDF2|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])|\uD83C\uDDED(?:\uD83C[\uDDF0\uDDF2\uDDF3\uDDF7\uDDF9\uDDFA])|\uD83C\uDDEC(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEE\uDDF1-\uDDF3\uDDF5-\uDDFA\uDDFC\uDDFE])|\uD83C\uDDEA(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDED\uDDF7-\uDDFA])|\uD83C\uDDE8(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDEE\uDDF0-\uDDF5\uDDF7\uDDFA-\uDDFF])|\uD83C\uDDF2(?:\uD83C[\uDDE6\uDDE8-\uDDED\uDDF0-\uDDFF])|\uD83C\uDDF3(?:\uD83C[\uDDE6\uDDE8\uDDEA-\uDDEC\uDDEE\uDDF1\uDDF4\uDDF5\uDDF7\uDDFA\uDDFF])|\uD83C\uDDFC(?:\uD83C[\uDDEB\uDDF8])|\uD83C\uDDFA(?:\uD83C[\uDDE6\uDDEC\uDDF2\uDDF3\uDDF8\uDDFE\uDDFF])|\uD83C\uDDF0(?:\uD83C[\uDDEA\uDDEC-\uDDEE\uDDF2\uDDF3\uDDF5\uDDF7\uDDFC\uDDFE\uDDFF])|\uD83C\uDDEF(?:\uD83C[\uDDEA\uDDF2\uDDF4\uDDF5])|\uD83C\uDDF8(?:\uD83C[\uDDE6-\uDDEA\uDDEC-\uDDF4\uDDF7-\uDDF9\uDDFB\uDDFD-\uDDFF])|\uD83C\uDDEE(?:\uD83C[\uDDE8-\uDDEA\uDDF1-\uDDF4\uDDF6-\uDDF9])|\uD83C\uDDFF(?:\uD83C[\uDDE6\uDDF2\uDDFC])|\uD83C\uDDEB(?:\uD83C[\uDDEE-\uDDF0\uDDF2\uDDF4\uDDF7])|\uD83C\uDDF5(?:\uD83C[\uDDE6\uDDEA-\uDDED\uDDF0-\uDDF3\uDDF7-\uDDF9\uDDFC\uDDFE])|\uD83C\uDDE9(?:\uD83C[\uDDEA\uDDEC\uDDEF\uDDF0\uDDF2\uDDF4\uDDFF])|\uD83C\uDDF9(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDED\uDDEF-\uDDF4\uDDF7\uDDF9\uDDFB\uDDFC\uDDFF])|\uD83C\uDDE7(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEF\uDDF1-\uDDF4\uDDF6-\uDDF9\uDDFB\uDDFC\uDDFE\uDDFF])|[#\*0-9]\uFE0F\u20E3|\uD83C\uDDF1(?:\uD83C[\uDDE6-\uDDE8\uDDEE\uDDF0\uDDF7-\uDDFB\uDDFE])|\uD83C\uDDE6(?:\uD83C[\uDDE8-\uDDEC\uDDEE\uDDF1\uDDF2\uDDF4\uDDF6-\uDDFA\uDDFC\uDDFD\uDDFF])|\uD83C\uDDF7(?:\uD83C[\uDDEA\uDDF4\uDDF8\uDDFA\uDDFC])|\uD83C\uDDFB(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDEE\uDDF3\uDDFA])|\uD83C\uDDFE(?:\uD83C[\uDDEA\uDDF9])|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u261D\u270A-\u270D]|\uD83C[\uDF85\uDFC2\uDFC7]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66\uDC67\uDC70\uDC72\uDC74-\uDC76\uDC78\uDC7C\uDC83\uDC85\uDCAA\uDD74\uDD7A\uDD90\uDD95\uDD96\uDE4C\uDE4F\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD30-\uDD36\uDDB5\uDDB6\uDDD1-\uDDD5])(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u231A\u231B\u23E9-\u23EC\u23F0\u23F3\u25FD\u25FE\u2614\u2615\u2648-\u2653\u267F\u2693\u26A1\u26AA\u26AB\u26BD\u26BE\u26C4\u26C5\u26CE\u26D4\u26EA\u26F2\u26F3\u26F5\u26FA\u26FD\u2705\u270A\u270B\u2728\u274C\u274E\u2753-\u2755\u2757\u2795-\u2797\u27B0\u27BF\u2B1B\u2B1C\u2B50\u2B55]|\uD83C[\uDC04\uDCCF\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE1A\uDE2F\uDE32-\uDE36\uDE38-\uDE3A\uDE50\uDE51\uDF00-\uDF20\uDF2D-\uDF35\uDF37-\uDF7C\uDF7E-\uDF93\uDFA0-\uDFCA\uDFCF-\uDFD3\uDFE0-\uDFF0\uDFF4\uDFF8-\uDFFF]|\uD83D[\uDC00-\uDC3E\uDC40\uDC42-\uDCFC\uDCFF-\uDD3D\uDD4B-\uDD4E\uDD50-\uDD67\uDD7A\uDD95\uDD96\uDDA4\uDDFB-\uDE4F\uDE80-\uDEC5\uDECC\uDED0-\uDED2\uDEEB\uDEEC\uDEF4-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])|(?:[#*0-9\xA9\xAE\u203C\u2049\u2122\u2139\u2194-\u2199\u21A9\u21AA\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA\u24C2\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE\u2600-\u2604\u260E\u2611\u2614\u2615\u2618\u261D\u2620\u2622\u2623\u2626\u262A\u262E\u262F\u2638-\u263A\u2640\u2642\u2648-\u2653\u265F\u2660\u2663\u2665\u2666\u2668\u267B\u267E\u267F\u2692-\u2697\u2699\u269B\u269C\u26A0\u26A1\u26AA\u26AB\u26B0\u26B1\u26BD\u26BE\u26C4\u26C5\u26C8\u26CE\u26CF\u26D1\u26D3\u26D4\u26E9\u26EA\u26F0-\u26F5\u26F7-\u26FA\u26FD\u2702\u2705\u2708-\u270D\u270F\u2712\u2714\u2716\u271D\u2721\u2728\u2733\u2734\u2744\u2747\u274C\u274E\u2753-\u2755\u2757\u2763\u2764\u2795-\u2797\u27A1\u27B0\u27BF\u2934\u2935\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55\u3030\u303D\u3297\u3299]|\uD83C[\uDC04\uDCCF\uDD70\uDD71\uDD7E\uDD7F\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE02\uDE1A\uDE2F\uDE32-\uDE3A\uDE50\uDE51\uDF00-\uDF21\uDF24-\uDF93\uDF96\uDF97\uDF99-\uDF9B\uDF9E-\uDFF0\uDFF3-\uDFF5\uDFF7-\uDFFF]|\uD83D[\uDC00-\uDCFD\uDCFF-\uDD3D\uDD49-\uDD4E\uDD50-\uDD67\uDD6F\uDD70\uDD73-\uDD7A\uDD87\uDD8A-\uDD8D\uDD90\uDD95\uDD96\uDDA4\uDDA5\uDDA8\uDDB1\uDDB2\uDDBC\uDDC2-\uDDC4\uDDD1-\uDDD3\uDDDC-\uDDDE\uDDE1\uDDE3\uDDE8\uDDEF\uDDF3\uDDFA-\uDE4F\uDE80-\uDEC5\uDECB-\uDED2\uDEE0-\uDEE5\uDEE9\uDEEB\uDEEC\uDEF0\uDEF3-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])\uFE0F|(?:[\u261D\u26F9\u270A-\u270D]|\uD83C[\uDF85\uDFC2-\uDFC4\uDFC7\uDFCA-\uDFCC]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66-\uDC69\uDC6E\uDC70-\uDC78\uDC7C\uDC81-\uDC83\uDC85-\uDC87\uDCAA\uDD74\uDD75\uDD7A\uDD90\uDD95\uDD96\uDE45-\uDE47\uDE4B-\uDE4F\uDEA3\uDEB4-\uDEB6\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD26\uDD30-\uDD39\uDD3D\uDD3E\uDDB5\uDDB6\uDDB8\uDDB9\uDDD1-\uDDDD])/g
                   var comme = name.replace(regex, '')
                  this.setState({name:comme})
              }}
                ref={(input) => { this.name = input; }}
                onSubmitEditing = {() => this.description.focus()}
                maxLength={25}
                keyboardType={Platform.OS === 'ios' ? 'ascii-capable' : 'visible-password'}
              />
  
           <TextField
                label='Description'
                value={this.state.description}
                 multiline={true}
                tintColor={colors.color_primary}
                baseColor='white'
                textColor='white'
                onChangeText={(description) => this.setState({ description})}
                ref={(input) => { this.description = input; }}
                onSubmitEditing = {() => this.url.focus()}
              />

              <TextField
              label='Advertisement URL'
              value={this.state.url}
               multiline={true}
              tintColor={colors.color_primary}
              baseColor='white'
              textColor='white'
              onChangeText={(url) => this.setState({ url})}
              ref={(input) => { this.url = input; }}
            
            />
  
              </View>


              {this.checkMedia()}

             
  
              <TouchableOpacity style={{width:'100%'}} onPress={() => {this.MediaSheetChoose.open()}}>
              <View style={{
                width: '90%', height: 50, backgroundColor: 'white', justifyContent: 'center',
                alignItems: 'center', alignSelf: 'center', marginTop: 30, borderRadius: 5,
                shadowColor: 'gray',
                shadowRadius: 10,
                shadowOpacity: 1,
                elevation:10
  
              }}>
                <Text style={{ color: 'blue', fontWeight: 'bold' }}>Add Media</Text>
              </View>
            </TouchableOpacity>

              <TouchableOpacity style={{width:'100%'}} onPress={() => {this._confirmSelection()}}>
              <View style={{
                width: '90%', height: 50, backgroundColor: colors.color_primary, justifyContent: 'center',
                alignItems: 'center', alignSelf: 'center', marginTop: 20, borderRadius: 5,
                shadowColor: 'gray',
                shadowRadius: 10,
                shadowOpacity: 1,
                elevation:10,marginBottom:90
  
              }}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>Post Advertisement</Text>
              </View>
            </TouchableOpacity>
  
  
          </KeyboardAwareScrollView>
      
              {this.state.loading_status &&
                <View style={[
              StyleSheet.absoluteFill,
              { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
            ]}>
                <WaveIndicator color={colors.color_primary}/>
                </View>
              }
             
            </View>
            </ImageBackground>
           
  
          )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(AddAdvertisements);



const styles = StyleSheet.create({
    container: {
        height: '100%',
        
      },


      imagebackground:{
      position: 'absolute',
      
      backgroundColor:'rgba(0,0,0,0.45)',
     top:0,bottom:0,right:0,left:0,zIndex:-2
    },
  
    outsideview:{ 
      width: '90%', 
      justifyContent: 'center',
       alignSelf: 'center' ,
       marginBottom:30},
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  },
  headingLeft:{color:colors.color_primary,
    fontSize:15,fontWeight:'bold'},

    headingRight:{color:'black',
    fontSize:12,fontWeight:'100'},
    main:{flexDirection:'row',alignItems:'center',margin:4}

  });