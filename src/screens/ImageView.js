import React from 'react';
import { colors, urls } from '../Globals';
import Snackbar from 'react-native-snackbar';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';

import Video from 'react-native-video-controls';


import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';

import FastImage from 'react-native-fast-image'
var windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height;
class ImageView extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      image_url:'',
      image_width:'',
      image_height:'',
      user_id:'',
      showMenu:false,
      item:'',
    };

  }

  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };
  
  hideMenu = () => {
    this._menu.hide();
  };
  
  showMenu = () => {
    this._menu.show();
  };


  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'black',
    headerStyle: {
      backgroundColor: 'black',
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
      
            
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                 <TouchableOpacity onPress={()=>{ navigation.goBack() }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

          
     </View>
    ),
  });


  componentWillMount(){
    AsyncStorage.getItem('user_id').then((userid) => {
      if (userid) {
       this.setState({user_id:userid})
      let result = this.props.navigation.getParam('result')
      var url = result['image_url']


      Image.getSize( url, ( width, height ) => 
      {
        var h_percentage = (windowWidth*100)/width
        var h_img = (height*h_percentage)/100
        if(width == 0){
            this.setState({image_height:400,image_width:width})
          } else {
  var h_percentage = (windowWidth*100)/width
  var h_img = (height*h_percentage)/100
  this.setState({image_height:h_img,image_width:width})
 }
 }, (error) => {
     //   console.log(`Couldn't get the image size: ${error.message}`);
        this.setState({image_height:400,image_width:'100%'})
       });
    this.setState({image_url:url , user_id:userid,item:result['item']})
      }})
  }

  myPostDelete=(item)=>{
                     this._menu.hide();
                      var formData = new FormData();
                      formData.append('user_id',this.state.user_id);
                      formData.append('advert_id',item.id);
                       fetch(urls.base_url +'api/delete_advert', {
                       method: 'POST',
                       headers: {
                         'Accept': 'application/json',
                         'Content-Type': 'multipart/form-data',
                       },
                      body: formData
                     }).then((response) => response.json())
                           .then((responseJson) => {
                 
                    console.log('delete-->>',responseJson)
                    if(!responseJson.error){
                      console.log("hi")
                      this.props.navigation.navigate('Home');//MyProfile
                      const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'Home' })],
                      });
                      this.props.navigation.dispatch(resetAction);
                  //  this.props.navigation.navigate('Home')
                                    }
                           }).catch((error) => {
                              console.log(error)
                                  });
  }
  
  myPostEdit=(item)=>{
    this._menu.hide();
    var data={'post':item}
   
    this.props.navigation.navigate('EditAdvertiesement',{result:data})
  
  }

    render()
    {

        return(
          <SafeAreaView style={styles.container}>

          <View style={{backgroundColor:'black',width:'100%',flex:1,
         justifyContent:'center',alignItems:'center'}}
          >
         

{
      this.state.user_id==this.state.item.user_id
      ?
    <View style={{zIndex:999,position:'absolute',right:10,top:3}}>
    <Menu
    ref={this.setMenuRef}
    button={
    <TouchableOpacity onPress={this.showMenu} style={{width:30,height:30,top:7}}>
    <Image source={require('../assets/threeDotWhite.png')}
                    style={[styles.menuImage,{alignSelf:'center'}]} resizeMode='contain'/>
    </TouchableOpacity>
    } >
    <MenuItem onPress={ ()=>{
              Alert.alert(
              "Alert", 
              'Are you sure want to delete ?',
              [
                { text: "OK", onPress:()=> this.myPostDelete(this.state.item)},
                { text: "CANCEL", onPress: () => console.log("cancel Pressed") }
              ],
              { cancelable: false } 
               ) }
            } >Delete</MenuItem>
    <MenuItem onPress={() => {
         this.myPostEdit(this.state.item)
    }
        }>Edit</MenuItem> 
    </Menu>
    </View>
      : null 
     } 


           <Image source={{uri:this.state.image_url}}
            resizeMode="stretch"
           style={{width:'100%',height: this.state.image_height !='' ? this.state.image_height : 400}}
          />
                
            </View>

            {this.state.loading_status &&
              <View pointerEvents="none" style={styles.loading}>
              <PacmanIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ImageView);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  },
  menuImage:{width:17,height:17,marginRight:6,flex:1},
  });