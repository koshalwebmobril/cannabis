
import React from 'react';
import { colors,urls } from '../Globals';
import { Button,Header, registerCustomIconType } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { Dropdown } from 'react-native-material-dropdown';
import Snackbar from 'react-native-snackbar';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ToastAndroid
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { addUser } from '../actions/actions';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { NavigationActions, StackActions } from 'react-navigation';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';




class AddPitch extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      title: '',
      email: '',
      contact: '',
      synopsis: '',
      more_details: '',  
      loading_status:false,
      capital_required:'',
      country_id:'',
      countries:[],
      country_name:'',
      state_id:'',
      states:[],
      state_name:'',
      city_id:'',
      city_name:'',
      cities:[],
      minimum_investors:'',
      fill_investment:''
    };


  }


    
  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
     
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
           <TouchableOpacity onPress={()=>{
             //navigation.goBack()
             navigation.navigate(navigation.getParam('route_name','HomePage')) 
              }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Add New Pitch </Text>
     </View>
    ),
  });

  fetchCountries = async () =>{
    this.setState({loading_status:true})

               let url = urls.base_url +'api/countries'
                    fetch(url, {
                    method: 'POST',

                    }).then((response) => response.json())
                        .then((responseJson) => {

                         // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          this.setState({loading_status:false})
                          if(!responseJson.error){
                           
                            var length = responseJson.data.length.toString();
                            var temp_arr=[]
                            for(var i = 0 ; i < length ; i++){
                            var id = responseJson.data[i].id
                            var name = responseJson.data[i].name
                          
                           
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], name: name };
                                  array[i] = { ...array[i], value : name };

                                  temp_arr = array
                                  
                                  //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                            }
                            this.setState({ countries : temp_arr});

                          }


                        else{
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                          // : Alert.alert(responseJson.message)

                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        }

                      }
                        ).catch((error) => {

                          this.setState({loading_status:false})
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show('Error !', ToastAndroid.SHORT)
                          // : Alert.alert('Error !')

                          Snackbar.show({
                            title: 'Error !',
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        });



  }

  fetchStates = async (countryId) =>{
    this.setState({loading_status:true,state_id:'',city_id:'',city_name:'',state_name:''})

    var formData = new FormData();
    formData.append('country_id', countryId);

               let url = urls.base_url +'api/states'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                    body: formData

                    }).then((response) => response.json())
                        .then((responseJson) => {

                         // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          this.setState({loading_status:false})
                          if(!responseJson.error){
                           
                            var length = responseJson.data.length.toString();
                            var temp_arr=[]
                            for(var i = 0 ; i < length ; i++){
                            var id = responseJson.data[i].id
                            var name = responseJson.data[i].name
                          
                           
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], name: name };
                                  array[i] = { ...array[i], value : name };

                                  temp_arr = array
                                  
                                  //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                            }
                            this.setState({ states : temp_arr});

                          }


                        else{
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                          // : Alert.alert(responseJson.message)

                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        }

                      }
                        ).catch((error) => {

                          this.setState({loading_status:false})
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show('Error !', ToastAndroid.SHORT)
                          // : Alert.alert('Error !')

                          Snackbar.show({
                            title: 'Error !',
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        });



  }
  fetchCities = async (stateId) =>{
    this.setState({loading_status:true,city_id:'',city_name:''})
    var formData = new FormData();
    formData.append('state_id',stateId);

    formData.append('country_id',this.state.country_id);


               let url = urls.base_url +'api/cities'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                    body: formData

                    }).then((response) => response.json())
                        .then((responseJson) => {

                         // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          this.setState({loading_status:false})
                          if(!responseJson.error){
                           
                            var length = responseJson.data.length.toString();
                            var temp_arr=[]
                            for(var i = 0 ; i < length ; i++){
                            var id = responseJson.data[i].id
                            var name = responseJson.data[i].name
                          
                           
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], name: name };
                                  array[i] = { ...array[i], value : name };

                                  temp_arr = array
                                  
                                  //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                            }
                            this.setState({ cities : temp_arr});

                          }


                        else{
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                          // : Alert.alert(responseJson.message)

                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        }

                      }
                        ).catch((error) => {

                          this.setState({loading_status:false})
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show('Error !', ToastAndroid.SHORT)
                          // : Alert.alert('Error !')

                          Snackbar.show({
                            title: 'Error !',
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        });



  }



  componentDidMount() {
    this.fetchCountries()
  }





  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }


  isValid() {

    var isnum = /^\d+$/.test(this.state.contact);



    const { title,contact, email, 
      synopsis,more_details,country_id,
      state_id,city_id,capital_required,
      minimum_investors,
    fill_investment } = this.state;

    let valid = false;

    if (title.toString().trim().length > 0 
      && email.toString().trim().length > 0 
      && this.validateEmail(email.trim())
      && contact.toString().trim().length > 7
      &&  synopsis.toString().trim().length < 249
      &&  more_details.toString().trim().length < 749
      &&  minimum_investors.toString().trim().length > 0
      &&  country_id.toString().trim().length > 0
      &&  state_id.toString().trim().length > 0
      &&  city_id.toString().trim().length > 0
      &&  fill_investment.toString().trim().length > 0) {
      valid = true;
    }



    else if (title.toString().trim().length == 0){
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter Cannabis Expression', ToastAndroid.SHORT)
      //   : Alert.alert('Enter Cannabis  Expression')

        Snackbar.show({
          title: 'Enter Pitch Title',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });


      return false;
    }

  
    else if (country_id.toString().trim().length == 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter Country', ToastAndroid.SHORT)
      //   : Alert.alert('Enter Country')

        Snackbar.show({
          title: 'Select Country!',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });
      return false;
    }

    else if (state_id.toString().trim().length == 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter State', ToastAndroid.SHORT)
      //   : Alert.alert('Enter State')

      Snackbar.show({
        title: 'Select State!',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
     
      });


      return false;
    }

    else if (city_id.toString().trim().length == 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter City', ToastAndroid.SHORT)
      //   : Alert.alert('Enter City')

      Snackbar.show({
        title: 'Select City ',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
     
      });
      return false;
    }

  
    else if (contact.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter  Email', ToastAndroid.SHORT)
      //   : Alert.alert('Enter  Email')

        Snackbar.show({
          title: 'Enter  Contact',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });


      return false;
    }
    else if (contact.toString().trim().length < 10) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter  Email', ToastAndroid.SHORT)
      //   : Alert.alert('Enter  Email')

        Snackbar.show({
          title: 'Contact Number be at least 10 characters long',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });


      return false;
    }

    else if(!isnum){

      Snackbar.show({
        title: 'Enter Valid Phone Number',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
     
      });



      return false;
    }
    else if (email.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter  Email', ToastAndroid.SHORT)
      //   : Alert.alert('Enter  Email')

        Snackbar.show({
          title: 'Enter  Email',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });


      return false;
    }
    else if (!this.validateEmail(email.toString().trim())) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter valid Email', ToastAndroid.SHORT)
      //   : Alert.alert('Enter valid Email')

        Snackbar.show({
          title: 'Enter valid Email!',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });


      return false;
    }



    else if (synopsis.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter date of birth', ToastAndroid.SHORT)
      //   : Alert.alert('Enter date of birth')

        Snackbar.show({
          title: 'Enter Synopsis',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });
      return false;
    }
    else if (synopsis.toString().trim().length > 250) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter date of birth', ToastAndroid.SHORT)
      //   : Alert.alert('Enter date of birth')

        // Snackbar.show({
        //   title: 'Synopsis Should be 250 characters long',
        //   duration: Snackbar.LENGTH_SHORT,
        //   backgroundColor:'black',
        //   color:'red',
        //   action: {
        //     title: 'OK',
        //     color: 'green',
        //     onPress: () => { /* Do something. */ },
        //   },
       
        // });

Alert.alert('Synopsis Should be 250 characters long')


      return false;
    }

    else if (more_details.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter date of birth', ToastAndroid.SHORT)
      //   : Alert.alert('Enter date of birth')

        Snackbar.show({
          title: 'Enter More Details',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });

       

      return false;
    }
    else if (more_details.toString().trim().length > 750) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter date of birth', ToastAndroid.SHORT)
      //   : Alert.alert('Enter date of birth')

        // Snackbar.show({
        //   title: 'More Details Should be 750 characters long',
        //   duration: Snackbar.LENGTH_SHORT,
        //   backgroundColor:'black',
        //   color:'red',
        //   action: {
        //     title: 'OK',
        //     color: 'green',
        //     onPress: () => { /* Do something. */ },
        //   },
       
        // });

        Alert.alert('More Details Should be 750 characters long')

      return false;
    }
    else if (capital_required.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter password', ToastAndroid.SHORT)
      //   : Alert.alert('Enter password')

        Snackbar.show({
          title: 'Enter required capital',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });
      return false;
    }
    else if (minimum_investors.toString().trim().length ==  0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Password should be 8-16 characters long', ToastAndroid.SHORT)
      //   : Alert.alert('Password should be 8-16 characters long')

        Snackbar.show({
          title: 'Enter Minimum number of Investors',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });
      return false;
    }
   
  
    else if (fill_investment.toString().trim().length == 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter City', ToastAndroid.SHORT)
      //   : Alert.alert('Enter City')

      Snackbar.show({
        title: 'Select Open to Fill Investment',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
     
      });
      return false;
    }
    

    return valid
  }


  submit(){
   // console.log("DD",JSON.stringify(this.isValid()))
      if(this.isValid()){
        AsyncStorage.getItem("user_id").then((item) => {
          if (item) {
      
           
      
            var formData = new FormData();
      
            formData.append('user_id',item);
            formData.append('pitch_title',this.state.title);
            formData.append('country_id', this.state.country_id);
            formData.append('city_id', this.state.city_id);
            formData.append('state_id', this.state.state_id);
            formData.append('contact_no', this.state.contact);
            formData.append('contact_email', this.state.email);
            formData.append('synopsis_of_idea', this.state.synopsis);
           
            formData.append('more_detail_of_idea', this.state.more_details);
            formData.append('capital_required', this.state.capital_required);
            formData.append('minimum_per_investor', this.state.minimum_investors);
            formData.append('open_to_full_investment', this.state.fill_investment);
     
            
      
                          this.setState({loading_status:true})
                          let url = urls.base_url +'api/add_pitch'
                          fetch(url, {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData
      
                        }).then((response) => response.json())
                              .then((responseJson) => {
                      this.setState({loading_status:false})
                    //  console.log(JSON.stringify(responseJson))
                 //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
      
                //  Platform.OS === 'android' 
                //                     ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                //                     : Alert.alert(responseJson.message)

                    //   console.log('data submition is ======>>>',responseJson)
    
                                    Snackbar.show({
                                      title: responseJson.message,
                                      duration: Snackbar.LENGTH_SHORT,
                                      backgroundColor:'black',
                                      color:'red',
                                      action: {
                                        title: 'OK',
                                        color: 'green',
                                        onPress: () => { /* Do something. */ },
                                      },
                                    });
      
      
                                  if(!responseJson.error){
                                    this.props.navigation.navigate('InvestmentHub');
                                
                                    const resetAction = StackActions.reset({
                                    index: 0,
                                    key: 'InvestmentHub',
                                    actions: [NavigationActions.navigate({ routeName: 'InvestmentHub' })],
                                  });
                
                                  this.props.navigation.dispatch(resetAction);
 
                                  // this.props.navigation.navigate("Home")
                                }
                                else{
      
                                    
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})
                             //   console.log(error)
                                Snackbar.show({
                                  title: 'Try Again !',
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });
      
                              });
          }
          else{
            ToastAndroid.show("User not found !",ToastAndroid.LONG)
          }
        });
      }
  }

  backBtn() {
    this.props.navigation.goBack();
  }


  login() {
    this.props.navigation.navigate('Login')
  }

  render() {

    let data = [{
      value: 'Yes',
    }, {
      value: 'No',
    }];

    return (
      <SafeAreaView style={styles.container}>




        <KeyboardAwareScrollView style={{backgroundColor:'white',width:'100%',flex:1}}
        showsVerticalScrollIndicator={false}
       contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

        


          <View style={{ width: '90%', justifyContent: 'center', alignSelf: 'center' ,marginBottom:30}}>


              <TextField
              label='Pitch Title'
              value={this.state.title}
              onChangeText={(title) => {
                 const regex = /\uD83C\uDFF4(?:\uDB40\uDC67\uDB40\uDC62(?:\uDB40\uDC65\uDB40\uDC6E\uDB40\uDC67|\uDB40\uDC77\uDB40\uDC6C\uDB40\uDC73|\uDB40\uDC73\uDB40\uDC63\uDB40\uDC74)\uDB40\uDC7F|\u200D\u2620\uFE0F)|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC68(?:\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D)?\uD83D\uDC68|(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3]))|\uD83D\uDC69\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D(?:\uD83D[\uDC68\uDC69])|\uD83D[\uDC68\uDC69])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66\u200D\uD83D\uDC66|(?:\uD83D\uDC41\uFE0F\u200D\uD83D\uDDE8|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\uD83D\uDC68(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\u200D[\u2695\u2696\u2708])|(?:(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)\uFE0F|\uD83D\uDC6F|\uD83E[\uDD3C\uDDDE\uDDDF])\u200D[\u2640\u2642]|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|\u200D[\u2640\u2642])|\uD83D\uDC69\u200D[\u2695\u2696\u2708])\uFE0F|\uD83D\uDC69\u200D\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC68(?:\u200D(?:(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D[\uDC66\uDC67])|\uD83C[\uDFFB-\uDFFF])|\uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08|\uD83D\uDC69\u200D\uD83D\uDC67|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66|\uD83C\uDDF6\uD83C\uDDE6|\uD83C\uDDFD\uD83C\uDDF0|\uD83C\uDDF4\uD83C\uDDF2|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])|\uD83C\uDDED(?:\uD83C[\uDDF0\uDDF2\uDDF3\uDDF7\uDDF9\uDDFA])|\uD83C\uDDEC(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEE\uDDF1-\uDDF3\uDDF5-\uDDFA\uDDFC\uDDFE])|\uD83C\uDDEA(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDED\uDDF7-\uDDFA])|\uD83C\uDDE8(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDEE\uDDF0-\uDDF5\uDDF7\uDDFA-\uDDFF])|\uD83C\uDDF2(?:\uD83C[\uDDE6\uDDE8-\uDDED\uDDF0-\uDDFF])|\uD83C\uDDF3(?:\uD83C[\uDDE6\uDDE8\uDDEA-\uDDEC\uDDEE\uDDF1\uDDF4\uDDF5\uDDF7\uDDFA\uDDFF])|\uD83C\uDDFC(?:\uD83C[\uDDEB\uDDF8])|\uD83C\uDDFA(?:\uD83C[\uDDE6\uDDEC\uDDF2\uDDF3\uDDF8\uDDFE\uDDFF])|\uD83C\uDDF0(?:\uD83C[\uDDEA\uDDEC-\uDDEE\uDDF2\uDDF3\uDDF5\uDDF7\uDDFC\uDDFE\uDDFF])|\uD83C\uDDEF(?:\uD83C[\uDDEA\uDDF2\uDDF4\uDDF5])|\uD83C\uDDF8(?:\uD83C[\uDDE6-\uDDEA\uDDEC-\uDDF4\uDDF7-\uDDF9\uDDFB\uDDFD-\uDDFF])|\uD83C\uDDEE(?:\uD83C[\uDDE8-\uDDEA\uDDF1-\uDDF4\uDDF6-\uDDF9])|\uD83C\uDDFF(?:\uD83C[\uDDE6\uDDF2\uDDFC])|\uD83C\uDDEB(?:\uD83C[\uDDEE-\uDDF0\uDDF2\uDDF4\uDDF7])|\uD83C\uDDF5(?:\uD83C[\uDDE6\uDDEA-\uDDED\uDDF0-\uDDF3\uDDF7-\uDDF9\uDDFC\uDDFE])|\uD83C\uDDE9(?:\uD83C[\uDDEA\uDDEC\uDDEF\uDDF0\uDDF2\uDDF4\uDDFF])|\uD83C\uDDF9(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDED\uDDEF-\uDDF4\uDDF7\uDDF9\uDDFB\uDDFC\uDDFF])|\uD83C\uDDE7(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEF\uDDF1-\uDDF4\uDDF6-\uDDF9\uDDFB\uDDFC\uDDFE\uDDFF])|[#\*0-9]\uFE0F\u20E3|\uD83C\uDDF1(?:\uD83C[\uDDE6-\uDDE8\uDDEE\uDDF0\uDDF7-\uDDFB\uDDFE])|\uD83C\uDDE6(?:\uD83C[\uDDE8-\uDDEC\uDDEE\uDDF1\uDDF2\uDDF4\uDDF6-\uDDFA\uDDFC\uDDFD\uDDFF])|\uD83C\uDDF7(?:\uD83C[\uDDEA\uDDF4\uDDF8\uDDFA\uDDFC])|\uD83C\uDDFB(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDEE\uDDF3\uDDFA])|\uD83C\uDDFE(?:\uD83C[\uDDEA\uDDF9])|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u261D\u270A-\u270D]|\uD83C[\uDF85\uDFC2\uDFC7]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66\uDC67\uDC70\uDC72\uDC74-\uDC76\uDC78\uDC7C\uDC83\uDC85\uDCAA\uDD74\uDD7A\uDD90\uDD95\uDD96\uDE4C\uDE4F\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD30-\uDD36\uDDB5\uDDB6\uDDD1-\uDDD5])(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u231A\u231B\u23E9-\u23EC\u23F0\u23F3\u25FD\u25FE\u2614\u2615\u2648-\u2653\u267F\u2693\u26A1\u26AA\u26AB\u26BD\u26BE\u26C4\u26C5\u26CE\u26D4\u26EA\u26F2\u26F3\u26F5\u26FA\u26FD\u2705\u270A\u270B\u2728\u274C\u274E\u2753-\u2755\u2757\u2795-\u2797\u27B0\u27BF\u2B1B\u2B1C\u2B50\u2B55]|\uD83C[\uDC04\uDCCF\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE1A\uDE2F\uDE32-\uDE36\uDE38-\uDE3A\uDE50\uDE51\uDF00-\uDF20\uDF2D-\uDF35\uDF37-\uDF7C\uDF7E-\uDF93\uDFA0-\uDFCA\uDFCF-\uDFD3\uDFE0-\uDFF0\uDFF4\uDFF8-\uDFFF]|\uD83D[\uDC00-\uDC3E\uDC40\uDC42-\uDCFC\uDCFF-\uDD3D\uDD4B-\uDD4E\uDD50-\uDD67\uDD7A\uDD95\uDD96\uDDA4\uDDFB-\uDE4F\uDE80-\uDEC5\uDECC\uDED0-\uDED2\uDEEB\uDEEC\uDEF4-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])|(?:[#*0-9\xA9\xAE\u203C\u2049\u2122\u2139\u2194-\u2199\u21A9\u21AA\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA\u24C2\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE\u2600-\u2604\u260E\u2611\u2614\u2615\u2618\u261D\u2620\u2622\u2623\u2626\u262A\u262E\u262F\u2638-\u263A\u2640\u2642\u2648-\u2653\u265F\u2660\u2663\u2665\u2666\u2668\u267B\u267E\u267F\u2692-\u2697\u2699\u269B\u269C\u26A0\u26A1\u26AA\u26AB\u26B0\u26B1\u26BD\u26BE\u26C4\u26C5\u26C8\u26CE\u26CF\u26D1\u26D3\u26D4\u26E9\u26EA\u26F0-\u26F5\u26F7-\u26FA\u26FD\u2702\u2705\u2708-\u270D\u270F\u2712\u2714\u2716\u271D\u2721\u2728\u2733\u2734\u2744\u2747\u274C\u274E\u2753-\u2755\u2757\u2763\u2764\u2795-\u2797\u27A1\u27B0\u27BF\u2934\u2935\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55\u3030\u303D\u3297\u3299]|\uD83C[\uDC04\uDCCF\uDD70\uDD71\uDD7E\uDD7F\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE02\uDE1A\uDE2F\uDE32-\uDE3A\uDE50\uDE51\uDF00-\uDF21\uDF24-\uDF93\uDF96\uDF97\uDF99-\uDF9B\uDF9E-\uDFF0\uDFF3-\uDFF5\uDFF7-\uDFFF]|\uD83D[\uDC00-\uDCFD\uDCFF-\uDD3D\uDD49-\uDD4E\uDD50-\uDD67\uDD6F\uDD70\uDD73-\uDD7A\uDD87\uDD8A-\uDD8D\uDD90\uDD95\uDD96\uDDA4\uDDA5\uDDA8\uDDB1\uDDB2\uDDBC\uDDC2-\uDDC4\uDDD1-\uDDD3\uDDDC-\uDDDE\uDDE1\uDDE3\uDDE8\uDDEF\uDDF3\uDDFA-\uDE4F\uDE80-\uDEC5\uDECB-\uDED2\uDEE0-\uDEE5\uDEE9\uDEEB\uDEEC\uDEF0\uDEF3-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])\uFE0F|(?:[\u261D\u26F9\u270A-\u270D]|\uD83C[\uDF85\uDFC2-\uDFC4\uDFC7\uDFCA-\uDFCC]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66-\uDC69\uDC6E\uDC70-\uDC78\uDC7C\uDC81-\uDC83\uDC85-\uDC87\uDCAA\uDD74\uDD75\uDD7A\uDD90\uDD95\uDD96\uDE45-\uDE47\uDE4B-\uDE4F\uDEA3\uDEB4-\uDEB6\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD26\uDD30-\uDD39\uDD3D\uDD3E\uDDB5\uDDB6\uDDB8\uDDB9\uDDD1-\uDDDD])/g
                 var comme = title.replace(regex, '')
                this.setState({ title : comme })
               // this.setState({ title})
              
              }}
              ref={(input) => { this.title = input; }}
              maxLength={45}
            />



          <Dropdown
          label='Select Your Country'
          rippleCentered={true}
          valueExtractor={({value})=> value}
          onChangeText = {(value, index, data) =>{
          //  console.log(`Selected value: ${value} ${index} ${data[index].id}`)
           // value is text name or label
           //index is index of item
           //data[index].id is id of the country
           this.setState({country_id:data[index].id,country_name:data[index].name})
           this.fetchStates(data[index].id)

          }}
          data={this.state.countries}
          value={this.state.country_name}
          ref={(input) => { this.country = input; }}
        />

        <Dropdown
          label='Select Your State'
          data={this.state.states}
          rippleCentered={true}
          valueExtractor={({value})=> value}
          onChangeText = {(value, index, data) =>{
           this.setState({state_id:data[index].id,state_name:data[index].name})
           this.fetchCities(data[index].id)

          }}
          value={this.state.state_name}
        />
        <Dropdown
          label='Select Your City'
          data={this.state.cities}
          rippleCentered={true}
          valueExtractor={({value})=> value}
         
          onChangeText = {(value, index, data) =>{
           this.setState({city_id:data[index].id,city_name:data[index].name})

          }}
          value={this.state.city_name}
        />


            <TextField
              label='Contact Number'
              value={this.state.contact}
              autoCapitalize={'none'}
              keyboardType = 'numeric'
              textContentType='telephoneNumber'
              onChangeText={(contact) => this.setState({ contact})}
              ref={(input) => { this.contact = input; }}
              maxLength={15}
            
            />



            <TextField
              label='Contact Email'
              keyboardType={Platform.OS === 'android' ? 'email-address' : 'ascii-capable'}
              value={this.state.email}
              onChangeText={(email) => this.setState({ email })}
              ref={(input) => { this.email = input; }}
              
            />

           
            <TextField
            label='Synopsis of Idea (250 characters)'
            value={this.state.synopsis}
            multiline={true}
            onChangeText={(synopsis) => this.setState({ synopsis })}
            ref={(input) => { this.confirm_password = input; }}
          
              
          />



            <TextField
            label='More Details of Idea (750 characters)'
            value={this.state.more_details}
            multiline={true}
            onChangeText={(more_details) => this.setState({ more_details })}
            ref={(input) => { this.confirm_password = input; }}
           
          />



            <TextField
            label='How Much Capital Required'
            value={this.state.capital_required}
            maxLength={5}
            autoCapitalize={'none'}
          keyboardType = 'numeric'
          textContentType='telephoneNumber'
            onChangeText={(capital_required) => this.setState({ capital_required })}
            ref={(input) => { this.confirm_password = input; }}
           

              
          />


          <TextField
            label='Minimum Per Investor'
            value={this.state.minimum_investors}
            autoCapitalize={'none'}
          keyboardType = 'numeric'
          textContentType='telephoneNumber'
            maxLength={4}
            onChangeText={(minimum_investors) => this.setState({ minimum_investors })}
            ref={(input) => { this.confirm_password = input; }}
         
              
          />


            <Dropdown
                label='Open To Full Investment'
                data={data}
                rippleCentered={true}
                valueExtractor={({value})=> value}
                onChangeText = {(value, index, data) =>{
                this.setState({fill_investment:value})
                }}
                value={this.state.fill_investment}
        />


            </View>

          

          <TouchableOpacity style={{width:150,margin:10}} onPress={() => this.submit()}>
            <View style={{
              width: '90%', height: 50, backgroundColor: colors.color_primary, justifyContent: 'center',
              alignItems: 'center', alignSelf: 'center', marginTop: 20, borderRadius: 5,
              shadowColor: 'grey',
              shadowRadius: 10,
              shadowOpacity: 1,
              elevation:10

            }}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Submit</Text>
            </View>
          </TouchableOpacity>

        </KeyboardAwareScrollView>


        {this.state.loading_status &&
          <View style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
          ]}>
            <WaveIndicator color={colors.color_primary}/>
          </View>
        }
      </SafeAreaView>

    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // dispatching plain actions
    add: (userinfo) => dispatch(addUser(userinfo)),
  
    
  }
}


export default connect(null, mapDispatchToProps)(AddPitch);

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  buttonLargeContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40
  },
  primaryButton: {
    backgroundColor: '#FF0017',
    height: 45
  },
  buttonText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold'
  },
  containerAct: {
    flex: 1,
    justifyContent: 'center'
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }


});