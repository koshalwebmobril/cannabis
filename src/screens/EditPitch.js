
import React from 'react';
import { colors,urls } from '../Globals';
import { Button,Header, registerCustomIconType } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { Dropdown } from 'react-native-material-dropdown';
import Snackbar from 'react-native-snackbar';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ToastAndroid
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { addUser } from '../actions/actions';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { NavigationActions, StackActions } from 'react-navigation';




class EditPitch extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      title: '',
      email: '',
      pitch_id:'',
      contact: '',
      synopsis: '',
      more_details: '',  
      loading_status:false,
      capital_required:'',
      country_id:'',
      countries:[],
      country_name:'',
      state_id:'',
      states:[],
      state_name:'',
      city_id:'',
      city_name:'',
      cities:[],
      minimum_investors:'',
      fill_investment:''
    };


  }


    
  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
     
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
           <TouchableOpacity onPress={()=>{navigation.goBack()}}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Edit Pitch </Text>
     </View>
    ),
  });

  fetchCountries = async () =>{
    this.setState({loading_status:true})

               let url = urls.base_url +'api/countries'
                    fetch(url, {
                    method: 'POST',

                    }).then((response) => response.json())
                        .then((responseJson) => {

                         // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          this.setState({loading_status:false})
                          if(!responseJson.error){
                           
                            var length = responseJson.data.length.toString();
                            var temp_arr=[]
                            for(var i = 0 ; i < length ; i++){
                            var id = responseJson.data[i].id
                            var name = responseJson.data[i].name
                          
                           
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], name: name };
                                  array[i] = { ...array[i], value : name };

                                  temp_arr = array
                                  
                                  //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                            }
                            this.setState({ countries : temp_arr});

                          }


                        else{
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                          // : Alert.alert(responseJson.message)

                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        }

                      }
                        ).catch((error) => {

                          this.setState({loading_status:false})
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show('Error !', ToastAndroid.SHORT)
                          // : Alert.alert('Error !')

                          Snackbar.show({
                            title: 'Error !',
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        });



  }

  fetchStates = async (countryId) =>{
    this.setState({loading_status:true,state_id:'',city_id:'',city_name:'',state_name:''})

    var formData = new FormData();
    formData.append('country_id', countryId);

               let url = urls.base_url +'api/states'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                    body: formData

                    }).then((response) => response.json())
                        .then((responseJson) => {

                         // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          this.setState({loading_status:false})
                          if(!responseJson.error){
                           
                            var length = responseJson.data.length.toString();
                            var temp_arr=[]
                            for(var i = 0 ; i < length ; i++){
                            var id = responseJson.data[i].id
                            var name = responseJson.data[i].name
                          
                           
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], name: name };
                                  array[i] = { ...array[i], value : name };

                                  temp_arr = array
                                  
                                  //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                            }
                            this.setState({ states : temp_arr});

                          }


                        else{
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                          // : Alert.alert(responseJson.message)

                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        }

                      }
                        ).catch((error) => {

                          this.setState({loading_status:false})
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show('Error !', ToastAndroid.SHORT)
                          // : Alert.alert('Error !')

                          Snackbar.show({
                            title: 'Error !',
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        });



  }
  fetchCities = async (stateId) =>{
    this.setState({loading_status:true,city_id:'',city_name:''})
    var formData = new FormData();
    formData.append('state_id',stateId);

    formData.append('country_id',this.state.country_id);


               let url = urls.base_url +'api/cities'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                    body: formData

                    }).then((response) => response.json())
                        .then((responseJson) => {

                         // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          this.setState({loading_status:false})
                          if(!responseJson.error){
                           
                            var length = responseJson.data.length.toString();
                            var temp_arr=[]
                            for(var i = 0 ; i < length ; i++){
                            var id = responseJson.data[i].id
                            var name = responseJson.data[i].name
                          
                           
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], name: name };
                                  array[i] = { ...array[i], value : name };

                                  temp_arr = array
                                  
                                  //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                            }
                            this.setState({ cities : temp_arr});

                          }


                        else{
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                          // : Alert.alert(responseJson.message)

                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        }

                      }
                        ).catch((error) => {

                          this.setState({loading_status:false})
                          // Platform.OS === 'android'
                          // ? ToastAndroid.show('Error !', ToastAndroid.SHORT)
                          // : Alert.alert('Error !')

                          Snackbar.show({
                            title: 'Error !',
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                         
                          });
                        });



  }

  componentDidMount() {
    this.fetchCountries()
    console.log('hi my data is === >>>>>>>',this.props.navigation.getParam('result')) ;
    this.setState({
        title: this.props.navigation.getParam('result').pitch_title,
        email: this.props.navigation.getParam('result').contact_email,
        pitch_id:this.props.navigation.getParam('result').id,
        contact: this.props.navigation.getParam('result').contact_no,
        synopsis: this.props.navigation.getParam('result').synopsis_of_idea,
        more_details: this.props.navigation.getParam('result').more_detail_of_idea,  
        capital_required:this.props.navigation.getParam('result').capital_required,
        country_id:this.props.navigation.getParam('result').country_id,
        country_name:this.props.navigation.getParam('result').country.name,
        state_id:this.props.navigation.getParam('result').state_id,
        state_name:this.props.navigation.getParam('result').state.name,
        city_id:this.props.navigation.getParam('result').city_id,
        city_name:this.props.navigation.getParam('result').city.name,
        minimum_investors:this.props.navigation.getParam('result').minimum_per_investor,
        fill_investment:this.props.navigation.getParam('result').open_to_full_investment
    })

    setTimeout(()=>{

        console.log('my  title is------>>>>',this.state.title)

    },2000)

  }





  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }


  isValid() {

    var isnum = /^\d+$/.test(this.state.contact);



    const { title,contact, email, 
      synopsis,more_details,country_id,
      state_id,city_id,capital_required,
      minimum_investors,
    fill_investment } = this.state;

    let valid = false;

    if (title.toString().trim().length > 0 
      && email.toString().trim().length > 0 
      && this.validateEmail(email.trim())
      && contact.toString().trim().length > 7
      &&  synopsis.toString().trim().length < 249
      &&  more_details.toString().trim().length < 749
      &&  minimum_investors.toString().trim().length > 0
      &&  country_id.toString().trim().length > 0
      &&  state_id.toString().trim().length > 0
      &&  city_id.toString().trim().length > 0
      &&  fill_investment.toString().trim().length > 0) {
      valid = true;
    }



    else if (title.toString().trim().length == 0){
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter Cannabis Expression', ToastAndroid.SHORT)
      //   : Alert.alert('Enter Cannabis  Expression')

        Snackbar.show({
          title: 'Enter Pitch Title',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });


      return false;
    }

  
    else if (country_id.toString().trim().length == 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter Country', ToastAndroid.SHORT)
      //   : Alert.alert('Enter Country')

        Snackbar.show({
          title: 'Select Country!',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });
      return false;
    }

    else if (state_id.toString().trim().length == 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter State', ToastAndroid.SHORT)
      //   : Alert.alert('Enter State')

      Snackbar.show({
        title: 'Select State!',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
     
      });


      return false;
    }

    else if (city_id.toString().trim().length == 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter City', ToastAndroid.SHORT)
      //   : Alert.alert('Enter City')

      Snackbar.show({
        title: 'Select City ',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
     
      });
      return false;
    }

  
    else if (contact.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter  Email', ToastAndroid.SHORT)
      //   : Alert.alert('Enter  Email')

        Snackbar.show({
          title: 'Enter  Contact',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });


      return false;
    }
    else if (contact.toString().trim().length < 10) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter  Email', ToastAndroid.SHORT)
      //   : Alert.alert('Enter  Email')

        Snackbar.show({
          title: 'Contact Number be at least 10 characters long',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });


      return false;
    }

    else if(!isnum){

      Snackbar.show({
        title: 'Enter Valid Phone Number',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
     
      });



      return false;
    }
    else if (email.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter  Email', ToastAndroid.SHORT)
      //   : Alert.alert('Enter  Email')

        Snackbar.show({
          title: 'Enter  Email',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });


      return false;
    }
    else if (!this.validateEmail(email.toString().trim())) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter valid Email', ToastAndroid.SHORT)
      //   : Alert.alert('Enter valid Email')

        Snackbar.show({
          title: 'Enter valid Email!',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });


      return false;
    }



    else if (synopsis.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter date of birth', ToastAndroid.SHORT)
      //   : Alert.alert('Enter date of birth')

        Snackbar.show({
          title: 'Enter Synopsis',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });
      return false;
    }
    else if (synopsis.toString().trim().length > 250) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter date of birth', ToastAndroid.SHORT)
      //   : Alert.alert('Enter date of birth')

        // Snackbar.show({
        //   title: 'Synopsis Should be 250 characters long',
        //   duration: Snackbar.LENGTH_SHORT,
        //   backgroundColor:'black',
        //   color:'red',
        //   action: {
        //     title: 'OK',
        //     color: 'green',
        //     onPress: () => { /* Do something. */ },
        //   },
       
        // });

Alert.alert('Synopsis Should be 250 characters long')


      return false;
    }

    else if (more_details.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter date of birth', ToastAndroid.SHORT)
      //   : Alert.alert('Enter date of birth')

        Snackbar.show({
          title: 'Enter More Details',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });

       

      return false;
    }
    else if (more_details.toString().trim().length > 750) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter date of birth', ToastAndroid.SHORT)
      //   : Alert.alert('Enter date of birth')

        // Snackbar.show({
        //   title: 'More Details Should be 750 characters long',
        //   duration: Snackbar.LENGTH_SHORT,
        //   backgroundColor:'black',
        //   color:'red',
        //   action: {
        //     title: 'OK',
        //     color: 'green',
        //     onPress: () => { /* Do something. */ },
        //   },
       
        // });

        Alert.alert('More Details Should be 750 characters long')

      return false;
    }
    else if (capital_required.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter password', ToastAndroid.SHORT)
      //   : Alert.alert('Enter password')

        Snackbar.show({
          title: 'Enter required capital',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });
      return false;
    }
    else if (minimum_investors.toString().trim().length ==  0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Password should be 8-16 characters long', ToastAndroid.SHORT)
      //   : Alert.alert('Password should be 8-16 characters long')

        Snackbar.show({
          title: 'Enter Minimum number of Investors',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
       
        });
      return false;
    }
   
  
    else if (fill_investment.toString().trim().length == 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter City', ToastAndroid.SHORT)
      //   : Alert.alert('Enter City')

      Snackbar.show({
        title: 'Select Open to Fill Investment',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
     
      });
      return false;
    }
    

    return valid
  }


  submit(){
   console.log('hihihihihihiihihihihihihih',this.state.pitch_id)
      if(this.isValid()){
        AsyncStorage.getItem("user_id").then((item) => {
          if (item) {
      
            var formData = new FormData();
      
            formData.append('user_id',item);
            formData.append('pitch_id',this.state.pitch_id);
            formData.append('pitch_title',this.state.title);
            formData.append('country_id', this.state.country_id);
            formData.append('city_id', this.state.city_id);
            formData.append('state_id', this.state.state_id);
            formData.append('contact_no', this.state.contact);
            formData.append('contact_email', this.state.email);
            formData.append('synopsis_of_idea', this.state.synopsis);
            formData.append('more_detail_of_idea', this.state.more_details);
            formData.append('capital_required', this.state.capital_required);
            formData.append('minimum_per_investor', this.state.minimum_investors);
            formData.append('open_to_full_investment', this.state.fill_investment);
     
            
      
                          this.setState({loading_status:true})
                          let url = urls.base_url +'api/update_pitch'
                          fetch(url, {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData
      
                        }).then((response) => response.json())
                              .then((responseJson) => {
                      this.setState({loading_status:false})
                      console.log(JSON.stringify(responseJson))
              
    
    
                                    Snackbar.show({
                                      title: responseJson.message,
                                      duration: Snackbar.LENGTH_SHORT,
                                      backgroundColor:'black',
                                      color:'red',
                                      action: {
                                        title: 'OK',
                                        color: 'green',
                                        onPress: () => { /* Do something. */ },
                                      },
                                    });
      
    
                                  if(!responseJson.error){
                                    this.props.navigation.navigate('InvestmentHub');
                      
                                    const resetAction = StackActions.reset({
                                    index: 0,
                                    key: 'InvestmentHub',
                                    actions: [NavigationActions.navigate({ routeName: 'InvestmentHub' })],
                                  });
                
                                  this.props.navigation.dispatch(resetAction);
 
                                  // this.props.navigation.navigate("Home")
                                }
                                else{
      
                                    
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})
                                console.log('error is ',error)
                                Snackbar.show({
                                  title: 'Try Again !',
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });
      
                              });
          }
          else{
            ToastAndroid.show("User not found !",ToastAndroid.LONG)
          }
        });
      }
  }

  backBtn() {
    this.props.navigation.goBack();
  }


  login() {
    this.props.navigation.navigate('Login')
  }

  render() {

    let data = [{
      value: 'Yes',
    }, {
      value: 'No',
    }];

    return (
      <SafeAreaView style={styles.container}>


        <KeyboardAwareScrollView style={{backgroundColor:'white',width:'100%',flex:1}}
        showsVerticalScrollIndicator={false}
       contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>


          <View style={{ width: '90%', justifyContent: 'center', alignSelf: 'center' ,marginBottom:30}}>


              <TextField
              label='Pitch Title'
            //  value={this.state.title}
              value={this.props.navigation.getParam('result').pitch_title}
              onChangeText={(title) => this.setState({ title})}
              ref={(input) => { this.title = input; }}
            
            />

          <Dropdown
          label='Select Your Country'
          rippleCentered={true}
          valueExtractor={({value})=> value}
          onChangeText = {(value, index, data) =>{
            console.log(`Selected value: ${value} ${index} ${data[index].id}`)
           // value is text name or label
           //index is index of item
           //data[index].id is id of the country
           this.setState({country_id:data[index].id,country_name:data[index].name})
           this.fetchStates(data[index].id)

          }}
          data={this.state.countries}
          value={this.state.country_name}
          ref={(input) => { this.country = input; }}
        />

        <Dropdown
          label='Select Your State'
          data={this.state.states}
          rippleCentered={true}
          valueExtractor={({value})=> value}
          onChangeText = {(value, index, data) =>{
           this.setState({state_id:data[index].id,state_name:data[index].name})
           this.fetchCities(data[index].id)

          }}
          value={this.state.state_name}
        />
        <Dropdown
          label='Select Your City'
          data={this.state.cities}
          rippleCentered={true}
          valueExtractor={({value})=> value}
         
          onChangeText = {(value, index, data) =>{
           this.setState({city_id:data[index].id,city_name:data[index].name})

          }}
          value={this.state.city_name}
        />


            <TextField
              label='Contact Number'
             // value={this.state.contact}
              value={this.props.navigation.getParam('result').contact_no}
              autoCapitalize={'none'}
                keyboardType = 'numeric'
                textContentType='telephoneNumber'
                onChangeText={(contact) => {
                  this.setState({ contact})
                }}
              ref={(input) => { this.contact = input; }}
            
            />



            <TextField
              label='Contact Email'
              keyboardType={Platform.OS === 'android' ? 'email-address' : 'ascii-capable'}
             // value={this.state.email}
              value={this.props.navigation.getParam('result').contact_email}
              onChangeText={(email) => this.setState({ email })}
              ref={(input) => { this.email = input; }}
              
            />

           

            <TextField
            label='Synopsis of Idea (250 characters)'
            //value={this.state.synopsis}
            value={this.props.navigation.getParam('result').synopsis_of_idea}
            multiline={true}
            onChangeText={(synopsis) => this.setState({ synopsis })}
            ref={(input) => { this.confirm_password = input; }}
          
              
          />



            <TextField
            label='More Details of Idea (750 characters)'
           // value={this.state.more_details}
            value={this.props.navigation.getParam('result').more_detail_of_idea}
            multiline={true}
            onChangeText={(more_details) => this.setState({ more_details })}
            ref={(input) => { this.confirm_password = input; }}
           
          />



            <TextField
            label='How Much Capital Required'
           // value={this.state.capital_required}
            value={this.props.navigation.getParam('result').capital_required}
            maxLength={5}
            autoCapitalize={'none'}
          keyboardType = 'numeric'
          textContentType='telephoneNumber'
            onChangeText={(capital_required) => this.setState({ capital_required })}
            ref={(input) => { this.confirm_password = input; }}
           

              
          />


          <TextField
            label='Minimum Per Investors'
          //  value={this.state.minimum_investors}
            value={this.props.navigation.getParam('result').minimum_per_investor}
            autoCapitalize={'none'}
          keyboardType = 'numeric'
          textContentType='telephoneNumber'
            maxLength={4}
            onChangeText={(minimum_investors) => this.setState({ minimum_investors })}
            ref={(input) => { this.confirm_password = input; }}
          />


            <Dropdown
                label='Open To Full Investment'
                data={data}
                rippleCentered={true}
                valueExtractor={({value})=> value}
                onChangeText = {(value, index, data) =>{
                this.setState({fill_investment:value})
                }}
                value={this.state.fill_investment}
        />


            </View>

          

          <TouchableOpacity style={{width:'100%',margin:10}} onPress={() => this.submit()}>
            <View style={{
              width: '90%', height: 50, backgroundColor: colors.color_primary, justifyContent: 'center',
              alignItems: 'center', alignSelf: 'center', marginTop: 20, borderRadius: 5,
              shadowColor: 'grey',
              shadowRadius: 10,
              shadowOpacity: 1,
              elevation:10

            }}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Submit</Text>
            </View>
          </TouchableOpacity>

        

        
         

        </KeyboardAwareScrollView>


        {this.state.loading_status &&
          <View style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
          ]}>
            <WaveIndicator color={colors.color_primary}/>
          </View>
        }
      </SafeAreaView>

    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // dispatching plain actions
    add: (userinfo) => dispatch(addUser(userinfo)),
  
    
  }
}


export default connect(null, mapDispatchToProps)(EditPitch);

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  buttonLargeContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40
  },
  primaryButton: {
    backgroundColor: '#FF0017',
    height: 45
  },
  buttonText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold'
  },
  containerAct: {
    flex: 1,
    justifyContent: 'center'
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }


});