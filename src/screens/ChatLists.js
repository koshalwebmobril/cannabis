import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import firebase from 'react-native-firebase';
import { addUser, changeUser } from '../actions/actions';
import { GiftedChat } from 'react-native-gifted-chat';

import firebaseSDK from '../config/firebaseSDK';

import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {WaveIndicator} from 'react-native-indicators';



class ChatLists extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      lists: [],
      user_id:0

    };


  }

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,

          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

       <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}


       </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                 <TouchableOpacity onPress={()=>{ navigation.navigate(navigation.getParam('route_name','Home')) }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Vault Chat Lists</Text>
     </View>
    ),
  });



  makeList(childData){
         console.log("my state",this.state.lists);
          let oldList = [...this.state.lists];

          oldList.push(childData);

          this.setState({lists:oldList},()=>{

            console.log("new list ",this.state.lists);
          })
  }
  initialize(id){

    this.setState({loading_status:true})




      //It will read data once from the `Users` object and print it on console.
      //If we want to get data whenever there is any change in it,
      //we can use on instead of once
    firebase.database().ref('recents/').orderByChild('timestamp').once('value',  (snapshot)=> {
// console.log("SNAPSHOT",snapshot.val())
        snapshot.forEach((childSnapshot) =>{
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
      // Display the data
      //  console.log("childKey",childKey)//5_22
      //   console.log("childData",childData)// {"last_message": "Scdscsdc",
        //"timestamp": 1576000309991}

        var str = childKey;
        var res = str.split("_");//5,22

        //console.log("childKey-----------",res)


        if(res[0] == id || res[1] == id){
         // console.log("YESSSSSS-----------",res)

         this.setState(previousState => ({
          lists: [...previousState.lists, childData]
      }));

              // this.setState({
              //   lists:childData
              // },()=>console.log("STATE-----------",JSON.stringify(this.state.lists)))

        }



        this.setState({loading_status:false})

      });
     // console.log("STATE-----------",JSON.stringify(this.state.lists))




  }, (error) => {
   // console.log("ERRROR-----------",error)
});





///

firebase.database().ref('recents/').orderByChild('timestamp').on('child_changed',  (snapshot)=> {

    //  console.log('Name of the field/node that was modified',snapshot.key);
    // console.log('Value of the field/node that was modified',snapshot.val());
    // console.log("The updated " +
    // snapshot.key + " is " + JSON.stringify(snapshot.val()));

    var str = snapshot.key
    var res = str.split("_");

    if(res[0] == this.props.user.user_id || res[1] == this.props.user.user_id){

        var index = this.state.lists.findIndex(x => (

        (x.receiver_id == res[0]  &&  x.sender_id == res[1])
        ||  (x.sender_id == res[0]  &&  x.receiver_id == res[1])
        ));

        //console.log("INDEX",index)

        const { lists } = this.state
        var tempArr = [...lists]
        tempArr[index] = snapshot.val()
        this.setState({lists:tempArr})

        snapshot.forEach((childSnapshot) =>{

        });

    }

}, (error) => {
//  console.log(error)
});


  }





  write(){

    // firebase.database().ref(`/users/${firebase.auth().currentUser.uid}/some/`)
    // .push((item, error) => {
    //     if (!error){
    //       console.log("Item added to firebase");
    //     }

    //     else{
    //       console.warn("There was an error writing to the database, error)
    //     }

    //   }
    // });


  }


  componentWillMount(){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {

        this.setState({user_id:item},this.initialize(item))
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  //this.initialize()

  }

  componentWillUnmount() {


    firebase.database().ref('recents/').off('child_changed');

}





next(item){
  item.receiver_id == this.props.user.user_id ? item.sender_image : item.receiver_image

  let obj={
    'to_id':this.props.user.user_id  == item.receiver_id ? item.sender_id : item.receiver_id,
    'to_name':this.props.user.user_id == item.receiver_id? item.sender_name : item.receiver_name,
    'to_image':this.props.user.user_id  == item.receiver_id? item.sender_image : item.receiver_image,
    'to_status':'offline',
    'to_typing':false
  }
  this.props.navigation.navigate("Chats",{result:obj})

}

checkType(item){
  if(item.msg_type == 'text'){
    return (

      <Text style={{color:'grey'}}>{item.last_message.length > 5 ? 
        item.last_message.substring(0,6)   : 
        item.last_message }</Text>
    )
  }
  else if(item.msg_type == 'image'){
      return (
        <View style={{flexDirection:'row',
                                  alignItems:'center',marginTop:5}}>

                                    <Image source={require('../assets/attachment.png')
                                    } style={{width:10,height:10,marginRight:10}} resizeMode='contain'/>

                                    <Text style={{color:'grey'}}>Image </Text>


                                  </View>
      )
  }
  else if(item.msg_type == 'audio'){
    return (
      <View style={{flexDirection:'row',
      alignItems:'center',marginTop:5}}>

        <Image source={require('../assets/voice-black.png')
        } style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>

        <Text style={{color:'grey'}}>Audio </Text>


      </View>
    )
}
}

renderItem = ({item}) =>  {


  const a = urls.base_url
  const b = this.props.user.user_id  == item.receiver_id
  ? item.sender_image
  : item.receiver_image

  //BELOW code is for typing features
  const rId = item.receiver_id
  const sId = item.sender_id

  let typeStatus = false
  let id = 0
  if(rId == this.props.user.user_id){
    //means recieverr id was my id
    id = sId

  }
  else if(sId == this.props.user.user_id){
    //means sender id was my id
    id = rId
  }

  let key = `${id}_typing`

  //END



  const imageUrl = a + b
  //console.log('ssss',imageUrl)

  return(
    <TouchableOpacity onPress={()=>{this.next(item)}}>
      <View style={{width:Dimensions.get('window').width * 0.98,
      padding:6,marginBottom:3,marginTop:3,marginLeft:4,marginRight:4,flexDirection:'row',
      justifyContent:'space-between',alignItems:'center',padding:6}}>


                  <View style={{height:null,flexDirection:'row',
                    justifyContent:'flex-start',alignItems:'center',
                    width:null}}>



                            <Image source={{uri:imageUrl}}
                            style={{width:40,height:40,borderRadius:20,overflow:'hidden'}} 
                            resizeMode='cover'/>




                            <View style={{marginLeft:20}}>

                              <Text style={{marginLeft:0,fontWeight:'bold',fontSize:16}}>{
                                this.props.user.user_id == item.receiver_id
                                ? item.sender_name
                                : item.receiver_name
                                }</Text>

                                {
                                  item[key] ?
                                  <Text style={{fontWeight:'700',color:'green'}}>typing..</Text>
                                  :
                                 this.checkType(item)


                                }


                                                    </View>






                    </View>



                    <Text style={{color:'black',
                         marginLeft:3,marginRight:15,
                         alignSelf:'flex-end'}}>{item.time}</Text>




       </View>

        <View
        style={{
          height: 1,
          width: "85%",
          backgroundColor: "#d3d3d3",
          marginBottom:1,marginTop:1,marginLeft:40,marginRight:1
        }}
      />




      </TouchableOpacity>
  )
 }

renderItems = ({item}) =>  {


  const a = urls.base_url
  const b = this.props.user.user_id  == item.receiver_id
  ? item.sender_image
  : item.receiver_image


  const imageUrl = a + b
  //console.log('ssss',imageUrl)

  return(
    <TouchableOpacity onPress={()=>{this.next(item)}}>
      <View style={{width:Dimensions.get('window').width*1,
      padding:6,marginBottom:2,marginTop:2,marginLeft:4,marginRight:4}}>

       <View style={{width:'100%',height:null,flexDirection:'row',
      justifyContent:'space-around',alignItems:'center',padding:15}}>

                  <View style={{height:null,flexDirection:'row',
                    justifyContent:'flex-start',alignItems:'center',
                    width:Dimensions.get('window').width*0.8}}>
                            <Image source={{uri:imageUrl}}
                            style={{width:30,height:30,borderRadius:15,overflow:'hidden'}}
                            resizeMode='cover'/>


                   <View>

                              <Text style={{marginLeft:15,fontWeight:'bold'}}>{
                                this.props.user.user_id == item.receiver_id
                                ? item.sender_name
                                 : item.receiver_name
                                 }</Text>

                                 {
                                   item.msg_type == 'text'
                                   ?
                                   <Text style={{marginLeft:15,color:'grey'}}>{item.last_message}</Text>
                                   :
                                   <View style={{flexDirection:'row',
                                   alignItems:'center',marginLeft:15}}>

                                    <Image source={require('../assets/attachment.png')
                                    } style={{width:10,height:10,marginRight:10}} resizeMode='contain'/>

                                    <Text style={{marginLeft:0,color:'grey'}}>Image </Text>


                                   </View>



                                 }

                              </View>



                    </View>



                    <Text style={{color:'black',
                         marginLeft:3,marginRight:15,
                         alignSelf:'flex-end'}}>{item.time}</Text>




       </View>

        <View
        style={{
          height: 1,
          width: "82%",
          backgroundColor: "#d3d3d3",
          marginBottom:1,marginTop:1,marginLeft:40,marginRight:10
        }}
      />


     </View>

      </TouchableOpacity>
  )
 }

 onContentOffsetChanged = (distanceFromTop: number) => {
  distanceFromTop === 0 && console.log("TOP")

}



    render()
    {




        return(
          <SafeAreaView style={styles.container}>



            <View style={{backgroundColor:'white',width:'100%',
            flex:1,justifyContent:'center',alignItems:'center'}}
            >








           {
             this.state.lists.length > 0
             ?


             <FlatList
                           style={{flex:1}}
                           data={this.state.lists}
                            numColumns={1}
                            disableVirtualization={false}
                            onScroll={
                                (event: NativeSyntheticEvent<NativeScrollEvent>) =>
                                    this.onContentOffsetChanged(event.nativeEvent.contentOffset.y)
                            }
                            ref={ (ref) => { this.myFlatListRef = ref } }
                           onContentSizeChange={ () => { this.myFlatListRef.scrollToEnd({animated:true}) } }

                            onLayout={ () => { this.myFlatListRef.scrollToEnd({animated:true}) } }
                            onEndReached={()=> console.log("Bottom")}
                            showsVerticalScrollIndicator={false}
                           renderItem={this.renderItem}

                         />
            :
            this.state.loading_status ?
            null
            :

                <View style={{justifyContent:'center',alignItems:'center',height:'90%'}}>
            <Image source={require('../assets/no-chat.png')}
              style={{width:200,height:200,alignSelf:'center',marginTop:Dimensions.get('window').height * 0.02}} resizeMode='contain'/>
              </View>


           }




            </View>


            {this.state.loading_status &&
              <View pointerEvents="none" style={styles.loading}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }

          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,

	};
};

const mapDispatchToProps = dispatch => {
  return {

    change: (userinfo) => dispatch(changeUser(userinfo)),


  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ChatLists);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },

  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });
