import React from 'react';
import { colors,urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import Snackbar from 'react-native-snackbar';

import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Image,
    TouchableOpacity,
    Alert,ActivityIndicator,ToastAndroid
} from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';

const initialState ={
  otp1:'',
  otp2:'',
  otp3:'',
  otp4:'',
  otp5:'',
  otp6:'',
  loading_status:false,
}

export default class Otp extends React.Component {

 

  constructor(props) {
    super(props);
    this.state = {
      otp1:'',
      otp2:'',
      otp3:'',
      otp4:'',
      otp5:'',
      otp6:'',
      loading_status:false,
  };
}


static navigationOptions  = ({ navigation }) => ({
  headerTitleStyle: { 
    color : 'black', 
    flex: 1,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 17,
  
    
},
headerLayoutPreset: 'center',
  //he back button and title both use this property as their color.
  headerTintColor: 'royalblue',
  headerStyle: {
    backgroundColor: 'white',
  
        shadowOffset: {
    width: 0,
    height: 3,
},
shadowOpacity: 0.5,//for ios
shadowRadius: 5,
elevation:10,//for android
  },
// headerTitle: () => <Text>Login</Text>,
title:"OTP",

  headerLeft: () => (
    <TouchableOpacity onPress={()=>{
     
     navigation.goBack()
    }}>
      <Image source={require('../assets/left-arrow.png')} style={{width:20,height:20,marginLeft:15}} resizeMode='contain'/>
      
      </TouchableOpacity>
  ),
  headerRight: () => (
    <View></View>
  ),
});

isValid() {

  var result  = this.props.navigation.getParam('result')
  var email = result["email"]
  // ToastAndroid.show(email,ToastAndroid.SHORT)
  
  var isnumotp1 = /^\d+$/.test(this.state.otp1);
  var isnumotp2 = /^\d+$/.test(this.state.otp2);
  var isnumotp3 = /^\d+$/.test(this.state.otp3);
  var isnumotp4 = /^\d+$/.test(this.state.otp4);

  let valid = false;

  if (this.state.otp1.trim().length > 0 &&
     this.state.otp2.trim().length > 0 &&
     this.state.otp3.trim().length > 0 &&
     this.state.otp4.trim().length > 0 && 
     this.state.otp5.trim().length > 0 &&
     this.state.otp6.trim().length > 0 ) {
       valid = true;
       return true;
  }

 
 if(this.state.otp1.toString().trim().length === 0
 || this.state.otp2.toString().trim().length === 0
 || this.state.otp3.toString().trim().length === 0
 || this.state.otp4.toString().trim().length === 0
 || this.state.otp5.toString().trim().length === 0
 || this.state.otp6.toString().trim().length === 0){


  Snackbar.show({
    title: 'Enter OTP',
    duration: Snackbar.LENGTH_SHORT,
    backgroundColor:'black',
    color:'red',
    action: {
      title: 'OK',
      color: 'green',
      onPress: () => { /* Do something. */ },
    },
  });


    return false;

  } 
 

  //return valid;
}



onOTP() {
 // this.setState({otp1:6,otp2:7,otp3:8})

if (this.isValid()) {


 this.setState({loading_status:true})

 var result  = this.props.navigation.getParam('result')
  var email = result["email"]


 var formData = new FormData();

 var otp = this.state.otp1 +  this.state.otp2 + this.state.otp3 + this.state.otp4+ this.state.otp5 + this.state.otp6

  formData.append('email', email);
  formData.append('otp', otp);

 

  
 // ToastAndroid.show(JSON.stringify(formData),ToastAndroid.LONG)

          let url = urls.base_url +'api/verify_otp'
          fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
          },
          body: formData

        }).then((response) => response.json())
              .then((responseJson) => {
                
                  this.setState({loading_status:false, otp1:'',
                  otp2:'',
                  otp3:'',
                  otp4:'',
                  otp5:'',
                  otp6:''})

                  // this.setState(prevState =>({
                  //     loading_status : !prevState.loading_status,
                  //   //  otp1 :"",


                  // }),()=>{
                  //   console.log("otp1",this.state.otp1);
                  // })
            
             //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);

              if(!responseJson.error){

                      let obj ={
                        'email':email
                      }
               

                    this.props.navigation.navigate("ResetPassword",{result:obj})


                  }else{
                   
                    Snackbar.show({
                      title: responseJson.message,
                      duration: Snackbar.LENGTH_SHORT,
                      backgroundColor:'black',
                      color:'red',
                      action: {
                        title: 'OK',
                        color: 'green',
                        onPress: () => { /* Do something. */ },
                      },
                    });
                      this.setState({
                        otp1:'',
                        otp2:'',
                        otp3:'',
                        otp4:'',
                        otp5:'',
                        otp6:''})
                      
                     

                    }

              }).catch((error) => {
                        this.setState({loading_status:false})

                        Snackbar.show({
                          title: 'Try Again !',
                          duration: Snackbar.LENGTH_SHORT,
                          backgroundColor:'black',
                          color:'red',
                          action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                          },
                        });

              });


 }

}



      
      backBtn()
      {
        this.props.navigation.goBack()
      }
    
    render() {
     

        return (
          <SafeAreaView style={styles.container}>


    
    
            <ScrollView style={{backgroundColor:'white',width:'100%',flex:1,padding:20}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>
    
             
              <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                      <Image style={{ height: 160, width: 160, marginTop: 30 }}
                        source={require('../assets/logo.png')}
                        resizeMode='contain'
                      ></Image>
              </View>

              <Text style={{color:'grey',textAlign:'center',marginTop:30}}>OTP has to send your registered email address.
               Please enter OTP to reset your password.</Text>
    
    
    
   <View style={{height:null,width:'100%',backgroundColor:'white',
        flexDirection:'row',justifyContent:'center',alignSelf:'center'}}>
      <View style={{flex:1,justifyContent:'center',alignSelf:'center',margin:5}}>
      <TextField 
     ref={(input) => { this.otp1 = input; }}
            value={this.state.otp1}
            maxLength={1}
            onChangeText={ (otp1) => {
              
              
              // this.setState({otp1:otp1.replace(/[^0-9.]/g, '')},()=>{
              //   console.log("check otp length",otp1);
              // })
              otp1.length ==1 ? this.otp2.focus() : ""
              this.setState({otp1:otp1},()=>{
                console.log("check otp length",otp1);
              })
            }}
            onKeyPress={e =>{ 
                    if(e.nativeEvent.key == 'Backspace'){
                     
                     this.setState({otp1:''})
                    }
                    }} 
            
            autoCorrect={false}
            autoCapitalize={'none'}
            keyboardType = 'numeric'
            textContentType='telephoneNumber'
            style={styles.input}
          />
      </View>

      <View style={{flex:1,justifyContent:'center',alignSelf:'center',margin:5}}>
      <TextField 
          value={this.state.otp2}
          maxLength={1}
          ref={(input) => { this.otp2 = input; }}
        
          onChangeText={ (otp2) => {
            otp2.length ==1 ? this.otp3.focus() : null
            this.setState({otp2:otp2.replace(/[^0-9.]/g, '')})
          }}
          onKeyPress={e =>{ 
                    if(e.nativeEvent.key == 'Backspace'){
                     this.otp1.focus();
                     this.setState({otp2:''})
                    }
                    }} 
        
          autoCorrect={false}
          autoCapitalize={'none'}
          keyboardType = 'numeric'
          textContentType='telephoneNumber'
          style={styles.input}
          />
      </View>

      <View style={{flex:1,justifyContent:'center',alignSelf:'center',margin:5}}>
      <TextField 
            value={this.state.otp3}
            maxLength={1}
            ref={(input) => { this.otp3 = input; }}
            onChangeText={ (otp3) => {
              otp3.length ==1 ? this.otp4.focus() : null
              this.setState({otp3:otp3.replace(/[^0-9.]/g, '')})
            }}
            onKeyPress={e =>{ 
                    if(e.nativeEvent.key == 'Backspace'){
                     this.otp2.focus();
                     this.setState({otp3:''})
                    }
                    }} 
          
            autoCorrect={false}
            autoCapitalize={'none'}
            keyboardType = 'numeric'
            textContentType='telephoneNumber'
            style={styles.input}
          />
      </View>

      <View style={{flex:1,justifyContent:'center',alignSelf:'center',margin:5}}>
      <TextField 
            value={this.state.otp4}
            maxLength={1}
            ref={(input) => { this.otp4 = input; }}
            onChangeText={ (otp4) => {
              otp4.length ==1 ? this.otp5.focus() : null
              this.setState({otp4:otp4.replace(/[^0-9.]/g, '')})
            }}
            onKeyPress={e =>{ 
                    if(e.nativeEvent.key == 'Backspace'){
                     this.otp3.focus();
                     this.setState({otp4:''})
                    }
                    }} 
          
            autoCorrect={false}
            autoCapitalize={'none'}
            keyboardType = 'numeric'
            textContentType='telephoneNumber'
            style={styles.input}
          />
      </View>

      <View style={{flex:1,justifyContent:'center',alignSelf:'center',margin:5}}>
      <TextField 
              value={this.state.otp5}
              maxLength={1}
              ref={(input) => { this.otp5 = input; }}
              onChangeText={ (otp5) => {
                otp5.length ==1 ? this.otp6.focus() : null
                this.setState({otp5:otp5.replace(/[^0-9.]/g, '')})
              }}
              onKeyPress={e =>{ 
                    if(e.nativeEvent.key == 'Backspace'){
                     this.otp4.focus();
                     this.setState({otp5:''})
                    }
                    }} 
            
              autoCorrect={false}
              autoCapitalize={'none'}
              keyboardType = 'numeric'
              textContentType='telephoneNumber'
              style={styles.input}
          />
      </View>

      <View style={{flex:1,justifyContent:'center',alignSelf:'center',margin:5}}>
      <TextField 
      value={this.state.otp6}
      maxLength={1}
      ref={(input) => { this.otp6 = input; }}
      onChangeText={ (otp6) => {
       
        this.setState({otp6:otp6.replace(/[^0-9.]/g, '')})
       // otp6.length ==1 ? this.onOTP() : null
      }}
      onKeyPress={e =>{ 
                    if(e.nativeEvent.key == 'Backspace'){
                     this.otp5.focus();
                     this.setState({otp6:''})
                    }
                    }} 
    
      autoCorrect={false}
      autoCapitalize={'none'}
      keyboardType = 'numeric'
      textContentType='telephoneNumber'
      style={styles.input}
          />
      </View>
        </View>
                 
                 
    
    
              <TouchableOpacity style={{width:'100%'}} onPress={() => this.onOTP()}>
                <View style={{
                  width: '100%', height: 50, backgroundColor: colors.color_primary, justifyContent: 'center',
                  alignItems: 'center', alignSelf: 'center', marginTop: 20, borderRadius: 5,
                  shadowColor: 'gray',
                  shadowRadius: 10,
                  shadowOpacity: 1,
                  elevation:10
    
                }}>
                  <Text style={{ color: 'white', fontWeight: 'bold' }}>Submit</Text>
                </View>
              </TouchableOpacity>
    
             
    
            </ScrollView>
    
    
            {this.state.loading_status &&
              <View style={[
                StyleSheet.absoluteFill,
                { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
              ]}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
          </SafeAreaView>
    
        )
      }
    }
    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
      },
      input:{
        flex:1,height:40,
        width:'100%',
        textAlign:'center'
      },
      loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(128,128,128,0.5)'
      }

    });