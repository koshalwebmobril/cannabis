import React from 'react';
import { colors,urls } from '../Globals';
import { Button ,Header} from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import Snackbar from 'react-native-snackbar';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Image,
    TouchableOpacity,
    Alert,
    ActivityIndicator,
    ToastAndroid
} from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';

export default class Forgot extends React.Component {

   

    constructor(props) {
        super(props);
        this.state = {
            email:'',
            loading_status: false    
        }
        ;
    }

    static navigationOptions  = ({ navigation }) => ({
      headerTitleStyle: { 
        color : 'black', 
        flex: 1,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 17,
      
        
    },
    headerLayoutPreset: 'center',
      //he back button and title both use this property as their color.
      headerTintColor: 'royalblue',
      headerStyle: {
        backgroundColor: 'white',
      
            shadowOffset: {
        width: 0,
        height: 3,
    },
    shadowOpacity: 0.5,//for ios
    shadowRadius: 5,
    elevation:10,//for android
      },
   // headerTitle: () => <Text>Login</Text>,
  title:"Forgot Password",
    
      headerLeft: () => (
        <TouchableOpacity onPress={()=>{
         
         navigation.goBack()
        }}>
          <Image source={require('../assets/left-arrow.png')} style={{width:20,height:20,marginLeft:15}} resizeMode='contain'/>
          
          </TouchableOpacity>
      ),
      headerRight: () => (
        <View></View>
      ),
    });

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    isValid()
    {
        const {email} = this.state;
        var valid = false
        if (email.length > 0 && this.validateEmail(email.trim())) {
            valid = true;
          }
          else if (email.trim().length === 0) {
            // Platform.OS === 'android' 
            // ?  ToastAndroid.show('Enter an email', ToastAndroid.SHORT)
            // : Alert.alert('Enter an email')

            Snackbar.show({
              title: 'Enter an email',
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });
            return false;
          }
          else if(!this.validateEmail(email.trim())){
            // Platform.OS === 'android' 
            // ?  ToastAndroid.show('Enter valid email', ToastAndroid.SHORT)
            // : Alert.alert('Enter valid email')

            Snackbar.show({
              title: 'Enter valid email',
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });
            return false;
          }
          return valid
    }

    componentDidMount() {
    }

    backBtn(){
        this.props.navigation.goBack();
    }

    submit(){

        if (this.isValid())
        {
            this.setState({loading_status:true})
            var formData = new FormData();
           
             formData.append('email', this.state.email);

          
                     let url = urls.base_url +'api/forgot_password'
                     fetch(url, {
                     method: 'POST',
                     headers: {
                       'Accept': 'application/json',
                       'Content-Type': 'multipart/form-data',
                     },
                     body: formData
                   }).then((response) => response.json())
                         .then((responseJson) => {
                             this.setState({loading_status:false})
                      
                        if(!responseJson.error){

                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                          });

                          let obj ={
                            "email" : this.state.email
                          }
                        this.props.navigation.navigate("Otp",{result : obj})
                        } 
                        else{
                              
                                //  Platform.OS === 'android' 
                                //  ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                //  : Alert.alert(responseJson.message)

                                 Snackbar.show({
                                  title: responseJson.message,
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });
                               }
                         }).catch((error) => {
                                   this.setState({loading_status:false})
                                  //  Platform.OS === 'android' 
                                  //  ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                                  //  : Alert.alert("Try Again !")

                                  Snackbar.show({
                                    title: 'Try Again !',
                                    duration: Snackbar.LENGTH_SHORT,
                                    backgroundColor:'black',
                                    color:'red',
                                    action: {
                                      title: 'OK',
                                      color: 'green',
                                      onPress: () => { /* Do something. */ },
                                    },
                                  });
                         });
      
        }
    }

   
    render() {
        let { email } = this.state;

        return (
          <SafeAreaView style={styles.container}>


        
    
    
            <ScrollView style={{backgroundColor:'white',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>
    
             
              <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                      <Image style={{ height: 160, width: 160, marginTop: 30 }}
                        source={require('../assets/logo.png')}
                        resizeMode='contain'
                      ></Image>
              </View>
    
    
    
          
              <View style={{ width: '90%', justifyContent: 'center', alignSelf: 'center' ,marginBottom:20}}>
                <TextField
                  label='Email Address'
                  value={this.state.email}
                  onChangeText={(email) => this.setState({ email })}
                  onSubmitEditing = {() => this.submit()}
                />
    
              
              </View>
    
    
                 
                 
    
    
              <TouchableOpacity style={{width:'100%'}} onPress={() => this.submit()}>
                <View style={{
                  width: '90%', height: 50, backgroundColor: colors.color_primary, justifyContent: 'center',
                  alignItems: 'center', alignSelf: 'center', marginTop: 20, borderRadius: 5,
                  shadowColor: 'gray',
                  shadowRadius: 10,
                  shadowOpacity: 1,
                  elevation:10
    
                }}>
                  <Text style={{ color: 'white', fontWeight: 'bold' }}>Submit</Text>
                </View>
              </TouchableOpacity>

              <Text></Text>
              
    
             
    
            </ScrollView>
    
    
            {this.state.loading_status &&
              <View style={[
                StyleSheet.absoluteFill,
                { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
              ]}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
          </SafeAreaView>
    
        )
    }
}

const styles = StyleSheet.create(
    {
        container: {
            flex: 1
        },
        loading: {
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'rgba(128,128,128,0.5)'
          }
        
    }
);