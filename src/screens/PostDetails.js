import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { Dropdown } from 'react-native-material-dropdown';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import DateTimePicker from 'react-native-modal-datetime-picker';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback,Keyboard
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import Video from 'react-native-video';
//import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import { throwStatement } from '@babel/types';
import ChatTextInput from '../components/ChatTextInput';
import FastImage from 'react-native-fast-image'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


 class PostDetails extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      smiley_status:false,
      item:null,
       loading_status:false,
       comment:'',
       make_comment:'',
       bottom:0,
       comment_list:[],
       image_width:0,
       image_height:0,
       showMenu:false,
       userId:'',
     
    };


  }

  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };
  
  hideMenu = () => {
    this._menu.hide();
  };
  
  showMenu = () => {
    this._menu.show();
  };
  

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'black',
    headerStyle: {
      backgroundColor: 'black',
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
      
            
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                 <TouchableOpacity onPress={()=>{navigation.goBack() }}>
                  <FastImage source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode={FastImage.resizeMode.contain}/>
                  </TouchableOpacity>

          
     </View>
    ),
  });




  showReaction(){
    
  }

  hideReaction(){
             
  }

  next(){
    
  }


  getComment(){

     AsyncStorage.getItem("user_id").then((item) => {
       if (item) {
    this.setState({userId:item})
         var formData = new FormData();
         formData.append('post_id',this.state.item.id);
       
                       let url = urls.base_url +'api/get_post_comments'
                       fetch(url, {
                       method: 'POST',
                       headers: {
                         'Accept': 'application/json',
                         'Content-Type': 'multipart/form-data',
                       },
                   body: formData
   
                     }).then((response) => response.json())
                           .then((responseJson) => {
                     this.setState({loading_status:false ,comment:'',make_comment:''})
 
   
   
                               if(!responseJson.error){

                                 this.setState({comment_list:responseJson.data})
                             }
                             else{
   
                              //  Snackbar.show({
                              //    title: responseJson.message,
                              //    duration: Snackbar.LENGTH_SHORT,
                              //    backgroundColor:'black',
                              //    color:'red',
                              //    action: {
                              //      title: 'OK',
                              //      color: 'green',
                              //      onPress: () => { /* Do something. */ },
                              //    },
                              //  });
                                 
                             }
                           }).catch((error) => {
                             this.setState({loading_status:false})
                             
                             Snackbar.show({
                               title: error.message,
                               duration: Snackbar.LENGTH_SHORT,
                               backgroundColor:'black',
                               color:'red',
                               action: {
                                 title: 'OK',
                                 color: 'green',
                                 onPress: () => { /* Do something. */ },
                               },
                             });
   
   
                           });
       }
       else{
         ToastAndroid.show("User not found !",ToastAndroid.LONG)
       }
     });
    
   }

  makeComment(){
    if(this.state.comment.toString().trim().length > 0){
 
     Keyboard.dismiss()
     //var comm = this.state.comment
     this.setState({make_comment:this.state.comment,comment:''})
     AsyncStorage.getItem("user_id").then((item) => {
       if (item) {
   
        Keyboard.dismiss()
        var comm = this.state.comment
 
 
        this.setState({comment:''})
   
         var formData = new FormData();
   
         formData.append('user_id',item);
         formData.append('post_id',this.state.item.id);
         formData.append('comment',this.state.make_comment);
 
         
   
                       this.setState({loading_status:true})
                       let url = urls.base_url +'api/comment_post'
                       fetch(url, {
                       method: 'POST',
                       headers: {
                         'Accept': 'application/json',
                         'Content-Type': 'multipart/form-data',
                       },
                   body: formData
   
                     }).then((response) => response.json())
                           .then((responseJson) => {
                     this.setState({loading_status:false ,comment:'',make_comment:''})
 
                    /// console.log('Comment',JSON.stringify(responseJson))
 
                   if(responseJson.error == false){
 
                    //  var com = {...this.state.detail}
                    //  com.comments = responseJson.comments
                    //  //detail.comment_count
                    //  com.comment_count = this.state.detail.comment_count + 1
                    //  this.setState({detail:com})
 
 
                     // var com = detail.comments
                   //this.setState({detail['comments']: responseJson.comments})
                   }
              //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
   
             
   
   
                               if(!responseJson.error){

                                //   var com = {...this.state.comment_list}
                                //   com.comments = responseJson.comments
                                
                                //  this.setState({comment_list:com})

                                this.setState({
                                  comment_list: [...this.state.comment_list, responseJson.comments]
                                });
   
                               


                                //  Snackbar.show({
                                //   title: responseJson.message,
                                //   duration: Snackbar.LENGTH_SHORT,
                                //   backgroundColor:'black',
                                //   color:'red',
                                //   action: {
                                //     title: 'OK',
                                //     color: 'green',
                                //     onPress: () => { /* Do something. */ },
                                //   },
                                // });
                            
                             }
                             else{
   
                               Snackbar.show({
                                 title: responseJson.message,
                                 duration: Snackbar.LENGTH_SHORT,
                                 backgroundColor:'black',
                                 color:'red',
                                 action: {
                                   title: 'OK',
                                   color: 'green',
                                   onPress: () => { /* Do something. */ },
                                 },
                               });
                                 
                             }
                           }).catch((error) => {
                             this.setState({loading_status:false})
                             
                             Snackbar.show({
                               title: error.message,
                               duration: Snackbar.LENGTH_SHORT,
                               backgroundColor:'black',
                               color:'red',
                               action: {
                                 title: 'OK',
                                 color: 'green',
                                 onPress: () => { /* Do something. */ },
                               },
                             });
   
   
                           });
       }
       else{
         ToastAndroid.show("User not found !",ToastAndroid.LONG)
       }
     });
    }else{
      return
    }
   }


  likePosts(post_id,like_id){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
        var formData = new FormData();

        formData.append('user_id',item);
        formData.append('post_id',post_id);
        formData.append('like_type',like_id);
        

                      this.setState({loading_status:true,smiley_status:false})
                      let url = urls.base_url +'api/like_post'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                               // console.log(responseJson)
                              if(!responseJson.error){

                                Platform.OS === 'android' 
                                ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                : Alert.alert(responseJson.message)

                                var com = {...this.state.item}
                                com.like_flag = like_id

                                var like_update = responseJson.like_update
                                if(like_update == 1){
                                  //means already likhed
                                }
                                else if(like_update == 0){
                                  com.likes_count = com.likes_count + 1
                                }
                                
                               this.setState({item:com})


                     
                            }
                            else{

                             
                               

                        Snackbar.show({
                          title: responseJson.message,
                          duration: Snackbar.LENGTH_SHORT,
                          backgroundColor:'black',
                          color:'red',
                          action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                          },
                        });
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                           
                        Snackbar.show({
                          title: 'Try Again !',
                          duration: Snackbar.LENGTH_SHORT,
                          backgroundColor:'black',
                          color:'red',
                          action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                          },
                        });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }


  checkBuyPackagesChat(user){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
       var formData = new FormData();
        formData.append('from_id',item);
        formData.append('to_id',user.id);

       
                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/check_chat_status'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                              //     Snackbar.show({
                              //   title: "Chats Coming Soon",
                              //   duration: Snackbar.LENGTH_SHORT,
                              //   backgroundColor:'black',
                              //   color:'red',
                              //   action: {
                              //     title: 'OK',
                              //     color: 'green',
                              //     onPress: () => { /* Do something. */ },
                              //   },
                              // });


                              //niche wala 

                              let obj={
                                'to_id':user.id,
                                'to_name':user.name,
                                'to_image':user.user_image,
                                'to_status':'offline',
                                'to_typing':false
                                
                              }
                              this.props.navigation.navigate("ChatOutside",{result:obj})
                              

                                 
                              }


                           
                               else{

                                   
                                Snackbar.show({
                                  title: responseJson.message,
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });



                                 if(responseJson.request_status == ''){

                                  
                               if(responseJson.to_buy == 0){
                                      //means samne wale ne chat ka package nahi kharida hai
                               }
                                else{
                                    let obj={
                                      'to_id':user.id,
                                      'to_name':user.name,
                                      'to_image':user.user_image,
                                      'to_status':'offline'
                                    }
                                    this.props.navigation.navigate("ChatPackages",{result:obj})

                               }
                             

                                 
                                 }
                                 else{
                                  
                                 }
                             
                            
                                }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                          
                            Snackbar.show({
                              title: error.message,
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }


  chatStatus(user){

 
    if(user.seller_flag == 1){
   

    this.checkBuyPackagesChat(user)

    }
    else{
      Snackbar.show({
        title: 'This User Is Not Seller',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor:'black',
        color:'red',
        action: {
          title: 'OK',
          color: 'green',
          onPress: () => { /* Do something. */ },
        },
      });
    }
    
  }


    
  renderItem = ({item, index}) => {
    return(
      <View style={{width:'90%'}}>
     

          <View style={{height:null ,margin:1,
          padding:10,flexDirection:'row',justifyContent:'flex-start',alignItems:'center'}}>

          <Image source={{uri:urls.base_url + item.user.user_image}} 
          style={{width:30,height:30,marginRight:10,borderRadius:25,overflow:'hidden'}}/>

                <View style={{backgroundColor:'#CDCDCD'
                ,paddingRight:12,paddingTop:5,paddingLeft:12,
                paddingBottom:5,borderRadius:20,width:item.comment.length > 10 ? '80%' : '60%'}}>

                  
                  <Text style={{fontWeight:'bold'}}>{item.user.name}</Text>
                  <Text style={{fontSize:12,margin:3}}>{item.comment} </Text>
              
                <TouchableOpacity onPress={()=> {
                  if (item.user.id == this.props.user.user_id) {
                          Snackbar.show({
                                         title: "You can't chat with yourself",
                                         duration: Snackbar.LENGTH_SHORT,
                                         backgroundColor:'black',
                                         color:'red',
                                         action: {
                                           title: 'OK',
                                           color: 'green',
                                           onPress: () => { /* Do something. */ },
                                         },
                                       });
                  }
                  else{
                    this.chatStatus(item.user)
                  }
                }}
                 style={{position:'absolute',top:10,right:item.comment.length > 10 ? 5 : 5}}>
                 <Image source={require('../assets/chatcomment.png')}
                 style={{width:20,height:20}} resizeMode='contain'/>
                 </TouchableOpacity>
                </View>

         </View>

      </View>
    )
   }

   _keyboardDidShow = () => {
    // alert('Keyboard Shown');
     this.setState({bottom:Platform.OS == 'android'? 0 :
       Dimensions.get('window').height * 0.38})
   }
 
   _keyboardDidHide = () => {
     //alert('Keyboard Hidden');
     this.setState({bottom:0})
   }
 



 _checkCondition(){
    let img;
    const {item} = this.state
    if(item.like_flag == 1){
        img = require('../assets/p1.png')
    }else if (item.like_flag == 2){
      img = require('../assets/p2.png')
    }else if (item.like_flag == 3){
      img = require('../assets/p3.png')
    }else  if (item.like_flag == 4){
      img = require('../assets/p4.png')
    }
    else  if (item.like_flag == 5){
      img = require('../assets/p5.png')
    }
    else{
      img = require('../assets/p1.png')
    }

    
    return img;   
};








  componentWillMount(){
    this.getComment()
    let result = this.props.navigation.getParam('result')
    let item =  result['item']
    this.setState(prevState => ({
      item: result['item'],
      userId:item,
    })); 

    if(item.media_type == 1){
      Image.getSize(urls.base_url+ item.post_img, (width, height) => {
        console.log(`The image dimensions are ${width}x${height}`);

        var h_percentage = (windowWidth*100)/width
        var h_img = (height*h_percentage)/100
        this.setState({image_width:width, image_height:h_img})

       //this.setState({image_width:width, image_height:height})
      }, (error) => {
        console.log(`Couldn't get the image size: ${error.message}`);
      });
    }

   


   console.log(JSON.stringify(result['item']))

    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );

  
  }

  componentWillUnmount(){
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }


myPostDelete=(item)=>{
  this._menu.hide();
  var formData = new FormData();
  formData.append('post_id',item.id);
  

                     fetch(urls.base_url +'api/delete_post', {
                     method: 'POST',
                     headers: {
                       'Accept': 'application/json',
                       'Content-Type': 'multipart/form-data',
                     },
                    body: formData
                   }).then((response) => response.json())
                         .then((responseJson) => {
               
                  console.log('delete-->>',responseJson)
                  if(!responseJson.error){
                    console.log("hi")
    if(this.props.navigation.getParam('rout') && this.props.navigation.getParam('rout') == 'MyProfile'){
      this.props.navigation.navigate('MyProfile');//MyProfile
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'MyProfile' })],
      });
      this.props.navigation.dispatch(resetAction);
    }else{
      this.props.navigation.navigate('Home');
    }

                                  }
                         }).catch((error) => {
                            console.log(error)
                         
                                });
}

myPostEdit=(item)=>{
  this._menu.hide();
  var data={'post':item}
 
  this.props.navigation.navigate('EditPost',{result:data,routs:'edit'})

 
}
 

    render()
    {

      const {item} = this.state
  
      
        return(
          <SafeAreaView style={styles.container}>
    
            <ScrollView style={{backgroundColor:'black',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

          <View style={{ width: '100%',
           justifyContent: 'center', alignSelf: 'center' }}>

          <View style={{flexDirection:'row',
            justifyContent:'flex-start',alignItems:'center'}}>
    
    
           
                 <Image source={{uri:urls.base_url+item.user_pic}}
                    style={{height:40,width:40,
                    overflow:'hidden',borderRadius:20,margin:10}}  />
    
                    <View style={{marginLeft:8}}>
                    <Text style={{marginBottom:5,fontWeight:'bold',color:'white'}}>{item.user_name}</Text>
                    
                    </View>

    
{
      this.state.userId==item.user_id 
      ?
    <View style={{zIndex:999,position:'absolute',right:10,top:3}}>
    <Menu
    ref={this.setMenuRef}
    button={
    <TouchableOpacity onPress={this.showMenu} style={{width:30,height:30,top:7}}>
    <Image source={require('../assets/threeDotWhite.png')}
                    style={[styles.menuImage,{alignSelf:'center'}]} resizeMode='contain'/>
    </TouchableOpacity>
    } >
    <MenuItem onPress={ ()=>{
              Alert.alert(
              "Alert", 
              'Are you sure want to delete ?',
              [
                { text: "OK", onPress:()=> this.myPostDelete(item)},
                { text: "CANCEL", onPress: () => console.log("cancel Pressed") }
              ],
              { cancelable: false } 
               ) }
            } >Delete</MenuItem>
    <MenuItem onPress={() => {
         this.myPostEdit(item)
    }
        }>Edit</MenuItem> 
    </Menu>
    </View>
      : null 
     } 

            </View> 


              <Text style={{margin:15,color:'white',alignSelf:'center'
                }}>{item.description}</Text>

                <View style={{height:item.media_type == '2'? Dimensions.get('window').height * 0.45 : this.state.image_height + 0}}>
                {
                  item.media_type == '2'
                    ?
                    <Video source={{uri:urls.base_url + item.post_video}}
                     ref={(ref) => {
                            this.player = ref
                          }} 
                          resizeMode='cover'
                          
                        controls={true}
                        paused={true}
                        onError={e => console.log(e)}
                        onLoad={()=> console.log()}
                        onBuffer={()=> {}}   
                        style={{width:'100%',height:'100%',backgroundColor:'grey'}}/>
                    : 

                  // null
                    <FastImage source={{uri:urls.base_url+ item.post_img}}
                     resizeMode={FastImage.resizeMode.stretch }
                    style={
                      {alignSelf:'center',height:this.state.image_height,width:windowWidth,
                    overflow:'hidden'}
                  }
                    />
                   
                   
                  } 

                  <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>



                  <TouchableOpacity activeOpacity={0.6}
                  style={{flexDirection:'row',alignItems:'center',width:50,marginBottom:25}}
                 // onPress={()=> this.likePosts(this.props.posts[this.props.index].id,1)}
                  onLongPress={()=> {
                    //console.log("sdfhsdbfjhdsfbjhdsfbdhjfdhj")
                  this.setState({smiley_status:true})
                  }}
                  onPress={()=>  this.likePosts(item.id,1)}
                  >
  
                  <Image source={this._checkCondition()}
                      style={{height:25,width:25,
                      overflow:'hidden',borderRadius:12,margin:10}}  />
      
                      <Text style={{color:'white'}}>{item.likes_count} likes</Text>
      
               </TouchableOpacity>

               <Text onPress={()=>{
                let obj ={ 'post_id' : this.state.item.id}
                this.props.navigation.navigate('PeopleReacted',{result:obj})
              }}
              style={{color:colors.color_primary,fontWeight:'bold',fontSize:17,marginBottom:25}}>See Who Reacted   </Text>


               </View>


                

               {
                this.state.smiley_status
                ?
                <View style={{position:'absolute',
                bottom:10,left:10,
                backgroundColor:'white',
                width:null,flexDirection:'row',
                 alignItems:'center',borderRadius:20,
                padding:5,borderWidth:1}}>
  

                <TouchableOpacity  onPress={()=> {
                 this.likePosts(item.id,1)
                }}>
                <View style={{justifyContent:'center',alignItems:'center'}}>
                <Image source={require('../assets/p1.png')}
                style={{width:40,height:40}} resizeMode='contain'/>
                  <Text>{item.likes_thumb}</Text>
                </View>
                </TouchableOpacity>
              
                <TouchableOpacity onPress={()=> {
                   this.likePosts(item.id,2)
                //  this.setState({posts:  this.state.posts})
                }}>
                <View style={{justifyContent:'center',alignItems:'center',marginLeft:5}}>
              <Image source={require('../assets/p2.png')}
                style={{width:40,height:40}} resizeMode='contain'/>
                <Text>{item.likes_cool}</Text>

                </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=> {
                  this.likePosts(item.id,3)
                }}>
                <View style={{justifyContent:'center',alignItems:'center',marginLeft:5}}>
               <Image source={require('../assets/p3.png')}
                style={{width:40,height:40}} resizeMode='contain'/>
                <Text>{item.likes_awesome}</Text>

                </View>
                </TouchableOpacity>


                <TouchableOpacity onPress={()=> {
                  
                  this.likePosts(item.id,4)
                 
                }}>
                <View style={{justifyContent:'center',alignItems:'center',marginLeft:5}}>
               <Image source={require('../assets/p4.png')}
                style={{width:40,height:40}} resizeMode='contain'/>
                <Text>{item.likes_other}</Text>
                </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=> {
                  
                  this.likePosts(item.id,5)
                 
                }}>
                <View style={{justifyContent:'center',alignItems:'center',marginLeft:5}}>
               <Image source={require('../assets/p5.png')}
                style={{width:40,height:40}} resizeMode='contain'/>
                <Text>{item.likes_fantastic}</Text>

                </View>
                </TouchableOpacity>


                </View>
                : null 
              }

    
               

                </View>


                 

              

               
                {
                  this.state.comment_list.length == 0
                  ? null 
                  : 
                  <View style={{paddingBottom:40, width:Dimensions.get('window').width * 0.99,marginTop:30}}>
                  <Text style={{margin:10,color:'white',fontWeight:'bold',fontSize:17}}>Comments</Text>
                  <FlatList
                   style={{marginBottom:10,width:Dimensions.get('window').width * 0.9,padding:4}}
                    data={this.state.comment_list}
                   showsVerticalScrollIndicator={false}
                   renderItem={(item,index) => this.renderItem(item,index)}
                 />
                 </View>
                }
    
            </View>
            <View style={{width:100,height:120}}></View>
            </ScrollView>

             {/* comment section */}
                     
             <View style={{position:'absolute',bottom:this.state.bottom,right:0,
             left:0,height:60,backgroundColor:'white',elevation:10,shadowOpacity: 5,paddingRight:10,
             paddingLeft:10,paddingTop:7,paddingBottom:7}}>

                  <View style={{flexDirection:'row',alignItems:'center',
                  flex:4,borderColor:'black',borderWidth:0.4,marginLeft:4,marginRight:2,borderRadius:20,backgroundColor:'#F2F2F2'}}>


                  <View style={{flex:2.5}}>
                  <TextInput
                  value={this.state.comment}
                  onSubmitEditing={() => {this.makeComment()}}
                  multiline={true}
                  ref={(input) => { this.commentBox = input; }}
                  placeholder={'Write Comment Here..'}
                  placeholderTextColor={colors.color_primary}
                  onFocus={()=> {}}
                  onChangeText={(comment) => this.setState({ comment : comment })}
                  style={{width:'100%',height:'100%',padding:10}}

                  />

                  {/* onSubmitEditing={() => { this.commentBox.focus(); }} */}

                  </View>


                  <View style={{flex:0.5}}>
                    
                    <TouchableOpacity onPress={() =>{this.makeComment()}}>
                        <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/send-button-grey.png')} />
                        </TouchableOpacity>
    </View>






</View>
             
              </View>

              {/**commnets */}


             
    
            {this.state.loading_status &&
              <View style={[
                StyleSheet.absoluteFill,
                { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
              ]}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,

	};
};

const mapDispatchToProps = dispatch => {
  return {

    change: (userinfo) => dispatch(changeUser(userinfo)),
    enterpreneurUser: () => dispatch(enterpreneurUser()),


  }
}


export default connect(mapStateToProps, mapDispatchToProps)(PostDetails);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  },  
  menuImage:{width:17,height:17,marginRight:6,flex:1},
  });