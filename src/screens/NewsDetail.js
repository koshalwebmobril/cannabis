import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback,Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import {  StackActions, NavigationActions} from 'react-navigation';
import HTML from "react-native-render-html";

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class NewsDetail extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
       detail:null,
       comment:'',
       make_comment:'',
       bottom:0,
       image_width:0,
       image_height:0,
     
    };


  }

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
  headerRight: () => (

    <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
      {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
      
  </View>
  ),
  headerLeft: () => (
    <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
         <TouchableOpacity onPress={()=>{navigation.goBack() }}>
                <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                </TouchableOpacity>

                <Text style={{marginLeft:20,color:'white',fontSize:16}}>News</Text>
   </View>
  ),
  });


  makeComment(news_id){
   if(this.state.comment.toString().trim().length > 0){

    Keyboard.dismiss()
   
    this.setState({make_comment:this.state.comment,comment:''})

    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
       Keyboard.dismiss()
       var comm = this.state.comment
       this.setState({comment:''})
        var formData = new FormData();
        formData.append('user_id',item);
        formData.append('news_id',news_id);
        formData.append('comment',this.state.make_comment);

        
  
                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/comment_news'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData
  
                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false ,comment:'',make_comment:''})

                console.log('Comment',JSON.stringify(responseJson))

                  if(responseJson.error == false){

                    var com = {...this.state.detail}
                    com.comments = responseJson.comments
                    //detail.comment_count
                    com.comment_count = this.state.detail.comment_count + 1
                    this.setState({detail:com})


                    // var com = detail.comments
                  //this.setState({detail['comments']: responseJson.comments})
                  }
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
  
            
  
  
                              if(!responseJson.error){
  
                                this.setState({comment:''})
                           
                            }
                            else{
  
                              Snackbar.show({
                                title: responseJson.message,
                                duration: Snackbar.LENGTH_SHORT,
                                backgroundColor:'black',
                                color:'red',
                                action: {
                                  title: 'OK',
                                  color: 'green',
                                  onPress: () => { /* Do something. */ },
                                },
                              });
                                
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: error.message,
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });
  
  
                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
   }else{
     return
   }
  }

  makeNewsLike(news_id){

    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
       Keyboard.dismiss()
                  var formData = new FormData();
                  formData.append('user_id',item);
                  formData.append('news_id',news_id);
                  formData.append('like_type','1');

                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/like_news'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData
  
                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})

                console.log('likes',JSON.stringify(responseJson))

                              if(!responseJson.error){
  
                                Snackbar.show({
                                  title: responseJson.message,
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });
                          //  this.myComponentWillMount();
                            }
                            else{
  
                              Snackbar.show({
                                title: responseJson.message,
                                duration: Snackbar.LENGTH_SHORT,
                                backgroundColor:'black',
                                color:'red',
                                action: {
                                  title: 'OK',
                                  color: 'green',
                                  onPress: () => { /* Do something. */ },
                                },
                              });
                                
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: error.message,
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });
  
  
                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });






  }
  
  renderItem = ({item, index}) => {
    return(
      <View style={{width:'90%'}}>
     

          <View style={{height:null ,margin:1,
          padding:10,flexDirection:'row',justifyContent:'flex-start',alignItems:'center'}}>

          <Image source={{uri:urls.base_url + item.user.user_image}} 
          style={{width:30,height:30,marginRight:10,borderRadius:25,overflow:'hidden'}}/>


  

                <View style={{backgroundColor:'#CDCDCD'
                ,paddingRight:12,paddingTop:5,paddingLeft:12,
                paddingBottom:5,borderRadius:20,width:item.comment.length > 10 ? '80%' : '60%'}}>

                  
                  <Text style={{fontWeight:'bold'}}>{item.user.name}</Text>
                  <Text style={{fontSize:12,margin:3}}>{item.comment} </Text>
                </View>



         </View>

         {/* 
         <Text 
         onPress={()=> this.commentBox.focus()}
         style={{marginTop:-15,alignSelf:'flex-end',
         textDecorationLine: 'underline',fontWeight: 'bold',
         fontStyle: 'italic'}}>Reply</Text>

         */}

          
      </View>
    )
   }

   

  _keyboardDidShow = () => {
    // alert('Keyboard Shown');
     this.setState({bottom:Platform.OS == 'android'? 0 :
       Dimensions.get('window').height * 0.38})
 
 
   }
 
   _keyboardDidHide = () => {
     //alert('Keyboard Hidden');
     this.setState({bottom:0})
   }
 


  componentWillMount(){
    this.myComponentWillMount();
  
  }
myComponentWillMount(){
  let result = this.props.navigation.getParam('result')
  var detail = result['news_detail']

  this.setState({detail})
console.log('sdfghjklfghj',detail)
  if(detail.img){
    Image.getSize(urls.base_url+ detail.img, (width, height) => {
      console.log(`The image dimensions are ${width}x${height}`);

      var h_percentage = (windowWidth*100)/width
      var h_img = (height*h_percentage)/100
      this.setState({image_width:width, image_height:h_img})

    
    }, (error) => {
      console.log(`Couldn't get the image size: ${error.message}`);
    });
  }




 // console.log(JSON.stringify(detail))

  this.keyboardDidShowListener = Keyboard.addListener(
    'keyboardDidShow',
    this._keyboardDidShow,
  );
  this.keyboardDidHideListener = Keyboard.addListener(
    'keyboardDidHide',
    this._keyboardDidHide,
  );

}

  _deleteNews(){
    

        let result = this.props.navigation.getParam('result')
         var formData = new FormData();

       
         formData.append('news_id',this.state.detail.id);
        
 
                       this.setState({loading_status:true})
                       let url = urls.base_url +'api/api_delete_news'
                       fetch(url, {
                       method: 'POST',
                       headers: {
                         'Accept': 'application/json',
                         'Content-Type': 'multipart/form-data',
                       },
                   body: formData
 
                     }).then((response) => response.json())
                           .then((responseJson) => {
                   this.setState({loading_status:false})
                  
             
          

              Snackbar.show({
                title: responseJson.message,
                duration: Snackbar.LENGTH_SHORT,
                backgroundColor:'black',
                color:'red',
                action: {
                  title: 'OK',
                  color: 'green',
                  onPress: () => { /* Do something. */ },
                },
              });

              
 
              
                         if(!responseJson.error){

                          this.props.navigation.navigate('News');
                          const resetAction = StackActions.reset({
                            index: 0,
                            actions: [NavigationActions.navigate({ routeName: 'News' })],
                          });
                          this.props.navigation.dispatch(resetAction);
                              
                              
                             }
                             else{
 
                              
                             }
                           }).catch((error) => {
                             this.setState({loading_status:false})
                             
                      //  Platform.OS === 'android' 
                      //  ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                      //  : Alert.alert("Try Again !")

                      Snackbar.show({
                        title: "Try Again !",
                        duration: Snackbar.LENGTH_SHORT,
                        backgroundColor:'black',
                        color:'red',
                        action: {
                          title: 'OK',
                          color: 'green',
                          onPress: () => { /* Do something. */ },
                        },
                      });
 
 
                           });
       
  }

  componentWillUnmount(){
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _editNews(){
    let obj ={
      'id':this.state.detail.id,
      'user_id':this.state.detail.user_id,
      'title':this.state.detail.title,
      'description':this.state.detail.description,
      'img':this.state.detail.img,

    }
    this.props.navigation.navigate('EditNews',{result:obj})
  }

    render()
    {

      const DeleteSheet= () =>{
     
        return (
          <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={200}
          duration={0}
          customStyles={{
            container: {
              justifyContent: "center",
              alignItems: "center",
              borderTopLeftRadius:15,
              borderTopRightRadius:15
            },
            // wrapper:{
            //   backgroundColor:'grey'
            // }
          }}
          animationType='fade'
          closeOnDragDown={true}
          minClosingHeight={10}

>
            <View style={{width:'80%',height:'80%',backgroundColor:'white',padding:10}}>

                <View style={{flexDirection:'row',justifyContent:'space-around',alignItems:'center',marginBottom:20}}>
                      <Image style={{ height: 40, width: 40}}
                      resizeMode='contain'
                      source={require('../assets/logo.png')}>
                      </Image>

                      <Text style={{fontWeight:'bold',fontSize:16}}>Are you sure to delete this news ? </Text>
                
                </View>


                <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',
              flex:2}}>

                  <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                  <TouchableOpacity onPress={()=> this._deleteNews()}
                  style={{width:'100%',backgroundColor:colors.color_primary,padding:20,
                    justifyContent:'center',alignItems:'center',elevation:10}}>
                  <Text style={{color:'white'}}>Yes</Text>
                  </TouchableOpacity>
                  </View>


                  <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                  <TouchableOpacity onPress={()=> this.RBSheet.close()}
                  style={{width:'100%',backgroundColor:'white',padding:20,
                    justifyContent:'center',alignItems:'center',elevation:10}}>
                  <Text style={{color:colors.color_primary}}>No</Text>
                  </TouchableOpacity>
                  </View>
              
              </View>
            </View>
</RBSheet>

        )
      }

      const{detail} = this.state
  
      
        return(
          <SafeAreaView style={styles.container}>
          <DeleteSheet/>
  
            <ScrollView style={{backgroundColor:'#EBFAFF',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',padding:6,alignSelf:'center'}}>

           <View style={{
             width:    Dimensions.get('window').width * 0.92, //       Dimensions.get('window').width * 0.92,
             height:   this.state.image_height,                   //  Dimensions.get('window').height * 0.29,   
             alignSelf:'center'
            


            //  var h_percentage = (windowWidth*100)/width
            //  var h_img = (height*h_percentage)/100
           }}>

           <Image source={{uri:urls.base_url + detail.img}}
          resizeMode="cover"
           style={{width:'100%',height:'100%'}}>
           </Image>
           </View>

           <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
           
            <TouchableOpacity activeOpacity={0.6}
            style={{flexDirection:'row',alignItems:'center'}}>
        
                <Image source={require('../assets/p1.png')}
                  style={{height:25,width:25,
                  overflow:'hidden',borderRadius:12,margin:8}}  />

                  <Text>{detail.likes_count} likes</Text>

            </TouchableOpacity>

            {
              this.state.detail.user_id == this.props.user.user_id 
              ?
              <View
            style={{flexDirection:'row',alignItems:'center'}}>
       
                <TouchableOpacity onPress={()=> this._editNews()}>
               <Image source={require('../assets/edit.png')}
               resizeMode='contain'
                 style={{height:25,width:25,
                 overflow:'hidden',borderRadius:12,marginRight:14}}  />
                 </TouchableOpacity>

                 <TouchableOpacity onPress={() => {this.RBSheet.open()}}>
                 <Image source={require('../assets/trash.png')}
                   style={{height:25,width:25,
                   overflow:'hidden',borderRadius:12,marginRight:8}}  />
                   </TouchableOpacity>

           </View>
           : null

            }


            
            
           
           </View>
          
                <View style={{width:Dimensions.get('window').width * 0.95,
                backgroundColor:'black',
                 height:1,marginTop:0,marginBottom:1,alignSelf:'center'}}></View>

                <View style={{flexDirection:'row',
                justifyContent:'space-between',alignItems:'center'}}>
                     {/* this.makeNewsLike(detail.id) */}
                  <TouchableOpacity onPress={()=>{this.makeNewsLike(detail.id)}}>
                  <Image source={require('../assets/thumb-up.png')}
                      style={{height:25,width:25,
                      overflow:'hidden',borderRadius:12,margin:10}}  />
                  </TouchableOpacity>
                  

                  <TouchableOpacity onPress={()=> this.commentBox.focus()}
                   style={{flexDirection:'row',
                alignItems:'center'}}>
                <Text style={{color:'grey'}}> {detail.comment_count} Comments</Text>
                  <Image source={require('../assets/comment-grey.png')}
                      style={{height:25,width:25,
                      overflow:'hidden',borderRadius:12,margin:10}}  />
                  </TouchableOpacity>


                </View>


                <Text style={{margin:6,alignSelf:'flex-start',
                color:'grey',fontWeight:'bold',fontSize:17}}>{detail.title}</Text>


          <View style={{width:'55%',flexDirection:'row',alignItems:'center',
            justifyContent:'flex-start',margin:6}}>



                  <View style={{flexDirection:'row',width:'90%',alignItems:'center'}}>
                  <Image source={require('../assets/clock.png')}
                              style={{width:17,height:17,marginRight:6}} resizeMode='contain'/>

                  <Text style={{color:'grey',fontSize:13}}>{detail.created_at}</Text>
            
                </View>


                <View style={{flexDirection:'row',width:'40%',alignItems:'center'}}>

                <Text style={{color:'black',fontSize:13,fontWeight:'bold'}}>Posted By :  </Text>

                  <Text style={{color:'grey',fontSize:13}}>{detail.posted_by}</Text>
            
                </View>


         </View>

            {/* <Text  adjustsFontSizeToFit 
            style={{margin:5,color:'black',margin:10,paddingBottom:detail.comments.length == 0 ? 50 : 0,
            alignSelf:'flex-start'
              }}>
                 {detail.description.toString()}
              
              </Text> */}
              <View style={{width:'100%',padding:10}}>
                <HTML
                 html={detail.description.toString()}
                 imagesMaxWidth={Dimensions.get("window").width}
                
                 />
              </View>
               

           {
             detail.comments.length == 0
             ? null 
             : 
             <View style={{paddingBottom:40, width:Dimensions.get('window').width * 0.99}}>
             <Text style={{margin:10,color:'black',fontWeight:'bold',fontSize:17}}>Comments</Text>
             <FlatList
              style={{marginBottom:10,width:Dimensions.get('window').width * 0.9,padding:4}}
              data={detail.comments}
              showsVerticalScrollIndicator={false}
              renderItem={(item,index) => this.renderItem(item,index)}
            />
            </View>
           }


<View style={{width:100,height:100}}></View>

            </ScrollView>

               {/* comment section */}
                     
               <View style={{position:'absolute',bottom:this.state.bottom,right:0,
               left:0,height:60,backgroundColor:'white',elevation:10,shadowOpacity: 5,paddingRight:10,
               paddingLeft:10,paddingTop:7,paddingBottom:7}}>

<View style={{flexDirection:'row',alignItems:'center',
flex:4,borderColor:'black',borderWidth:0.4,marginLeft:4,marginRight:2,borderRadius:20,backgroundColor:'#F2F2F2'}}>


<View style={{flex:2.5}}>
<TextInput
value={this.state.comment}
onSubmitEditing={() => {this.makeComment(detail.id)}}
// keyboardType={Platform.OS === 'ios' ? 'ascii-capable' : 'visible-password'}
multiline={true}
ref={(input) => { this.commentBox = input; }}
placeholder={'Write Comment Here..'}
placeholderTextColor={colors.color_primary}
onFocus={()=> {}}
onChangeText={(comment) => {
  //  const regex = /\uD83C\uDFF4(?:\uDB40\uDC67\uDB40\uDC62(?:\uDB40\uDC65\uDB40\uDC6E\uDB40\uDC67|\uDB40\uDC77\uDB40\uDC6C\uDB40\uDC73|\uDB40\uDC73\uDB40\uDC63\uDB40\uDC74)\uDB40\uDC7F|\u200D\u2620\uFE0F)|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC68(?:\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D)?\uD83D\uDC68|(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3]))|\uD83D\uDC69\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D(?:\uD83D[\uDC68\uDC69])|\uD83D[\uDC68\uDC69])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66\u200D\uD83D\uDC66|(?:\uD83D\uDC41\uFE0F\u200D\uD83D\uDDE8|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\uD83D\uDC68(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\u200D[\u2695\u2696\u2708])|(?:(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)\uFE0F|\uD83D\uDC6F|\uD83E[\uDD3C\uDDDE\uDDDF])\u200D[\u2640\u2642]|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|\u200D[\u2640\u2642])|\uD83D\uDC69\u200D[\u2695\u2696\u2708])\uFE0F|\uD83D\uDC69\u200D\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC68(?:\u200D(?:(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D[\uDC66\uDC67])|\uD83C[\uDFFB-\uDFFF])|\uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08|\uD83D\uDC69\u200D\uD83D\uDC67|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66|\uD83C\uDDF6\uD83C\uDDE6|\uD83C\uDDFD\uD83C\uDDF0|\uD83C\uDDF4\uD83C\uDDF2|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])|\uD83C\uDDED(?:\uD83C[\uDDF0\uDDF2\uDDF3\uDDF7\uDDF9\uDDFA])|\uD83C\uDDEC(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEE\uDDF1-\uDDF3\uDDF5-\uDDFA\uDDFC\uDDFE])|\uD83C\uDDEA(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDED\uDDF7-\uDDFA])|\uD83C\uDDE8(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDEE\uDDF0-\uDDF5\uDDF7\uDDFA-\uDDFF])|\uD83C\uDDF2(?:\uD83C[\uDDE6\uDDE8-\uDDED\uDDF0-\uDDFF])|\uD83C\uDDF3(?:\uD83C[\uDDE6\uDDE8\uDDEA-\uDDEC\uDDEE\uDDF1\uDDF4\uDDF5\uDDF7\uDDFA\uDDFF])|\uD83C\uDDFC(?:\uD83C[\uDDEB\uDDF8])|\uD83C\uDDFA(?:\uD83C[\uDDE6\uDDEC\uDDF2\uDDF3\uDDF8\uDDFE\uDDFF])|\uD83C\uDDF0(?:\uD83C[\uDDEA\uDDEC-\uDDEE\uDDF2\uDDF3\uDDF5\uDDF7\uDDFC\uDDFE\uDDFF])|\uD83C\uDDEF(?:\uD83C[\uDDEA\uDDF2\uDDF4\uDDF5])|\uD83C\uDDF8(?:\uD83C[\uDDE6-\uDDEA\uDDEC-\uDDF4\uDDF7-\uDDF9\uDDFB\uDDFD-\uDDFF])|\uD83C\uDDEE(?:\uD83C[\uDDE8-\uDDEA\uDDF1-\uDDF4\uDDF6-\uDDF9])|\uD83C\uDDFF(?:\uD83C[\uDDE6\uDDF2\uDDFC])|\uD83C\uDDEB(?:\uD83C[\uDDEE-\uDDF0\uDDF2\uDDF4\uDDF7])|\uD83C\uDDF5(?:\uD83C[\uDDE6\uDDEA-\uDDED\uDDF0-\uDDF3\uDDF7-\uDDF9\uDDFC\uDDFE])|\uD83C\uDDE9(?:\uD83C[\uDDEA\uDDEC\uDDEF\uDDF0\uDDF2\uDDF4\uDDFF])|\uD83C\uDDF9(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDED\uDDEF-\uDDF4\uDDF7\uDDF9\uDDFB\uDDFC\uDDFF])|\uD83C\uDDE7(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEF\uDDF1-\uDDF4\uDDF6-\uDDF9\uDDFB\uDDFC\uDDFE\uDDFF])|[#\*0-9]\uFE0F\u20E3|\uD83C\uDDF1(?:\uD83C[\uDDE6-\uDDE8\uDDEE\uDDF0\uDDF7-\uDDFB\uDDFE])|\uD83C\uDDE6(?:\uD83C[\uDDE8-\uDDEC\uDDEE\uDDF1\uDDF2\uDDF4\uDDF6-\uDDFA\uDDFC\uDDFD\uDDFF])|\uD83C\uDDF7(?:\uD83C[\uDDEA\uDDF4\uDDF8\uDDFA\uDDFC])|\uD83C\uDDFB(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDEE\uDDF3\uDDFA])|\uD83C\uDDFE(?:\uD83C[\uDDEA\uDDF9])|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u261D\u270A-\u270D]|\uD83C[\uDF85\uDFC2\uDFC7]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66\uDC67\uDC70\uDC72\uDC74-\uDC76\uDC78\uDC7C\uDC83\uDC85\uDCAA\uDD74\uDD7A\uDD90\uDD95\uDD96\uDE4C\uDE4F\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD30-\uDD36\uDDB5\uDDB6\uDDD1-\uDDD5])(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u231A\u231B\u23E9-\u23EC\u23F0\u23F3\u25FD\u25FE\u2614\u2615\u2648-\u2653\u267F\u2693\u26A1\u26AA\u26AB\u26BD\u26BE\u26C4\u26C5\u26CE\u26D4\u26EA\u26F2\u26F3\u26F5\u26FA\u26FD\u2705\u270A\u270B\u2728\u274C\u274E\u2753-\u2755\u2757\u2795-\u2797\u27B0\u27BF\u2B1B\u2B1C\u2B50\u2B55]|\uD83C[\uDC04\uDCCF\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE1A\uDE2F\uDE32-\uDE36\uDE38-\uDE3A\uDE50\uDE51\uDF00-\uDF20\uDF2D-\uDF35\uDF37-\uDF7C\uDF7E-\uDF93\uDFA0-\uDFCA\uDFCF-\uDFD3\uDFE0-\uDFF0\uDFF4\uDFF8-\uDFFF]|\uD83D[\uDC00-\uDC3E\uDC40\uDC42-\uDCFC\uDCFF-\uDD3D\uDD4B-\uDD4E\uDD50-\uDD67\uDD7A\uDD95\uDD96\uDDA4\uDDFB-\uDE4F\uDE80-\uDEC5\uDECC\uDED0-\uDED2\uDEEB\uDEEC\uDEF4-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])|(?:[#*0-9\xA9\xAE\u203C\u2049\u2122\u2139\u2194-\u2199\u21A9\u21AA\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA\u24C2\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE\u2600-\u2604\u260E\u2611\u2614\u2615\u2618\u261D\u2620\u2622\u2623\u2626\u262A\u262E\u262F\u2638-\u263A\u2640\u2642\u2648-\u2653\u265F\u2660\u2663\u2665\u2666\u2668\u267B\u267E\u267F\u2692-\u2697\u2699\u269B\u269C\u26A0\u26A1\u26AA\u26AB\u26B0\u26B1\u26BD\u26BE\u26C4\u26C5\u26C8\u26CE\u26CF\u26D1\u26D3\u26D4\u26E9\u26EA\u26F0-\u26F5\u26F7-\u26FA\u26FD\u2702\u2705\u2708-\u270D\u270F\u2712\u2714\u2716\u271D\u2721\u2728\u2733\u2734\u2744\u2747\u274C\u274E\u2753-\u2755\u2757\u2763\u2764\u2795-\u2797\u27A1\u27B0\u27BF\u2934\u2935\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55\u3030\u303D\u3297\u3299]|\uD83C[\uDC04\uDCCF\uDD70\uDD71\uDD7E\uDD7F\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE02\uDE1A\uDE2F\uDE32-\uDE3A\uDE50\uDE51\uDF00-\uDF21\uDF24-\uDF93\uDF96\uDF97\uDF99-\uDF9B\uDF9E-\uDFF0\uDFF3-\uDFF5\uDFF7-\uDFFF]|\uD83D[\uDC00-\uDCFD\uDCFF-\uDD3D\uDD49-\uDD4E\uDD50-\uDD67\uDD6F\uDD70\uDD73-\uDD7A\uDD87\uDD8A-\uDD8D\uDD90\uDD95\uDD96\uDDA4\uDDA5\uDDA8\uDDB1\uDDB2\uDDBC\uDDC2-\uDDC4\uDDD1-\uDDD3\uDDDC-\uDDDE\uDDE1\uDDE3\uDDE8\uDDEF\uDDF3\uDDFA-\uDE4F\uDE80-\uDEC5\uDECB-\uDED2\uDEE0-\uDEE5\uDEE9\uDEEB\uDEEC\uDEF0\uDEF3-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])\uFE0F|(?:[\u261D\u26F9\u270A-\u270D]|\uD83C[\uDF85\uDFC2-\uDFC4\uDFC7\uDFCA-\uDFCC]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66-\uDC69\uDC6E\uDC70-\uDC78\uDC7C\uDC81-\uDC83\uDC85-\uDC87\uDCAA\uDD74\uDD75\uDD7A\uDD90\uDD95\uDD96\uDE45-\uDE47\uDE4B-\uDE4F\uDEA3\uDEB4-\uDEB6\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD26\uDD30-\uDD39\uDD3D\uDD3E\uDDB5\uDDB6\uDDB8\uDDB9\uDDD1-\uDDDD])/g
  //  var comme = comment.replace(regex, '')
  // this.setState({ comment : comme })
  this.setState({ comment : comment })

}
}
style={{width:'100%',height:'100%',padding:10}}

/>
</View>


<View style={{flex:0.5}}>
  
   <TouchableOpacity onPress={() =>{this.makeComment(detail.id)}}>
      <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/send-button-grey.png')} />
      </TouchableOpacity>
</View>






</View>
               
                </View>

    
            {this.state.loading_status &&
              <View pointerEvents="none" style={styles.loading}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(NewsDetail);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });