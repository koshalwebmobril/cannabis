import React from 'react';
import {colors, urls} from '../Globals';
import {Header} from 'react-native-elements';
import {TextField} from 'react-native-material-textfield';

import FullView from './FullView';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';
import firebaseSDK from '../config/firebaseSDK';
import {Slider, Overlay} from 'react-native-elements';
import {StackActions, NavigationActions} from 'react-navigation';
import FastImage from 'react-native-fast-image';
import { ProcessingManager } from 'react-native-video-processing';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,
  FlatList,
  TextInput,
  PermissionsAndroid,
  RefreshControl,
  Fragment,
  Platform,
  AppState,
  Linking,
} from 'react-native';
import {DrawerActions} from 'react-navigation';
import {connect} from 'react-redux';
import {addUser, changeUser} from '../actions/actions';
import ImagePicker from 'react-native-image-picker';

//import Video from 'react-native-video';
//import Video from 'react-native-video-controls';
import Video from 'react-native-video-player';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import Snackbar from 'react-native-snackbar';
import PostComponent from '../components/PostComponent';
import CustomHeader from '../components/CustomHeader';
import {getFCMToken} from '../config/DeviceTokenUtils';
import Geolocation from '@react-native-community/geolocation';
import RNThumbnail from 'react-native-thumbnail';
import AdvertiesmentImg from '../components/AvertiesmentImg';
import PostAdvertiesment from '../components/PostAdvertiesment';
import {fetchPosts} from '../api/apis.js';
import {showMessage} from '../config/snackmsg';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';

import {OptimizedFlatList} from 'react-native-optimized-flatlist';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading_status: false,
      post_text: '',
      //only for text
      post_text_error: false,
      //for post eroor full
      post_error: false,
      //no image or video error
      image_video_error: false,
      photo: null,
      imageSource: null,
      videoSource: null,
      //dummy is just temp vidoe or image
      imageSourceDummy: null,
      videoSourceDummy: null,
      video: null,
      refreshing: false,
      smiley: false,
      posts: [],
      latitude: 0.0,
      longitude: 0.0,
      deviceToken: '',
      chatsModalVisible: false,
      investorModalVisible: false,
      newsModalVisible: false,
      videoThumbnail: null,
      user_id:'',
      showMenu:false,
      AdvmenuItemId:''
    };
  }


  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };

  static navigationOptions = ({navigation}) => ({
    headerTitleStyle: {alignSelf: 'flex-start'},
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,

      shadowOffset: {
        width: 0,
        height: 3,
      },
      shadowOpacity: 0.5, //for ios
      shadowRadius: 6,
      elevation: 10, //for android
    },
    // headerTitle: () => <Text>dssd</Text>,
    //title:"SSSS",
    headerRight: () => (
      <View
        style={{flexDirection: 'row', alignItems: 'center', marginRight: 7}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Discover');
          }}>
          <FastImage
            source={require('../assets/user.png')}
            style={{width: 20, height: 20}}
            resizeMode={FastImage.resizeMode.contain}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Requests', {route_name: 'Home'});
          }}>
          <FastImage
            source={require('../assets/notification.png')}
            style={{width: 20, height: 20, marginLeft: 20, marginRight: 5}}
            resizeMode={FastImage.resizeMode.contain}
          />
          {navigation.getParam('count') > 0 ? (
            <View
              style={{
                position: 'absolute',
                top: -10,
                height:
                  navigation.getParam('count').toString().length === 1
                    ? 20
                    : null,
                width:
                  navigation.getParam('count').toString().length === 1
                    ? 20
                    : null,
                backgroundColor: 'red',
                left: 28,
                borderRadius: 10,
                padding: 4,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: 'white', fontSize: 11}}>
                {navigation.getParam('count')}
              </Text>
            </View>
          ) : null}
        </TouchableOpacity>

        {/* navigation.navigate("Chats")*/}
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Chats', {route_name: 'Home'});
          }}>
          <FastImage
            source={require('../assets/chat.png')}
            style={{width: 20, height: 20, marginLeft: 15}}
            resizeMode={FastImage.resizeMode.contain}
          />
        </TouchableOpacity>

        {/* navigation.navigate("Search")*/}
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Search');
          }}>
          <FastImage
            source={require('../assets/search-white.png')}
            style={{width: 20, height: 20, marginLeft: 15}}
            resizeMode={FastImage.resizeMode.contain}
          />
        </TouchableOpacity>
      </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 7}}>
        <TouchableOpacity
          onPress={() => {
            navigation.toggleDrawer();
          }}>
          <FastImage
            source={require('../assets/menu.png')}
            style={{width: 20, height: 20}}
            resizeMode={FastImage.resizeMode.contain}
          />
        </TouchableOpacity>

        <Text style={{marginLeft: 20, color: 'white', fontSize: 13}}>
          Cannabis Expressions
        </Text>
      </View>
    ),
  });

  _handleAppStateChange = (nextAppState) => {
    if (nextAppState === 'active') {
      this.changeStatus(1);
    } else {
      this.changeStatus(0);
    }
  };

  async requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Cannabis App',
          message: 'Cannabis App access to your location ',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        Geolocation.getCurrentPosition(
          (position) => {
            //ToastAndroid.show(JSON.stringify(position), ToastAndroid.SHORT)
            this.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
            });
          },
          (error) => {
            //  ToastAndroid.show("Check your internet speed connection",ToastAndroid.SHORT);

            Platform.OS === 'android'
              ? ToastAndroid.show(
                  'Check your internet speed connection',
                  ToastAndroid.SHORT,
                )
              : Alert.alert('Check your internet speed connection');
          },
          {enableHighAccuracy: false, timeout: 10000},
        );
      } else {
        Alert.alert('location permission denied');
        //alert("Location permission denied");
      }
    } catch (err) {
      Alert.alert(JSON.stringify(err.message));
    }
  }

  async requestLocationPermissionIOS() {
    Geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        });
      },
      (error) => {
        //  ToastAndroid.show("Check your internet speed connection",ToastAndroid.SHORT);

        Platform.OS === 'android'
          ? ToastAndroid.show(
              'Check your internet speed connection',
              ToastAndroid.SHORT,
            )
          : Alert.alert('Check your internet speed connection');

        this.setState({loading_status: false});
      },
      {enableHighAccuracy: false, timeout: 10000},
    );
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  componentDidMount() {
    StatusBar.setBackgroundColor(colors.status_bar_color);
  }

  componentWillMount = async () => {
    this.myComponantWillMount();
  };

myComponantWillMount= async ()=>
{

//  console.log('ashishkumar verma=============')
this.setState({
  loading_status: false,
      post_text: '',
      //only for text
      post_text_error: false,
      //for post eroor full
      post_error: false,
      //no image or video error
      image_video_error: false,
      photo: null,
      imageSource: null,
      videoSource: null,
      //dummy is just temp vidoe or image
      imageSourceDummy: null,
      videoSourceDummy: null,
      video: null,
      refreshing: false,
      smiley: false,
      posts: [],
      latitude: 0.0,
      longitude: 0.0,
      deviceToken: '',
      chatsModalVisible: false,
      investorModalVisible: false,
      newsModalVisible: false,
      videoThumbnail: null,
      user_id:'',
      showMenu:false,
      AdvmenuItemId:'',
})
this.props.navigation.setParams({count: 0});
    getFCMToken().then((response) => {
      this.setState(
        {
          deviceToken: response,
        },
        () => {
          //save user data to database for firebase
          this.writeUserData(
            this.props.user.user_id,
            this.props.user.name,
            this.props.user.image,
            this.props.user.email,
            response,
          );
        },
      );
    });

    Platform.OS === 'android'
      ? await this.requestLocationPermission()
      : await this.requestLocationPermissionIOS();

    AppState.addEventListener('change', this._handleAppStateChange);

    // this.props.navigation.closeDrawer()
    this.getPosts();
    // firebaseSDK

    const {item} = this.props.user;
    let a = JSON.stringify(this.props.user);
}

  isValid() {
    const {post_text, imageSource, videoSource} = this.state;
    let valid = false;
    if (
      post_text.toString().trim().length > 0 &&
      (imageSource != null || videoSource != null)
    ) {
      valid = true;
      //return true
    }

    if (imageSource != null && videoSource != null) {
      this.ErroSheet.open();
      //open error sheet
      return false;
    } else if (post_text.toString().trim().length === 0) {
      this.setState({post_error: true, post_text_error: true});
      setTimeout(() => {
        this.setState({post_text_error: false, post_error: false});
      }, 4000);

      return false;
    } else if (imageSource == null && videoSource == null) {
      this.setState({post_error: true, image_video_error: true});

      setTimeout(() => {
        this.setState({post_error: false, image_video_error: false});
      }, 4000);
      return false;
    }

    return valid;
  }


  makePost() {
    if (this.isValid()) {
      AsyncStorage.getItem('user_id').then((item) => {
        if (item) {
          var formData = new FormData();

          formData.append('user_id', item);
          formData.append('title', 'title');
          formData.append('description', this.state.post_text);
          {
            this.state.imageSource != null
              ? formData.append('image', this.state.photo)
              : formData.append('video', this.state.video);
          }
          {
            this.state.imageSource != null
              ? formData.append('media_type', 1)
              : formData.append('media_type', 2);
          }

          this.setState({loading_status: true});
          let url = urls.base_url + 'api/create_post';
          fetch(url, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'multipart/form-data',
            },
            body: formData,
          })
            .then((response) => response.json())
            .then((responseJson) => {
              this.setState({loading_status: false});
              
              Snackbar.show({
                title: responseJson.message,
                duration: Snackbar.LENGTH_SHORT,
                backgroundColor: 'black',
                color: 'red',
                action: {
                  title: 'OK',
                  color: 'green',
                  onPress: () => {
                    /* Do something. */
                  },
                },
              });

              if (!responseJson.error) {
                this.setState({
                  imageSource: null,
                  post_text: '',
                  videoSource: null,
                  video: null,
                  photo: null,
                  posts: [],
                });
              //  this.getPosts();
            // this.componentDidMount()
                this.myComponantWillMount();
                
              } else {
              }
              
            })
            .catch((error) => {
              this.setState({loading_status: false});
              console.log('myError is ===',error)
              // Snackbar.show({
              //   title: 'Try Again !',
              //   duration: Snackbar.LENGTH_SHORT,
              //   backgroundColor: 'black',
              //   color: 'red',
              //   action: {
              //     title: 'OK',
              //     color: 'green',
              //     onPress: () => {
              //       /* Do something. */
              //     },
              //   },
              // });
            });
        } else {
          ToastAndroid.show('User not found !', ToastAndroid.LONG);
        }
      });
    }
  }

  likePosts(post_id, like_id) {
    AsyncStorage.getItem('user_id').then((item) => {
      if (item) {
        var formData = new FormData();

        formData.append('user_id', item);
        formData.append('post_id', post_id);
        formData.append('like_type', like_id);
        this.setState({loading_status: true});
        let url = urls.base_url + 'api/like_post';
        fetch(url, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
          },
          body: formData,
        })
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({loading_status: false});
            if (!responseJson.error) {
              Platform.OS === 'android'
                ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                : Alert.alert(responseJson.message);

              if (responseJson.like_update == 1) {
                //means i already like but updated the like flag

                var p = this.state.posts;
                p.map((item) => {
                  if (item.id == post_id) {
                    Object.assign(item, {like_flag: like_id});
                  }
                });

                this.setState({posts: p});
              } else {
                var p = this.state.posts;
                p.map((item) => {
                  if (item.id == post_id) {
                    let count = parseInt(item.likes_count) + 1;
                    Object.assign(item, {
                      like_flag: like_id,
                      likes_count: count,
                    });
                  }
                });
                this.setState({posts: p});
              }
            } else {
              Snackbar.show({
                title: responseJson.message,
                duration: Snackbar.LENGTH_SHORT,
                backgroundColor: 'black',
                color: 'red',
                action: {
                  title: 'OK',
                  color: 'green',
                  onPress: () => {
                    /* Do something. */
                  },
                },
              });
            }
          })
          .catch((error) => {
            this.setState({loading_status: false});

            Snackbar.show({
              title: 'Try Again !',
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor: 'black',
              color: 'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => {
                  /* Do something. */
                },
              },
            });
          });
      } else {
        ToastAndroid.show('User not found !', ToastAndroid.LONG);
      }
    });
  }

  getPost() {
    AsyncStorage.getItem('user_id').then((item) => {
      if (item) {
        var formData = new FormData();

        formData.append('user_id', item);
        formData.append('lat', this.state.latitude);
        formData.append('long', this.state.longitude);
        this.setState({loading_status: true});
        let url = urls.base_url + 'api/home_screen';
        fetch(url, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
          },
          body: formData,
        })
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({loading_status: false});
            if (!responseJson.error) {
              var requestCount = responseJson.request_count;
              this.props.navigation.setParams({count: requestCount});

              //report
              var length = responseJson.posts.length.toString();
              var temp_arr = [];

              //attaching a key to all objects
              var p = responseJson.posts;
              p.map((item) => {
                Object.assign(item, {smiley: false});
              });
              this.setState({posts: p});
            } else {
              Snackbar.show({
                title: responseJson.message,
                duration: Snackbar.LENGTH_SHORT,
                backgroundColor: 'black',
                color: 'red',
                action: {
                  title: 'OK',
                  color: 'green',
                  onPress: () => {
                    /* Do something. */
                  },
                },
              });
            }
          })
          .catch((error) => {
            this.setState({loading_status: false});

            Snackbar.show({
              title: 'Try Again !',
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor: 'black',
              color: 'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => {
                  /* Do something. */
                },
              },
            });
          });
      } else {
        ToastAndroid.show('User not found !', ToastAndroid.LONG);
      }
    });
  }

  getPosts() {
   // console.warn('Get posts -------------------------------------------------');
    AsyncStorage.getItem('user_id').then((item) => {
      if (item) {
        this.setState({user_id:item})
        var formData = new FormData();

        formData.append('user_id', item);
        formData.append('lat', this.state.latitude);
        formData.append('long', this.state.longitude);

        let obj = {
          user_id: item,
          lat: this.state.latitude,
          long: this.state.longitude,
          no_of_rec: 7,
        };

        this.setState({loading_status: true});

        fetchPosts(obj)
          .then((responseJson) => {
            this.setState({loading_status: false});

            if (!responseJson.error) {
              var requestCount = responseJson.request_count;
              var notify_chat = responseJson.user.notify_chat;
              this.props.navigation.setParams({count: requestCount});

              //report
              var length = responseJson.posts.length.toString();
              var temp_arr = [];

              //attaching a key to all objects
              var p = responseJson.posts;
              p.map((item) => {
                Object.assign(item, {smiley: false});
              });

              //1 means not bought package but in deadline mode
              // this.setState({posts:p})

              this.setState(
                {
                  chatsModalVisible: notify_chat == 1 ? true : false,
                  posts: [],
                },
                () => {
                  // this.setState({posts: p.splice(0, 25)});
                  this.setState({posts: p});
                },
              );
            } else {
              showMessage(responseJson.message);
            }
          })
          .catch((error) => {
            this.setState({loading_status: false});

            showMessage(error.message);
          });
      } else {
        ToastAndroid.show('User not found !', ToastAndroid.LONG);
      }
    });
  }

  deletePost = (post_id) => {
    AsyncStorage.getItem('user_id').then((item) => {
      if (item) {
        var formData = new FormData();
        formData.append('user_id', item);
        formData.append('post_id', post_id);
        this.setState({loading_status: true});
        let url = urls.base_url + 'api/delete_post';
        fetch(url, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
          },
          body: formData,
        })
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({loading_status: false});
            if (!responseJson.error) {
              Platform.OS === 'android'
                ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                : Alert.alert(responseJson.message);
              this.getPosts();
            } else {
              Snackbar.show({
                title: responseJson.message,
                duration: Snackbar.LENGTH_SHORT,
                backgroundColor: 'black',
                color: 'red',
                action: {
                  title: 'OK',
                  color: 'green',
                  onPress: () => {
                    /* Do something. */
                  },
                },
              });
            }
          })
          .catch((error) => {
            this.setState({loading_status: false});

            Snackbar.show({
              title: 'Try Again !',
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor: 'black',
              color: 'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => {
                  /* Do something. */
                },
              },
            });
          });
      } else {
        ToastAndroid.show('User not found !', ToastAndroid.LONG);
      }
    });
  };

  checkPermissionPhoto() {
    PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then(
      (response) => {
        if (response === true) {
          this.addPhoto();
        } else if (response === false) {
          Alert('Please enable camera permission in device settings.');
        }
      },
    );
  }

  addVideo() {
    const options = {
      title: 'Video Picker',
      mediaType: 'video',
      videoQuality: 'medium',
      durationLimit: 80,
      takePhotoButtonTitle: 'Take Video...',
      allowsEditing: true,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        let source = {uri: response.uri};
        var video = {
          name: 'name.mp4',
          uri: source.uri,
          type: 'video/mp4',
        };

        this.setState({
          videoSourceDummy: source,
          video: video,
        });

        this.VideoSheet.open();
      }
    });
  }

  addVideoCamera() {
    const options = {
      title: 'Video Picker',
      mediaType: 'video',
      videoQuality: 'low',
      durationLimit: 80,
      takePhotoButtonTitle: 'Take Video...',
      allowsEditing: true,
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
      },
    };

    //launchCamera
    //showImagePicker
    //launchImageLibrary
    ImagePicker.launchCamera(options, async (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        let source = {uri: response.uri};
        var video = {
          name: 'name.mp4',
          uri: source.uri,
          type: 'video/mp4',
        };
        // const path = Platform.select({
        //   android: { "value": response.path },
        //   ios: { "value": response.uri }
        //     }).value;
        // compressVideo(path)
        this.setState({
          videoSourceDummy: source,
          video: video,
        });

        this.VideoSheet.open();
      }
    });
  }

  addVideoGallery() {
    const options = {
      title: 'Video Picker',
      mediaType: 'video',
      videoQuality: 'low',
      durationLimit: 80,
      takePhotoButtonTitle: 'Take Video...',
      allowsEditing: true,
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
      },
    };

    //launchCamera
    //showImagePicker
    //launchImageLibcdrary

    ImagePicker.launchImageLibrary(options, async (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        let source = {uri: response.uri};
        var video = {
          name: 'name.mp4',
          uri: source.uri,
          type: 'video/mp4',
        };
        // const path = Platform.select({
        //   android: { "value": response.path },
        //   ios: { "value": response.uri }
        //   }).value;

     //  compressVideo(path)

        this.setState({
          videoSourceDummy: source,
          video: video,
        });

        this.VideoSheet.open();
      }
    });
  }

// compressVideo(path) {
//     return async () => {
//       console.log(`begin compressing ${path}`);
//       const origin = await ProcessingManager.getVideoInfo(path);
//       const result = await ProcessingManager.compress(path, {
//         width: origin.size && origin.size.width / 3,
//         height: origin.size && origin.size.height / 3,
//         bitrateMultiplier: 7,
//         minimumBitrate: 300000
//       });
//       const thumbnail =  await ProcessingManager.getPreviewForSecond(result.source);
//       return { path: result.source, thumbnail };
//     };

//   }

  addPhoto() {
    const options = {
      // maxWidth: 500,
      // maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    //launchCamera
    //showImagePicker
    //launchImageLibrary
    ImagePicker.launchCamera(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        let source = {uri: response.uri};

        var photo = {
          uri: response.uri,
          type: 'image/jpeg',
          name:
            Platform.OS == 'android'
              ? response.fileName
              : 'asdfsdfasdfsdf.jpeg',
        };

        this.setState({
          imageSourceDummy: source,
          photo: photo,
        });
        this.PhotoSheet.open();

        {
          /*photo is a file object to send to parameters */
        }
      }
    });
  }

  addImageCamera() {
    const options = {
      // maxWidth: 500,
      // maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    //launchCamera
    //showImagePicker
    //launchImageLibrary
    ImagePicker.launchCamera(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        let source = {uri: response.uri};

        var photo = {
          uri: response.uri,
          type: 'image/jpeg',
          name:
            Platform.OS == 'android'
              ? response.fileName
              : 'asdfsdfasdfsdf.jpeg',
        };

        this.setState({
          imageSourceDummy: source,
          photo: photo,
        });
        this.PhotoSheet.open();

        {
          /*photo is a file object to send to parameters */
        }
      }
    });
  }

  addImageGallery() {
    const options = {
      // maxWidth: 500,
      // maxHeight: 500,
      quality: 1,
      storageOptions: {
        skipBackup: true,
      },
    };

    //launchCamera
    //showImagePicker
    //launchImageLibrary
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        let source = {uri: response.uri};

        var photo = {
          uri: response.uri,
          type: 'image/jpeg',
          name: 
            Platform.OS == 'android'
              ? response.fileName
              : 'asdfsdfasdfsdf.jpeg',
        };

        this.setState({
          imageSourceDummy: source,
          photo: photo,
        });
        this.PhotoSheet.open();

        {
         
        }
      }
    });
  }

  //By just calling remove() from the reference of database will delete that record.
  // You can even use null with set() or update() to delete that record from the database

  deleteData() {
    firebase.database().ref('Users/').remove();
  }

  //To update the data of any of the object, you need to create reference of it and use update()
  //with the data that you want to change.
  updateSingleData(email) {
    firebase.database().ref('Users/').update({
      email,
    });
  }

  //Read From Database

  //It will read data once from the `Users` object and print it on console.
  //If we want to get data whenever there is any change in it,
  //we can use on instead of once
  readUserData() {
    firebase
      .database()
      .ref('Users/')
      .once('value', function (snapshot) {});
  }

  readUserDataOn() {
    firebase
      .database()
      .ref('Users/')
      .on('value', function (snapshot) {});
  }

  //To push data in any of the object and create array with unique keys
  //push update nahi karta ek extra data add krta hai

  writeUserDataPush(user_id, name, image, email) {
    firebase
      .database()
      .ref('UsersList/' + user_id)
      .push({
        user_id,
        name,
        image,
        email,
      })
      .then((data) => {
        //success callback
      })
      .catch((error) => {
        //error callback
      });
  }

  changeStatus(status) {
    firebase
      .database()
      .ref('Users/' + this.props.user.user_id)
      .update({
        active: status == 1 ? 'online' : 'offline',
      });
  }

  //Writing To Database
  //set update krta hai
  writeUserData(user_id, name, image, email, deviceToken) {
    firebase
      .database()
      .ref('Users/' + user_id)
      .set({
        user_id,
        name,
        image,
        email,
        device_token: deviceToken,
        active: 'online',
      })
      .then((data) => {
        //success callback
      })
      .catch((error) => {
        //error callback
      });

    //Add a Completion Callback

    //   firebase.database().ref('Users/'+ user_id).set({
    //     user_id,
    //     name,
    //     image,
    //     email
    //   }, function(error) {
    //     if (error) {
    //       // The write failed...
    //     } else {
    //       // Data saved successfully!
    //     }
    //   });
    // }
  }

  c() {
    let a = JSON.stringify(this.props.user);
  }

  addKey() {
    this.props.enterpreneurUser();
    this.c();
  }

  imageVideoCheck() {
    const {imageSource, videoSource} = this.state;

    if (imageSource == null && videoSource == null) {
      return null;
    } else if (imageSource != null && videoSource == null) {
      return <View></View>;
    } else if (videoSource != null && imageSource == null) {
      return (
        <View
          style={{
            height: null,
            width: '90%',
            backgroundColor: 'red',
            padding: 20,
          }}></View>
      );
    }
  }

  next(item) {
    let user_id = item.user_id;
    let obj = {
      user_id: user_id,
    };
    this.props.navigation.navigate('SellerProfile', {result: obj});
  }

  //1-Like
  // 2-Cool
  // 3-Awesome
  // 3-Fantastic

  renderItems = ({item, index}) => {
    return (
      <View
        style={{
          width: Dimensions.get('window').width * 0.95,
          height: null,
          backgroundColor: 'white',
          margin: 10,
          padding: 0,
          borderWidth: 0.5,
          elevation: 3,
        }}>
        <TouchableOpacity
          onPress={() => {
            if (this.state.posts[index].smiley == true) {
              this.state.posts[index].smiley = false;
              this.setState({posts: this.state.posts});
            }
          }}>
          {/* onPress={()=> this.next(item)} */}
          <TouchableOpacity onPress={() => this.next(item)}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <Image
                source={{uri: urls.base_url + item.user_pic}}
                style={{
                  height: 40,
                  width: 40,
                  overflow: 'hidden',
                  borderRadius: 20,
                  margin: 10,
                }}
              />

              <View style={{marginLeft: 8}}>
                <Text style={{marginBottom: 5, fontWeight: 'bold'}}>
                  {item.user_name}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    width: '90%',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={require('../assets/clock.png')}
                    style={{width: 17, height: 17, marginRight: 6}}
                    resizeMode="contain"
                  />

                  <Text style={{color: 'grey', fontSize: 13}}>
                    {item.created_at}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>

          <Text style={{margin: 5}}>{item.description}</Text>

          <TouchableOpacity onPress={() => {}}>
            <View
              style={[
                {width: '100%'},

                {
                  height: Dimensions.get('window').height * 0.25,
                  margin: 10,
                  alignSelf: 'center',
                },
              ]}>
              {item.media_type == '2' ? (
                <Video
                  source={{uri: urls.base_url + item.post_video}}
                  disableBack
                  disableFullscreen
                  disableVolume
                  disableSeekbar
                  muted={true}
                  autoplay={false}
                  paused={false}
                  onPause={() => {}}
                  onPlay={() => {}}
                  resizeMode="stretch"
                  showOnStart={true}
                  seekColor={colors.color_primary}
                  style={{width: '93%', height: '100%', alignSelf: 'center'}}
                />
              ) : (
                <Image
                  source={{uri: urls.base_url + item.post_img}}
                  resizeMode="cover"
                  style={{
                    alignSelf: 'center',
                    height: '100%',
                    width: '90%',
                    overflow: 'hidden',
                    flex: 1,
                  }}
                />
              )}
            </View>
          </TouchableOpacity>

          <View
            style={{
              width: '100%',
              backgroundColor: 'grey',
              height: 0.5,
            }}></View>

          <TouchableOpacity
            activeOpacity={0.6}
            style={{flexDirection: 'row', alignItems: 'center'}}
            onPress={() => this.likePosts(this.state.posts[index].id, 1)}
            onLongPress={() => {
              this.state.posts[index].smiley = true;
              this.setState({posts: this.state.posts});
            }}>
            <Image
              source={require('../assets/p1.png')}
              style={{
                height: 25,
                width: 25,
                overflow: 'hidden',
                borderRadius: 12,
                margin: 10,
              }}
            />

            <Text>{item.likes_count} likes</Text>
          </TouchableOpacity>

          {item.smiley ? (
            <View
              style={{
                position: 'absolute',
                bottom: 10,
                left: 10,
                backgroundColor: 'white',
                width: null,
                flexDirection: 'row',
                alignItems: 'center',
                borderRadius: 10,
                padding: 5,
                borderWidth: 1,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.likePosts(this.state.posts[index].id, 1);
                  this.state.posts[index].smiley = false;
                  this.setState({posts: this.state.posts});
                }}>
                <Image
                  source={require('../assets/p1.png')}
                  style={{width: 20, height: 20, marginRight: 10}}
                  resizeMode="contain"
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  this.likePosts(this.state.posts[index].id, 2);
                  this.state.posts[index].smiley = false;
                  this.setState({posts: this.state.posts});
                }}>
                <Image
                  source={require('../assets/p2.png')}
                  style={{width: 20, height: 20, marginRight: 10}}
                  resizeMode="contain"
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  this.likePosts(this.state.posts[index].id, 3);
                  this.state.posts[index].smiley = false;
                  this.setState({posts: this.state.posts});
                }}>
                <Image
                  source={require('../assets/p3.png')}
                  style={{width: 20, height: 20, marginRight: 10}}
                  resizeMode="contain"
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  this.likePosts(this.state.posts[index].id, 4);
                  this.state.posts[index].smiley = false;
                  this.setState({posts: this.state.posts});
                }}>
                <Image
                  source={require('../assets/p4.png')}
                  style={{width: 20, height: 20, marginRight: 10}}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
          ) : null}
        </TouchableOpacity>
      </View>
    );
  };

  handler = (post_id, like_id) => {
    this.likePosts(post_id, like_id);
  };

  setPosts = (post) => {
    this.setState({posts: post});
  };

  seeImage = (item) => {
    if (item.media_type == '1') {
      let obj = {
        item: item,
      };
      this.props.navigation.navigate('PostImageDetail', {result: obj});
    } else {
      let obj = {
        item: item,
      };
      this.props.navigation.navigate('PostImageDetail', {result: obj});
    }
  };

  validURL(str) {
    var pattern = new RegExp(
      '^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$',
      'i',
    ); // fragment locator

    return !!pattern.test(str);
  }

  seeURL = (url) => {
    if (url.includes('https://')) {
    } else if (url.includes('http://')) {
    } else {
      url = 'http://' + url;
    }

    let f = Platform.select({
      ios: () => {
        Linking.openURL(url);
      },
      android: () => {
        Linking.openURL(url).catch((err) =>
          console.error('An error occurred', err),
        );
      },
    });

    f();
  };

  nextPage(item) {
    if (item.media_type == 1) {
      //image
      let obj = {
        'image_url': urls.base_url + item.img,
        'item': item,
      };
      this.props.navigation.navigate("ImageView", {result: obj});
    } else if (item.media_type == 2) {
      //image
      let obj = {
        'video_url': item.video,
        'item': item,
      };
      this.props.navigation.navigate("VideoView", {result: obj});
    }
  }
  myPostDelete=(item)=>{
    // this._menu.hide();
    var formData = new FormData();
                      formData.append('user_id',this.state.user_id);
                      formData.append('advert_id',item.id);
                       fetch(urls.base_url +'api/delete_advert', {
                       method: 'POST',
                       headers: {
                         'Accept': 'application/json',
                         'Content-Type': 'multipart/form-data',
                       },
                      body: formData
                     }).then((response) => response.json())
                           .then((responseJson) => {
                 
                   // console.log('delete-->>',responseJson)
                    if(!responseJson.error){
                    //  console.log("hi")
                    this.myComponantWillMount()
                                    }
                  
                           }).catch((error) => {
                              console.log(error)
                           
                                  });
  }
  
  myPostEdit=(item)=>{
    var data={'post':item}
    this.props.navigation.navigate('EditAdvertiesement',{result:data})
  
  }
  myshowAdvMenu=(items)=>{
    if(items.id==this.state.AdvmenuItemId){
      this.setState({
        AdvmenuItemId:''
      })
    }else{
       this.setState({
      AdvmenuItemId:items.id
    })
    }
   
  }
  
  renderItem = ({item, index}) => {

    if (item.advert_flag == 0) {
      //post type
      return (
        <PostComponent
          posts={this.state.posts}
          index={index}
          handler={this.handler}
          setPosts={this.setPosts}
          seeImage={this.seeImage}
          deletePost={this.deletePost}
          userProps={this.props}
        />
      );
    } else if (item.advert_flag == 1) {
   //  console.log('this is advertiesment==>>>>',item)
      
      return (
        <View >
         <PostAdvertiesment
          item={item} 
          next={()=>this.nextPage(item)}
          myedit={()=>this.myPostEdit(item)}
          mydelete={()=>this.myPostDelete(item)}
          />
          {/* <View
            style={{
              width: '95%',
              padding: 6,
              marginBottom: 5,
              marginTop: 5,
              borderColor: '#999',
              borderWidth: 1,
              alignSelf: 'center',

            }}> 
             
             {
              this.state.user_id==item.user_id
              ?
              
            <View style={{zIndex:999,alignSelf:'flex-end',right:4}}>
            <Menu
            ref={this.setMenuRef}
            button={
            <TouchableOpacity onPress={this.showMenu} style={{width:30,height:30,top:7}}>
            <Image source={require('../assets/menu-options.png')}
                            style={[styles.menuImage,{alignSelf:'center'}]} resizeMode='contain'/>
            </TouchableOpacity>
            } >
     <MenuItem onPress={ ()=>{
                      Alert.alert(
                      "Alert", 
                      'Are you sure want to delete ?',
                      [
                        { text: "OK", onPress: () =>  this.myPostDelete(item)},
                        { text: "CANCEL", onPress: () => console.log("cancel Pressed") }
                      ],
                      { cancelable: false } 
                       ) }
                    } >Delete</MenuItem>
            <MenuItem onPress={()=>this.myPostEdit(item)}>Edit</MenuItem>
            </Menu>
          </View>
              : null 
             } 

                <TouchableOpacity
               onPress={() => {
              this.nextPage(item);
             }} >
            <Text style={{marginBottom: 10, color: 'blue', fontWeight: 'bold'}}>
              Sponsored Content{' '}
            </Text>

            <View style={{width: '100%', height: null}}>
              {item.media_type == '2' ? (
                <Video
                  video={{uri: urls.base_url + item.video}}
                  resizeMode="stretch"
                  thumbnail={{uri: urls.base_url + item.video_thumbnail}}
                  style={{
                    width: '100%',
                    height: 170,
                    alignSelf: 'center',
                    marginTop: -7,
                  }}
                />
              ) : (
                <AdvertiesmentImg myitem={urls.base_url + item.img} />
              )}

              <Text style={{fontSize: 14, fontWeight: '800'}}>{item.name}</Text>
              <Text style={{fontSize: 12, color: 'grey'}}>
                {item.description}
              </Text>
              {item.url && this.validURL(item.url) ? (
                <Text
                  onPress={() => this.seeURL(item.url)}
                  style={{fontSize: 10, color: 'blue'}}>
                  {item.url}
                </Text>
              ) : null}
            </View>
             </TouchableOpacity>
          </View>
       */}
        </View>
      );
    }
  };

  checkUploadedMediaType() {
    if (this.state.imageSource != null && this.state.videoSource == null) {
      return (
        <Image
          style={{height: 100, width: 100, alignSelf: 'center'}}
          source={this.state.imageSource}></Image>
      );
    }

    if (this.state.videoSource != null && this.state.imageSource == null) {
      return (
        <Image
          style={{height: 100, width: 100, alignSelf: 'center'}}
          source={require('../assets/video-img.jpg')}></Image>
      );
    }

    if (this.state.videoSource != null && this.state.imageSource != null) {
      return (
        <View style={{flexDirection: 'row', alignSelf: 'center'}}>
          <Image
            style={{height: 80, width: 80, alignSelf: 'center', marginRight: 5}}
            source={this.state.imageSource}></Image>

          <Image
            style={{height: 80, width: 80, alignSelf: 'center', marginLeft: 5}}
            source={require('../assets/video-img.jpg')}></Image>
        </View>
      );
    }
  }

  renderFooter = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 10,
        }}>
        <Text
          style={{
            color: colors.color_primary,
          }}>
          No More Post Found
        </Text>
      </View>
    );
  };

  renderHeader = () => {
    return (
      <View>
        {/* top item */}
        <View
          style={{
            backgroundColor: 'white',
            height: Dimensions.get('window').height * 0.13,
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            padding: 10,
            flex: 2,
            marginBottom: 0,
          }}>
          <View
            style={{
              flex: 1.6,
              height: '100%',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View
              style={{
                flex: 0.3,
                height: '100%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('MyProfile');

                  const resetAction = StackActions.reset({
                    index: 0,
                    key: 'MyProfile',
                    actions: [
                      NavigationActions.navigate({routeName: 'MyProfile'}),
                    ],
                  });

                  this.props.navigation.dispatch(resetAction);
                }}>
                <Image
                  style={{
                    height: 50,
                    width: 50,
                    borderRadius: 65,
                    overflow: 'hidden',
                  }}
                  source={{
                    uri: urls.base_url + this.props.user.image,
                  }}></Image>
              </TouchableOpacity>
            </View>

            <View style={{flex: 1.3, height: '100%'}}>
              <TextInput
                style={{
                  margin: 10,
                  width: '85%',
                  height: '75%',
                  borderColor: 'grey',
                  borderWidth: 1,
                  borderRadius: 10,
                  paddingTop: 4,
                  paddingBottom: 4,
                  paddingRight: 8,
                  paddingLeft: 8,
                  textAlignVertical: 'top',
                }}
                multiline={true}
                autoFocus={this.props.navigation.getParam('routs') ? true:false}
                placeholder={'Express your vibe ..'}
                value={this.state.post_text}
              //  keyboardType={Platform.OS === 'ios' ? 'ascii-capable' : 'visible-password'}
                onChangeText={(post_text) =>
                  this.setState({post_text})
                }></TextInput>
            </View>
          </View>

          <View
            style={{
              flex: 0.4,
              height: '100%',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View
              style={{
                flex: 1,
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={() => this.makePost()}>
                <View
                  style={{
                    marginLeft: 5,
                    backgroundColor: 'blue',
                    height: 35,
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingTop: 3,
                    borderRadius: 5,
                    paddingBottom: 3,
                    paddingLeft: 11,
                    paddingRight: 11,
                  }}>
                  <Text style={{color: 'white', fontSize: 12}}>Post</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View
          style={{
            flex: 1,
            height: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            marginBottom: 10,
          }}>
          <View
            style={{
              flex: 0.5,
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity onPress={() => this.ImageSheetChoose.open()}>
              <Image
                source={require('../assets/photo.png')}
                style={{width: 25, height: 25}}
                resizeMode="contain"
              />
            </TouchableOpacity>

            <Text style={{fontSize: 10}}>Photo</Text>
          </View>

          <View
            style={{
              flex: 0.5,
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity onPress={() => this.VideoSheetChoose.open()}>
              <Image
                source={require('../assets/video.png')}
                style={{width: 25, height: 25}}
                resizeMode="contain"
              />
            </TouchableOpacity>
            <Text style={{fontSize: 10}}>Video</Text>
          </View>
        </View>

        <View
          style={{
            width: '100%',
            height: 0.4,
            backgroundColor: 'grey',
            marginBottom: 10,
          }}></View>

        {/* top item  end*/}

        {this.state.imageSource != null || this.state.videoSource != null ? (
          <View
            style={{
              backgroundColor: 'rgb(0,0,0)',
              width: '90%',
              padding: 20,
              marginLeft: 20, 
              marginBottom: 5
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginBottom: 7,
              }}>
              <Text style={{color: colors.color_primary, fontSize: 17}}></Text>

              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    videoSource: null,
                    imageSource: null,
                  });
                }}
                style={{
                  padding: 6,
                  backgroundColor: 'red',
                  borderRadius: 20,
                }}>
                <Image
                  style={{height: 12, width: 12}}
                  source={require('../assets/cross.png')}></Image>
              </TouchableOpacity>
            </View>

            {this.checkUploadedMediaType()}
          </View>
        ) : null}

        {/* post error */}

        {this.state.post_error ? (
          <View
            style={{
              height: null,
              width: '90%',
              backgroundColor: 'white',
              borderColor: 'red',
              borderWidth: 0.5,
              elevation: 6,
              padding: 4,
              margin: 5,
              marginLeft: 20,
              shadowOpacity: 0.3,
            }}>
            {this.state.post_text_error ? (
              <Text
                style={{
                  color: colors.color_primary,
                  fontWeight: 'bold',
                  margin: 3,
                  textAlign: 'center',
                }}>
                Please Enter Post Description.
              </Text>
            ) : (
              <Text
                style={{
                  color: colors.color_primary,
                  fontWeight: 'bold',
                  margin: 3,
                  textAlign: 'center',
                }}>
                Please Enter one image/video .
              </Text>
            )}

            <View
              style={{
                flexDirection: 'row',
                flex: 4,
                justifyContent: 'space-around',
                alignItems: 'center',
              }}>
              <Text
                style={{flex: 3, fontSize: 11, marginTop: -2, color: 'grey'}}>
                Your Post should contain description with only one Image / Video
                .Kindly input all to upload your post.
              </Text>
            </View>
          </View>
        ) : null}

        {/* post error  end*/}
      </View>
    );
  };

  render() {
    const PhotoSheet = () => {
      return (
        <RBSheet
          ref={(ref) => {
            this.PhotoSheet = ref;
          }}
          height={240}
          duration={0}
          customStyles={{
            container: {
              justifyContent: 'center',
              alignItems: 'center',
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
            },
            // wrapper:{
            //   backgroundColor:'grey'
            // }
          }}
          animationType="fade"
          minClosingHeight={10}>
          <View
            style={{
              width: '60%',
              height: '80%',
              padding: 0,
              flex: 3,
              flexDirection: 'row',
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={this.state.imageSourceDummy}
              style={{height: '90%', width: '60%'}}
              resizeMode = 'stretch'
            />
          </View>

          <Text style={{margin: 10}}>
            Do you want to add this image to add to post ?
          </Text>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              flex: 2,
            }}>
            <View
              style={{
                flex: 1,
                margin: 10,
                elevation: 10,
                shadowColor: '#000000',
                shadowOffset: {
                  width: 0,
                  height: 3,
                },
                shadowRadius: 10,
                shadowOpacity: 0.6,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({imageSource: this.state.imageSourceDummy});
                }}
                style={{
                  width: '100%',
                  backgroundColor: colors.color_primary,
                  padding: 15,
                  justifyContent: 'center',
                  alignItems: 'center',
                  elevation: 10,
                }}>
                <Text style={{color: 'white'}}>Yes</Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                flex: 1,
                margin: 10,
                elevation: 10,
                shadowColor: '#000000',
                shadowOffset: {
                  width: 0,
                  height: 3,
                },
                shadowRadius: 10,
                shadowOpacity: 0.6,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.PhotoSheet.close();
                }}
                style={{
                  width: '100%',
                  backgroundColor: 'white',
                  padding: 15,
                  justifyContent: 'center',
                  alignItems: 'center',
                  elevation: 10,
                }}>
                <Text style={{color: colors.color_primary}}>No</Text>
              </TouchableOpacity>
            </View>
          </View>
        </RBSheet>
      );
    };

    const VideoSheet = () => {
      return (
        <RBSheet
          ref={(ref) => {
            this.VideoSheet = ref;
          }}
          height={250}
          width={Dimensions.get('window').width * 0.7}
          duration={0}
          customStyles={{
            container: {
              justifyContent: 'center',
              alignItems: 'center',
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
            },
            // wrapper:{
            //   backgroundColor:'grey'
            // }
          }}
          animationType="fade"
          minClosingHeight={10}>
           <View
            style={{
              height: '60%',

              backgroundColor: 'red',
              padding: 10,
              flexDirection: 'row',
              backgroundColor: 'white',
            }}>
            <Video
              video={this.state.videoSourceDummy}
              showOnStart={false}
              seekColor={colors.color_primary}
              resizeMode="stretch"
              style={{
                height: '100%',
                width: Dimensions.get('window').width * 0.9,
              }}
            />
          </View>

          <Text style={{margin: 10}}>
            Do you want to add this video to add to post ?
          </Text>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              flex: 2,
              marginBottom: 10,
            }}>
            <View
              style={{
                flex: 1,
                margin: 10,
                elevation: 10,
                shadowColor: '#000000',
                shadowOffset: {
                  width: 0,
                  height: 3,
                },
                shadowRadius: 10,
                shadowOpacity: 0.6,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({videoSource: this.state.videoSourceDummy});
                }}
                style={{
                  width: '100%',
                  backgroundColor: colors.color_primary,
                  padding: 15,
                  justifyContent: 'center',
                  alignItems: 'center',
                  elevation: 10,
                }}>
                <Text style={{color: 'white'}}>Yes</Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                flex: 1,
                margin: 10,
                elevation: 10,
                shadowColor: '#000000',
                shadowOffset: {
                  width: 0,
                  height: 3,
                },
                shadowRadius: 10,
                shadowOpacity: 0.6,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.VideoSheet.close();
                }}
                style={{
                  width: '100%',
                  backgroundColor: 'white',
                  padding: 15,
                  justifyContent: 'center',
                  alignItems: 'center',
                  elevation: 10,
                }}>
                <Text style={{color: colors.color_primary}}>No</Text>
              </TouchableOpacity>
            </View>
          </View>
        </RBSheet>
      );
    };

    const ErroSheet = () => {
      return (
        <RBSheet
          ref={(ref) => {
            this.ErroSheet = ref;
          }}
          height={180}
          duration={0}
          customStyles={{
            container: {
              justifyContent: 'center',
              alignItems: 'center',
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
            },
            // wrapper:{
            //   backgroundColor:'grey'
            // }
          }}
          animationType="fade"
          minClosingHeight={10}>
          <View
            style={{
              width: Dimensions.get('window').width * 0.94,
              height: '20%',
              backgroundColor: 'white',
              padding: 10,
              flexDirection: 'column',
              backgroundColor: 'white',
            }}>
            <Text>
              Looks like you have added both image and video in your post.You
              can post with only one image or video.{' '}
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              flex: 2,
              marginBottom: 10,
            }}>
            <View
              style={{
                flex: 1,
                margin: 10,
                elevation: 10,
                shadowColor: '#000000',
                shadowOffset: {
                  width: 0,
                  height: 3,
                },
                shadowRadius: 10,
                shadowOpacity: 0.6,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({imageSource: null, imageSourceDummy: null});
                }}
                style={{
                  width: '100%',
                  backgroundColor: colors.color_primary,
                  padding: 15,
                  justifyContent: 'center',
                  alignItems: 'center',
                  elevation: 10,
                }}>
                <Text style={{color: 'white'}}>Remove Image</Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                flex: 1,
                margin: 10,
                elevation: 10,
                shadowColor: '#000000',
                shadowOffset: {
                  width: 0,
                  height: 3,
                },
                shadowRadius: 10,
                shadowOpacity: 0.6,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({videoSource: null, videoSourceDummy: null});
                }}
                style={{
                  width: '100%',
                  backgroundColor: 'white',
                  padding: 15,
                  justifyContent: 'center',
                  alignItems: 'center',
                  elevation: 10,
                }}>
                <Text style={{color: colors.color_primary}}>Remove Video</Text>
              </TouchableOpacity>
            </View>
          </View>
        </RBSheet>
      );
    };

    const ChatsModal = () => {
      return (
        <Overlay
          isVisible={this.state.chatsModalVisible}
          windowBackgroundColor="rgba(0, 0, 0, .5)"
          overlayBackgroundColor="white"
          width={300}
          height="auto">
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <View
              style={{
                padding: 15,
                borderRadius: 30,
                borderColor: 'grey',
                borderWidth: 1,
                marginTop: -35,
                backgroundColor: 'white',
                elevation: 4,
                shadowOpacity: 0.5,
                marginBottom: 15,
              }}>
              <Image
                source={require('../assets/logo.png')}
                style={{width: 30, height: 30}}
                resizeMode="contain"
              />
            </View>

            <Text style={{textAlign: 'center', fontWeight: 'bold', margin: 6}}>
              Package Expired.
            </Text>
            <Text style={{textAlign: 'center', color: 'grey'}}>
              Please renew your Chats Package.
            </Text>

            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({chatsModalVisible: false});
                  this.props.navigation.navigate('ChatPackages');
                }}
                style={{
                  backgroundColor: colors.color_primary,
                  paddingLeft: 20,
                  paddingRight: 20,
                  paddingTop: 6,
                  paddingBottom: 6,
                  alignItems: 'center',
                  justifyContent: 'center',
                  margin: 10,
                }}>
                <Text style={{color: 'white', fontWeight: 'bold'}}>
                  Renew Now
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  this.setState({chatsModalVisible: false});
                }}
                style={{
                  backgroundColor: 'red',
                  paddingLeft: 30,
                  paddingRight: 30,
                  paddingTop: 6,
                  paddingBottom: 6,
                  alignItems: 'center',
                  justifyContent: 'center',
                  margin: 10,
                }}>
                <Text style={{color: 'white', fontWeight: 'bold'}}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Overlay>
      );
    };

    const VideoSheetChoose = () => {
      return (
        <RBSheet
          ref={(ref) => {
            this.VideoSheetChoose = ref;
          }}
          height={200}
          duration={0}
          customStyles={{
            container: {
              justifyContent: 'center',
              alignItems: 'center',
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
            },
            // wrapper:{
            //   backgroundColor:'grey'
            // }
          }}
          animationType="fade"
          closeOnDragDown={true}
          minClosingHeight={10}>
          <View
            style={{
              width: '80%',
              height: '80%',
              backgroundColor: 'white',
              padding: 10,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
                marginBottom: 20,
              }}>
              <Image
                style={{height: 40, width: 40}}
                resizeMode="contain"
                source={require('../assets/logo.png')}></Image>

              <Text style={{fontWeight: 'bold', fontSize: 16}}>
                Choose Video Source !{' '}
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                flex: 2,
              }}>
              <View
                style={{
                  flex: 1,
                  margin: 10,
                  elevation: 10,
                  shadowColor: '#000000',
                  shadowOffset: {
                    width: 0,
                    height: 3,
                  },
                  shadowRadius: 10,
                  shadowOpacity: 0.6,
                }}>
                <TouchableOpacity
                  onPress={() => this.addVideoCamera()}
                  style={{
                    width: '100%',
                    backgroundColor: colors.color_primary,
                    padding: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                    elevation: 10,
                  }}>
                  <Text style={{color: 'white'}}>Camera</Text>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  flex: 1,
                  margin: 10,
                  elevation: 10,
                  shadowColor: '#000000',
                  shadowOffset: {
                    width: 0,
                    height: 3,
                  },
                  shadowRadius: 10,
                  shadowOpacity: 0.6,
                }}>
                <TouchableOpacity
                  onPress={() => this.addVideoGallery()}
                  style={{
                    width: '100%',
                    backgroundColor: 'white',
                    padding: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                    elevation: 10,
                  }}>
                  <Text style={{color: colors.color_primary}}>Gallery</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </RBSheet>
      );
    };

    const ImageSheetChoose = () => {
      return (
        <RBSheet
          ref={(ref) => {
            this.ImageSheetChoose = ref;
          }}
          height={200}
          duration={0}
          customStyles={{
            container: {
              justifyContent: 'center',
              alignItems: 'center',
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
            },
            // wrapper:{
            //   backgroundColor:'grey'
            // }
          }}
          animationType="fade"
          closeOnDragDown={true}
          minClosingHeight={10}>
          <View
            style={{
              width: '80%',
              height: '80%',
              backgroundColor: 'white',
              padding: 10,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
                marginBottom: 20,
              }}>
              <Image
                style={{height: 40, width: 40}}
                resizeMode="contain"
                source={require('../assets/logo.png')}></Image>

              <Text style={{fontWeight: 'bold', fontSize: 16}}>
                Choose Image Source !{' '}
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                flex: 2,
              }}>
              <View
                style={{
                  flex: 1,
                  margin: 10,
                  elevation: 10,
                  shadowColor: '#000000',
                  shadowOffset: {
                    width: 0,
                    height: 3,
                  },
                  shadowRadius: 10,
                  shadowOpacity: 0.6,
                }}>
                <TouchableOpacity
                  onPress={() => this.addImageCamera()}
                  style={{
                    width: '100%',
                    backgroundColor: colors.color_primary,
                    padding: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                    elevation: 10,
                  }}>
                  <Text style={{color: 'white'}}>Camera</Text>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  flex: 1,
                  margin: 10,
                  elevation: 10,
                  shadowColor: '#000000',
                  shadowOffset: {
                    width: 0,
                    height: 3,
                  },
                  shadowRadius: 10,
                  shadowOpacity: 0.6,
                }}>
                <TouchableOpacity
                  onPress={() => this.addImageGallery()}
                  style={{
                    width: '100%',
                    backgroundColor: 'white',
                    padding: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                    elevation: 10,
                  }}>
                  <Text style={{color: colors.color_primary}}>Gallery</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </RBSheet>
      );
    };

    return (
      <SafeAreaView style={styles.container} forceInset={{top: 'never'}}>
        <PhotoSheet></PhotoSheet>
        <VideoSheet></VideoSheet>
        <ErroSheet></ErroSheet>
        <VideoSheetChoose />
        <ImageSheetChoose />
        <ChatsModal />

        <View
          style={{backgroundColor: 'white', width: '100%', flex: 1}}
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              style={{backgroundColor: 'transparent'}}
              refreshing={this.state.loading_status}
              onRefresh={() => this.getPosts()}
            />
          }
          contentContainerStyle={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>

          <FlatList
            style={{marginBottom: 10, width: '100%'}}
            data={this.state.posts}
            extraData={this.state}
            refreshing={this.state.refreshing}
            showsVerticalScrollIndicator={false}
            renderItem={(item, index) => this.renderItem(item, index)}
            keyExtractor={(item) => item.id.toString()}
            contentContainerStyle={{paddingBottom: 20}}
            ListFooterComponent={this.renderFooter()}
            ListHeaderComponent={this.renderHeader()}
            removeClippedSubviews={true}
            maxToRenderPerBatch={60}
            windowSize={30}
            initialNumToRender={14}
            shouldComponentUpdate={false}
            onEndReachedThreshold={0.001}
            legacyImplementation={true}
            bounces={false}
            // // Performance settings
            // removeClippedSubviews={true} // Unmount components when outside of window
            // initialNumToRender={2} // Reduce initial render amount
            // maxToRenderPerBatch={1} // Reduce number in each render batch
            // updateCellsBatchingPeriod={100} // Increase time between renders
            // windowSize={7} // Reduce the window size
            // legacyImplementation= {true}
          />
        </View>

        {this.state.loading_status && (
          <View
            style={[
              StyleSheet.absoluteFill,
              {backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center'},
            ]}>
            <WaveIndicator color={colors.color_primary} />
          </View>
        )}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    change: (userinfo) => dispatch(changeUser(userinfo)),
    enterpreneurUser: () => dispatch(enterpreneurUser()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.status_bar_color,
  },

  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)',
  },
  menuImage:{width:17,height:17,marginRight:6,flex:1},
});
