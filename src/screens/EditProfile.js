import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { Dropdown } from 'react-native-material-dropdown';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import DateTimePicker from 'react-native-modal-datetime-picker';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import Snackbar from 'react-native-snackbar';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import { throwStatement } from '@babel/types';



class EditProfile extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      name: '',
      dob: '',
      date_obj:null,
      country_id:'',
      countries:[],
      country_name:'',
      state_id:'',
      states:[],
      state_name:'',
      city_id:'',
      city_name:'',
      cities:[]
     
    };


  }

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
           <TouchableOpacity onPress={()=>{navigation.goBack() }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Edit Profile</Text>
     </View>
    ),
  });

   foo =  (id,name,email,image) => {
  
    this.props.change({ user_id : id, name : name, image:image, email:email });
   }
  
  save1 = async () => {
   
    await this.foo("2","sadf","sadfksdfllflflfl","asdfdsf")
  
    let a = JSON.stringify(this.props.user)
    console.log("val----gggg-------",a)  
  }


isValid() {
    const { name,dob,country_id,state_id,city_id } = this.state;

    let valid = false;

    if (name.toString().trim().length > 0 
      &&  dob.toString().trim().length > 0
      &&  country_id.toString().trim().length > 0
      &&  state_id.toString().trim().length > 0
      &&  city_id.toString().trim().length > 0) {
      valid = true;
    }



    else if (name.toString().trim().length == 0){
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter Cannabis Expression', ToastAndroid.SHORT)
      //   : Alert.alert('Enter Cannabis  Expression')


        Snackbar.show({
          title: 'Enter Cannabis Expression',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
        });


      return false;
    }

    else if (dob.toString().trim().length === 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter date of birth', ToastAndroid.SHORT)
      //   : Alert.alert('Enter date of birth')

        
        Snackbar.show({
          title: 'Enter date of birth',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
        });


      return false;
    }
    else if (country_id.toString().trim().length == 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter Country', ToastAndroid.SHORT)
      //   : Alert.alert('Enter Country')


        
        Snackbar.show({
          title: 'Enter  Country',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
        });


      return false;
    }

    else if (state_id.toString().trim().length == 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter State', ToastAndroid.SHORT)
      //   : Alert.alert('Enter State')

        
        Snackbar.show({
          title: 'Enter State',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
        });


      return false;
    }

    else if (city_id.toString().trim().length == 0) {
      // Platform.OS === 'android'
      //   ? ToastAndroid.show('Enter City', ToastAndroid.SHORT)
      //   : Alert.alert('Enter City')


        
        Snackbar.show({
          title: 'Enter City',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor:'black',
          color:'red',
          action: {
            title: 'OK',
            color: 'green',
            onPress: () => { /* Do something. */ },
          },
        });


      return false;
    }
    

    return valid
  }

 



  editProfile(){
   // console.log("Get profile")
   if(this.isValid()){
     AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
       var formData = new FormData();

        formData.append('user_id',item);
        formData.append('name', this.state.name);
      
       formData.append('country_id', this.state.country_id);
       formData.append('city_id', this.state.city_id);
       formData.append('state_id', this.state.state_id);
       formData.append('dob', this.state.date_obj);
        

                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/edit_profile'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){
                               
                                
                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                          });
                                
                                 var  user_id = responseJson.result.id
                                var name = responseJson.result.name
                                var email = responseJson.result.email
                                var image = responseJson.result.user_image

                                try {
                                  AsyncStorage.setItem('name', name);
                                 
                                 } catch (error) {
                                  
                                   //ToastAndroid.show(error.message,ToastAndroid.LONG)
                                 }

                                 this.foo(user_id,name,email,image)

                                  this.props.navigation.navigate('MyProfile');
                                  const resetAction = StackActions.reset({
                                  index: 0,
                                  key: 'MyProfile',
                                  actions: [NavigationActions.navigate({ routeName: 'MyProfile' })],
                                });
               
                           this.props.navigation.dispatch(resetAction);
                            }
                            else{

                             
                               
                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                          });
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})

                            console.log(JSON.stringify(error))
                            
                          
                            Snackbar.show({
                              title: error.message,
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
   }
    
  }


  convertDate(date) {
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth()+1).toString();
    var dd  = date.getDate().toString();

    var mmChars = mm.split('');
    var ddChars = dd.split('');

    return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
  }


    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
  _hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
   
  }

  handleDatePicked = (date) => {

    console.log('A date has been picked: ',this.convertDate(date));
    this.setState({date_obj:this.convertDate(date),dob:date.toString().substring(4,15),isDateTimePickerVisible: false})
    
  };



 fetchCountries = async () =>{
    this.setState({loading_status:true})

               let url = urls.base_url +'api/countries'
                    fetch(url, {
                    method: 'POST',

                    }).then((response) => response.json())
                        .then((responseJson) => {

                         // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          this.setState({loading_status:false})
                          if(!responseJson.error){
                           
                            var length = responseJson.data.length.toString();
                            var temp_arr=[]
                            for(var i = 0 ; i < length ; i++){
                            var id = responseJson.data[i].id
                            var name = responseJson.data[i].name
                          
                           
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], name: name };
                                  array[i] = { ...array[i], value : name };

                                  temp_arr = array
                                  
                                  //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                            }
                            this.setState({ countries : temp_arr});

                          }


                        else{
                        
                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                          });
                        }

                      }
                        ).catch((error) => {

                          this.setState({loading_status:false})
                         
                          Snackbar.show({
                            title: "Error",
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                          });
                        });



  }

  fetchStates = async (countryId) =>{
    this.setState({loading_status:true,state_id:'',city_id:'',city_name:'',state_name:''})

    var formData = new FormData();
    formData.append('country_id', countryId);

               let url = urls.base_url +'api/states'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                    body: formData

                    }).then((response) => response.json())
                        .then((responseJson) => {

                         // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          this.setState({loading_status:false})
                          if(!responseJson.error){
                           
                            var length = responseJson.data.length.toString();
                            var temp_arr=[]
                            for(var i = 0 ; i < length ; i++){
                            var id = responseJson.data[i].id
                            var name = responseJson.data[i].name
                          
                           
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], name: name };
                                  array[i] = { ...array[i], value : name };

                                  temp_arr = array
                                  
                                  //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                            }
                            this.setState({ states : temp_arr});

                          }


                        else{
                         
                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                          });
                        }

                      }
                        ).catch((error) => {

                          this.setState({loading_status:false})
                         
                          Snackbar.show({
                            title: 'Error ',
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                          });
                        });



  }
  fetchCities = async (stateId) =>{
    this.setState({loading_status:true,city_id:'',city_name:''})
    var formData = new FormData();
    formData.append('state_id',stateId);

    formData.append('country_id',this.state.country_id);


               let url = urls.base_url +'api/cities'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                    body: formData

                    }).then((response) => response.json())
                        .then((responseJson) => {

                         // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          this.setState({loading_status:false})
                          if(!responseJson.error){
                           
                            var length = responseJson.data.length.toString();
                            var temp_arr=[]
                            for(var i = 0 ; i < length ; i++){
                            var id = responseJson.data[i].id
                            var name = responseJson.data[i].name
                          
                           
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], name: name };
                                  array[i] = { ...array[i], value : name };

                                  temp_arr = array
                                  
                                  //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                            }
                            this.setState({ cities : temp_arr});

                          }


                        else{
                         

                          Snackbar.show({
                            title: responseJson.message,
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                          });
                        }

                      }
                        ).catch((error) => {

                          this.setState({loading_status:false})
                          Snackbar.show({
                            title: 'Error',
                            duration: Snackbar.LENGTH_SHORT,
                            backgroundColor:'black',
                            color:'red',
                            action: {
                              title: 'OK',
                              color: 'green',
                              onPress: () => { /* Do something. */ },
                            },
                          });
                        });



  }


 async componentWillMount(){

 
  
   //this.getProfile()
   let result = this.props.navigation.getParam('result')
   var name = result['name']
   var dob = result['dob']

    var country_id = result['country_id']
   var country_name = result['country_name']
     var state_id = result['state_id']
   var state_name = result['state_name']
    var city_id = result['city_id']
   var city_name = result['city_name']

   this.setState({name:name})

   
   await this.fetchCountries()
   await this.fetchStates(country_id)
   await this.fetchCities(state_id)


  

   this.setState({
     name:name,
     dob:dob,
     date_obj:dob,
      country_id:country_id,
     country_name:country_name,
    state_id:state_id,
    state_name:state_name,
   city_id:city_id,
  city_name:city_name
   })


  
  }



   
 

    render()
    {


  
      
        return(
          <SafeAreaView style={styles.container}>

    

    
    
            <ScrollView style={{backgroundColor:'white',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

  <View style={{ width: '90%', justifyContent: 'center', alignSelf: 'center' ,marginBottom:30}}>
  

               <TextField
              label='Cannabis Expression'
              value={this.state.name}
              onChangeText={(name) => this.setState({ name})}
              ref={(input) => { this.name = input; }}
            
                  />


          <DateTimePicker
          mode="date"
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this._hideDateTimePicker}
         maximumDate={new Date()}
          >
          </DateTimePicker>


          <TouchableOpacity  onPress={this._showDateTimePicker}>
            <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between',
            borderBottomColor:this.state.dob.length > 0 ? 'grey' : 'grey',
            borderBottomWidth:0.5,paddingBottom:5,marginTop:10}}>
                <View>
                {
                  this.state.dob.length > 0
                  ? <Text  style={{color:'grey',fontSize:13}}>Date of Birth</Text>
                  : <Text></Text>
                }
                {
                  this.state.dob.length > 0
                  ?  <Text>{this.state.dob}</Text>
                  : <Text style={{color:'grey',fontSize:15}}>Date of Birth</Text>
                }
              

                </View>


            <Image source={require('../assets/calendar.png')} style={{width:20,height:20,marginLeft:-23}} resizeMode='contain'/>
        </View>
        </TouchableOpacity>



          <Dropdown
          label='Select Your Country'
          rippleCentered={true}
          valueExtractor={({value})=> value}
          onChangeText = {(value, index, data) =>{
            console.log(`Selected value: ${value} ${index} ${data[index].id}`)
           // value is text name or label
           //index is index of item
           //data[index].id is id of the country
           this.setState({country_id:data[index].id,country_name:data[index].name})
           this.fetchStates(data[index].id)

          }}
          data={this.state.countries}
          value={this.state.country_name}
          ref={(input) => { this.country = input; }}
        />

        <Dropdown
          label='Select Your State'
          data={this.state.states}
          rippleCentered={true}
          valueExtractor={({value})=> value}
       
          onChangeText = {(value, index, data) =>{
           this.setState({state_id:data[index].id,state_name:data[index].name})
           this.fetchCities(data[index].id)

          }}
          value={this.state.state_name}
        />
        <Dropdown
          label='Select Your City'
          data={this.state.cities}
          rippleCentered={true}
          valueExtractor={({value})=> value}
         
          onChangeText = {(value, index, data) =>{
           this.setState({city_id:data[index].id,city_name:data[index].name})

          }}
          value={this.state.city_name}
        />

          </View>


          <TouchableOpacity style={{width:'100%'}} onPress={() =>{this.editProfile()}}>
            <View style={{
              width: '90%', height: 50, backgroundColor: colors.color_primary, justifyContent: 'center',
              alignItems: 'center', alignSelf: 'center', marginTop: 50, borderRadius: 5,
              shadowColor: 'gray',
              shadowRadius: 10,
              shadowOpacity: 1,
              elevation:10

            }}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Update</Text>
            </View>
          </TouchableOpacity>
    
             
    
            </ScrollView>

             
    
            {this.state.loading_status &&
              <View pointerEvents="none" style={styles.loading}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });