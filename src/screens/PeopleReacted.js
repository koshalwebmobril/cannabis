import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
//import Video from 'react-native-video-controls';
import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import { throwStatement } from '@babel/types';
import { StackActions, NavigationActions} from 'react-navigation';
import ChatTextInput from '../components/ChatTextInput';
import FastImage from 'react-native-fast-image'


class PeopleReacted extends React.Component {

  constructor(props) {
    super(props);
    this.state={
     
      loading_status:false,
      selectedTab:1,
      like1:[],
      like2:[],
      like3:[],
      like4:[],
      like5:[],
     
     
    };


  }

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                 <TouchableOpacity onPress={()=>{
                  navigation.goBack()
 
                  }}>
                  <FastImage source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode={FastImage.resizeMode.contain}/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>People who reacted</Text>
     </View>
    ),
  });



  _getReactionLikeOne(postId) {
    console.log("_getReactionLikeOne------")

       var formData = new FormData();

        formData.append('post_id',postId);
        formData.append('type',1);
        

               this.setState({loading_status:true})
               let url = urls.base_url +'api/get_liked_post_by_types'
                fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                     body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                               this.setState({like1:responseJson.data})

                            }
                            else{

                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: "Try Again !",
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
      
   

    
  }


  _getReactionLikeTwo(postId) {
    console.log("_getReactionLikeTwo------")

       var formData = new FormData();

        formData.append('post_id',postId);
        formData.append('type',2);
        

               this.setState({loading_status:true})
               let url = urls.base_url +'api/get_liked_post_by_types'
                fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                     body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                               this.setState({like2:responseJson.data})

                            }
                            else{

                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: "Try Again !",
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
      
   

    
  }


  _getReactionLikeThree(postId) {
    console.log("_getReactionLikeThree------")

       var formData = new FormData();

        formData.append('post_id',postId);
        formData.append('type',3);
        

               this.setState({loading_status:true})
               let url = urls.base_url +'api/get_liked_post_by_types'
                fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                     body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                                this.setState({like3:responseJson.data})

                            }
                            else{

                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: "Try Again !",
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
      
   

    
  }


  _getReactionLikeFour(postId) {
    console.log("_getReactionLikeFour------")

       var formData = new FormData();

        formData.append('post_id',postId);
        formData.append('type',4);
        

               this.setState({loading_status:true})
               let url = urls.base_url +'api/get_liked_post_by_types'
                fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                     body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                                this.setState({like4:responseJson.data})

                            }
                            else{

                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: "Try Again !",
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
      
   

    
  }

  _getReactionLikeFive(postId) {
    console.log("_getReactionLikeFive------")

       var formData = new FormData();

        formData.append('post_id',postId);
        formData.append('type',5);
        

               this.setState({loading_status:true})
               let url = urls.base_url +'api/get_liked_post_by_types'
                fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                     body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                                this.setState({like5:responseJson.data})

                            }
                            else{

                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: "Try Again !",
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
      
   

    
  }




  next(user_id){
   
    let obj={
      'user_id':user_id
    }
      this.props.navigation.navigate("SellerProfile",{result:obj})
  
  
 
}



  renderItem = ({item, index}) => {
    return(
      <TouchableWithoutFeedback style={{margin:4}}>

      
                  <View style={{height:null,flexDirection:'row',
                    justifyContent:'flex-start',alignItems:'center'}}>
                    <Image source={{uri:urls.base_url + item.user.user_image}} 
                    style={{width:30,height:30,borderRadius:15,overflow:'hidden'}} 
                    resizeMode='cover'/>

          

          
                        <Text style={{marginLeft:15}}>{item.user.name}</Text>
                    
                  
                    </View>



        </TouchableWithoutFeedback>
    )
   }


   FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.4,
          width: "101%",
          backgroundColor: "#aaa9ad",
          margin:3
        }}
      />
    );
  }


  componentWillMount(){
    let result = this.props.navigation.getParam('result')
    var post_id = result.post_id
    this._getReactionLikeOne(post_id)
    this._getReactionLikeTwo(post_id)
    this._getReactionLikeThree(post_id)
    this._getReactionLikeFour(post_id)
    this._getReactionLikeFive(post_id)
  }

  checkView(){
    const {selectedTab,like1,like2,like3,like4,like5} = this.state


    if(selectedTab == 1){
     
       if(like1.length > 0){
        return(
          <FlatList
          style={{marginBottom:10,width:'100%',padding:4}}
          data={this.state.like1}
          ItemSeparatorComponent = { this.FlatListItemSeparator }
          showsVerticalScrollIndicator={false}
          renderItem={(item,index) => this.renderItem(item,index)}
        />
        )
       }
       else{
         return  <Text style={{margin:10,color:'grey'}}>No reaction</Text>
       }
    }
    else if(selectedTab == 2){
     
      if(like2.length > 0){
       return(
         <FlatList
         style={{marginBottom:10,width:'100%',padding:4}}
         data={this.state.like2}
         ItemSeparatorComponent = { this.FlatListItemSeparator }
         showsVerticalScrollIndicator={false}
         renderItem={(item,index) => this.renderItem(item,index)}
       />
       )
      }
      else{
        return  <Text style={{margin:10,color:'grey'}}>No reaction</Text>
      }
   }
   else if(selectedTab == 3){
     
    if(like3.length > 0){
     return(
       <FlatList
       style={{marginBottom:10,width:'100%',padding:4}}
       data={this.state.like3}
       ItemSeparatorComponent = { this.FlatListItemSeparator }
       showsVerticalScrollIndicator={false}
       renderItem={(item,index) => this.renderItem(item,index)}
     />
     )
    }
    else{
      return  <Text style={{margin:10,color:'grey'}}>No reaction</Text>
    }
 }
 else if(selectedTab == 4){
     
  if(like4.length > 0){
   return(
     <FlatList
     style={{marginBottom:10,width:'100%',padding:4}}
     data={this.state.like4}
     ItemSeparatorComponent = { this.FlatListItemSeparator }
     showsVerticalScrollIndicator={false}
     renderItem={(item,index) => this.renderItem(item,index)}
   />
   )
  }
  else{
    return  <Text style={{margin:10,color:'grey'}}>No reaction</Text>
  }
}
else if(selectedTab == 5){
     
  if(like5.length > 0){
   return(
     <FlatList
     style={{marginBottom:10,width:'100%',padding:4}}
     data={this.state.like5}
     ItemSeparatorComponent = { this.FlatListItemSeparator }
     showsVerticalScrollIndicator={false}
     renderItem={(item,index) => this.renderItem(item,index)}
   />
   )
  }
  else{
    return  <Text style={{margin:10,color:'grey'}}>No reaction</Text>
  }
}
  }

    render()
    {

        return(
          <SafeAreaView style={styles.container}>

    
            <ScrollView style={{backgroundColor:'white',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}>

            <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'center',
             borderBottomColor:'grey',borderBottomWidth:0.6,padding:5}}>
            <TouchableOpacity onPress={()=>this.setState({selectedTab:1})}>
            <FastImage
                style={styles.emotica}
                source={require('../assets/p1.png')}
                resizeMode={FastImage.resizeMode.contain}/>

                <Text style={{alignSelf:'center'}}>{this.state.like1.length}</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={()=>this.setState({selectedTab:2})}>
            <FastImage
                style={styles.emotica}
                source={require('../assets/p2.png')}
                resizeMode={FastImage.resizeMode.contain}/>
                <Text style={{alignSelf:'center'}}>{this.state.like2.length}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.setState({selectedTab:3})}>
            <FastImage
                style={styles.emotica}
                source={require('../assets/p3.png')}
                resizeMode={FastImage.resizeMode.contain}/>
                <Text style={{alignSelf:'center'}}>{this.state.like3.length}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.setState({selectedTab:4})}>
            <FastImage
                style={styles.emotica}
                source={require('../assets/p4.png')}
                resizeMode={FastImage.resizeMode.contain}/>
                <Text style={{alignSelf:'center'}}>{this.state.like4.length}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.setState({selectedTab:5})}>
            <FastImage
                style={styles.emotica}
                source={require('../assets/p5.png')}
                resizeMode={FastImage.resizeMode.contain}/>
                <Text style={{alignSelf:'center'}}>{this.state.like5.length}</Text>
            </TouchableOpacity>
            
            
            </View>
     

            {this.checkView()}



    
            </ScrollView>

             
    
         
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(PeopleReacted);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  },
  emotica:{
      height:30,width:30,marginBottom:10,marginTop:10
  }
  });