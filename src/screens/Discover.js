import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import AsyncStorage from '@react-native-community/async-storage';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import Snackbar from 'react-native-snackbar';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';



class Discover extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
     users:[]
     
    };


  }


  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
         
          <TouchableOpacity onPress={()=>{ navigation.navigate("Chats",{route_name:'Discover'})}}>
          <Image source={require('../assets/chat.png')} style={{width:30,height:30,marginLeft:20}} resizeMode='contain'/>
          </TouchableOpacity>
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                  <TouchableOpacity onPress={()=>{ navigation.pop()}}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Discover</Text>
            </View>
    ),
  });

  vibeUser(user_id){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
  
       
  
        var formData = new FormData();
  
        formData.append('user_id',item);
        formData.append('follow_user_id',user_id);
        
  
                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/vibe_user'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData
  
                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
  
            //  Platform.OS === 'android' 
            //                     ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
            //                     : Alert.alert(responseJson.message)



                                Snackbar.show({
                                  title: responseJson.message,
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });
  
  
                              if(!responseJson.error){
  
                                var p = this.state.users
                                p.map(item=>{
                                 if(item.id == user_id){
                                
                                  Object.assign(item,{vibing_flag:1})
                                 
                                 }
                                })
                                this.setState({users:p})
                            }
                            else{
  
                             
                                
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            Snackbar.show({
                              title: 'Try Again !',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });
  
                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }


  unVibeUser(user_id){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
  
       
  
        var formData = new FormData();
  
        formData.append('user_id',item);
        formData.append('follow_user_id',user_id);
        
  
                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/unvibe_user'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData
  
                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
  
            //  Platform.OS === 'android' 
            //                     ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
            //                     : Alert.alert(responseJson.message)



                                Snackbar.show({
                                  title: responseJson.message,
                                  duration: Snackbar.LENGTH_SHORT,
                                  backgroundColor:'black',
                                  color:'red',
                                  action: {
                                    title: 'OK',
                                    color: 'green',
                                    onPress: () => { /* Do something. */ },
                                  },
                                });
  
  
                              if(!responseJson.error){
  
                                var p = this.state.users
                                p.map(item=>{
                                 if(item.id == user_id){
                                
                                  Object.assign(item,{vibing_flag:0})
                                 
                                 }
                                })
                                this.setState({users:p})
                            }
                            else{
  
                             
                                
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            Snackbar.show({
                              title: 'Try Again !',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });
  
                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }

  
  getUsers(){
                  var formData = new FormData();
                
                   formData.append('user_id',this.props.user.user_id);
                   this.setState({loading_status:true})

                   console.log("G-----",JSON.stringify(formData))
                      let url = urls.base_url +'api/get_users'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                      body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                                
                               
                                     this.setState({users:responseJson.data})

                            }
                            else{

                             
                               
                              Snackbar.show({
                                title: responseJson.message,
                                duration: Snackbar.LENGTH_SHORT,
                                backgroundColor:'black',
                                color:'red',
                                action: {
                                  title: 'OK',
                                  color: 'green',
                                  onPress: () => { /* Do something. */ },
                                },
                              });

                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: 'Try Again !',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
  }






  componentWillMount(){
   this.getUsers()
  
  }

  next(user_id){
   
      let obj={
        'user_id':user_id
      }
        this.props.navigation.navigate("SellerProfile",{result:obj})
    
    
   
 }


renderItem = ({item, index}) => {
    return(
      <TouchableWithoutFeedback onPress={()=>{this.next(item.id)}}>
      <View style={{width:'100%',padding:6,marginBottom:2,marginTop:2}}>

       <View style={{width:'100%',height:null,flexDirection:'row',
      justifyContent:'space-between',alignItems:'center'}}>

                  <View style={{height:null,flexDirection:'row',
                    justifyContent:'flex-start',alignItems:'center'}}>
                    <Image source={{uri:urls.base_url + item.user_image}} 
                    style={{width:30,height:30,borderRadius:15,overflow:'hidden'}} 
                    resizeMode='cover'/>

                  {
                       item.seller_flag == 1
                       ?
                       <View style={{position:'absolute',bottom:20,left:16}}>
                         <Image source={require('../assets/tag.png')} 
                                  style={{width:20,height:20}} resizeMode='contain'/>

                   </View>

                          :null
                     }

                    {
                      item.name==null?
                     <Text style={{marginLeft:15}}>No name</Text>
                     :   <Text style={{marginLeft:15}}>{item.name}</Text>
                    }
                  
                    </View>



                    <TouchableOpacity onPress={()=> {this.vibeUser(item.id)}}>
                    <View style={{padding:6,height:null,width:null,
                      justifyContent:'center',alignItems:'center',
                      backgroundColor:item.vibing_flag == 0 ?  '#CDCDCD' : null,
                      margin:0,flex:1.6,alignSelf:'center'}}>
                      {
                        item.vibing_flag == 0
                        ?  <Text>Vibe +</Text>
                        
                        :  
                        
            
              <View style={{flexDirection:'row',
              justifyContent:'flex-start',
              alignItems:'center',marginLeft:3,marginRight:-5}}>
                  <TouchableOpacity onPress={()=>{this.unVibeUser(item.id)}}>
                    <View style={{justifyContent:'center',
                    alignItems:'center',
                    paddingBottom:5,
                    paddingTop:5,
                    paddingLeft:10,
                    paddingRight:10,backgroundColor:'red'}}>
                    <Text style={{color:'white'}}>Unvibe</Text>

                    </View>
                    </TouchableOpacity>


                    {/* <TouchableOpacity onPress={()=>{}}>
                    <View style={{justifyContent:'center',
                    alignItems:'center',
                    paddingBottom:5,
                    paddingTop:5,
                    paddingLeft:10,
                    paddingRight:10,backgroundColor:'green',marginLeft:10}}>
                    <Text style={{color:'white'}}>Vibing</Text>

                    </View>
                    </TouchableOpacity> */}
            

            </View>

               
                        
                      }
                     
                    </View>
                    </TouchableOpacity>
    
       </View>

        {/* <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#d3d3d3",
          marginBottom:1,marginTop:5
        }}
      /> */}
    

     </View>
     
      </TouchableWithoutFeedback>
    )
   }


  



   FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "101%",
          backgroundColor: "#aaa9ad",
          margin:3
        }}
      />
    );
  }

   s(){
    new Promise((resolve, reject) => {
			this.props.change({ user_id : 4, name : 'dssdsdfdsf', image:'image', email:'email' })
		})
		.then( success => {
			Alert.alert("Info", "Successfully Saved User Profile Information!");
		//	this.props.navigation.navigate('MainStackNavigation')
		})
		.catch( error => {
			console.log(error)
			Alert.alert("Error", 'Failed To Save Profile Information, Please try Again!')
		})
	}
    
   
 

    render()
    {


  
      
        return(
          <SafeAreaView style={styles.container}>

    
    
            <ScrollView style={{backgroundColor:'#EBFAFF',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>


           
          <FlatList
          style={{marginBottom:10,width:'100%',padding:4}}
          data={this.state.users}
          ItemSeparatorComponent = { this.FlatListItemSeparator }
          showsVerticalScrollIndicator={false}
          renderItem={(item,index) => this.renderItem(item,index)}
        />
             
    
            </ScrollView>

    
            {this.state.loading_status &&
              <View style={[
                StyleSheet.absoluteFill,
                { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
              ]}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Discover);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });