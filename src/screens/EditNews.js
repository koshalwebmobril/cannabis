import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import AsyncStorage from '@react-native-community/async-storage';
import Snackbar from 'react-native-snackbar';
//import {  StackActions, NavigationActions} from 'react-navigation';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {  StackActions, NavigationActions} from 'react-navigation';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';




class EditNews extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      title:'',
      description:'',
      image:''
   
     
    };


  }


  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
       
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
           <TouchableOpacity onPress={()=>{navigation.goBack() }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Edit News</Text>
     </View>
    ),
  });

  isValid(){
    const { title, description , imageSource } = this.state;

      let valid = false;

        if (title.toString().trim().length > 0 && 
        description.toString().trim().length > 0 
        ) {
              valid = true;
             
          }

          if(title.toString().trim().length == 0){
      
            // Platform.OS === 'android' 
            //           ?   ToastAndroid.show('Enter Title', ToastAndroid.SHORT)
            //           : Alert.alert("Enter Title!")

            Snackbar.show({
              title: "Enter Title",
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });
            return false;
          }

          else  if(description.toString().trim().length == 0){
      
            Snackbar.show({
              title: "Enter News Text",
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });


            return false;
          }

        
        return valid

  }



  editNews(){
    if(this.isValid()){

     AsyncStorage.getItem("user_id").then((item) => {
       if (item) {
        let result = this.props.navigation.getParam('result')
         var formData = new FormData();
 
         formData.append('user_id',result['user_id']);
         formData.append('news_id',result['id']);
         formData.append('title',this.state.title);
         formData.append('description',this.state.description);
         
         
 
                       this.setState({loading_status:true})
                       let url = urls.base_url +'api/edit_news'
                       fetch(url, {
                       method: 'POST',
                       headers: {
                         'Accept': 'application/json',
                         'Content-Type': 'multipart/form-data',
                       },
                   body: formData
 
                     }).then((response) => response.json())
                           .then((responseJson) => {
                   this.setState({loading_status:false})
                  
             
          

              Snackbar.show({
                title: responseJson.message,
                duration: Snackbar.LENGTH_SHORT,
                backgroundColor:'black',
                color:'red',
                action: {
                  title: 'OK',
                  color: 'green',
                  onPress: () => { /* Do something. */ },
                },
              });

              
 
              
                         if(!responseJson.error){

                          this.props.navigation.navigate('News');
                          const resetAction = StackActions.reset({
                            index: 0,
                            actions: [NavigationActions.navigate({ routeName: 'News' })],
                          });
                          this.props.navigation.dispatch(resetAction);
                              
                              
                             }
                             else{
 
                              
                             }
                           }).catch((error) => {
                             this.setState({loading_status:false})
                             
                      //        Platform.OS === 'android' 
                      //  ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                      //  : Alert.alert("Try Again !")

                      Snackbar.show({
                        title: "Try Again !",
                        duration: Snackbar.LENGTH_SHORT,
                        backgroundColor:'black',
                        color:'red',
                        action: {
                          title: 'OK',
                          color: 'green',
                          onPress: () => { /* Do something. */ },
                        },
                      });
 
 
                           });
       }
       else{
         ToastAndroid.show("User not found !",ToastAndroid.LONG)
       }
     });
    }
  }


  



  componentWillMount(){
    let result = this.props.navigation.getParam('result')
  
    console.log(JSON.stringify(result))
    this.setState({title:result['title'],
                    description:result['description'],
                    image:result['img'],
                })

  
  }


    render(){

  
     
        return(
          <ImageBackground
          source={require('../assets/news_bg.jpg')}
          resizeMode="cover"
          style={styles.imagebackground}>

          <SafeAreaView style={styles.container}>
    
      

        <ScrollView style={{width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

       <View style={styles.outsideview}>

       <View style={{
        width:Dimensions.get('window').width * 0.8,
        height:Dimensions.get('window').height * 0.29,
        alignSelf:'center',marginTop:10
       
      }}>
      <Image source={{uri:urls.base_url + this.state.image}}
     resizeMode="stretch"
      style={{width:'100%',height:'100%'}}>
      </Image>
      </View>


             <TextField
              label='Title'
              value={this.state.title}
              tintColor={colors.color_primary}
              baseColor='white'
              textColor='white'
              onChangeText={(title) => this.setState({ title})}
              ref={(input) => { this.title = input; }}
              onSubmitEditing = {() => this.description.focus()}
            />

         <TextField
              label='Add Text'
              value={this.state.description}
               multiline={true}
              tintColor={colors.color_primary}
              baseColor='white'
              textColor='white'
              onChangeText={(description) => this.setState({ description})}
              ref={(input) => { this.description = input; }}
              onSubmitEditing = {() => this.description.focus()}
            />

            </View>

           
          
            <TouchableOpacity style={{width:'100%'}} onPress={() => {this.editNews()}}>
            <View style={{
              width: '90%', height: 50, backgroundColor: colors.color_primary, justifyContent: 'center',
              alignItems: 'center', alignSelf: 'center', marginTop: 30, borderRadius: 5,
              shadowColor: 'gray',
              shadowRadius: 10,
              shadowOpacity: 1,
              elevation:10

            }}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Save</Text>
            </View>
          </TouchableOpacity>
          <View style={{width:100,height:100}}></View>


        </ScrollView>

    

          
              
    
            {this.state.loading_status &&
              <View style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
          ]}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>
          </ImageBackground>
         

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(EditNews);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    imagebackground:{
    position: 'absolute',
    flex: 1,
    backgroundColor:'rgba(0,0,0,0.45)',
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,zIndex:-2
  },

  outsideview:{ 
    width: '90%', 
    justifyContent: 'center',
     alignSelf: 'center' ,
     marginBottom:30},

   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });