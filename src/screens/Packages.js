import React from 'react';
import { colors, urls } from '../Globals';
import { Slider,Overlay } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser,investorUser,entrepreneurUser } from '../actions/actions';
import FullView from './FullView';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  TouchableWithoutFeedback,
  Modal
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';

import stripe from 'tipsi-stripe'

stripe.setOptions({
  publishableKey: 'pk_test_7BfmJjAx9dBFO2eGC4GGd1za',
 //sandox:pk_test_lH3nLCADTo6rIyxvns0P8igB008LHXo6qB
 //live : pk_live_j40fxUTzTRVqL0D8SWIIvdBy00NU75hBT2

androidPayMode: 'test',
//androidPayMode:'production'
})

class Packages extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
     packages:[ ],
     modalVisible: false,
     
    };


  }


  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
       
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
           <TouchableOpacity onPress={()=>{navigation.goBack() }}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  {
                   navigation.getParam('result')['upgrade_id'] == 2
                 ? <Text style={{marginLeft:20,color:'white',fontSize:16}}>Investor Seeking Opportunity</Text>
                  : <Text style={{marginLeft:20,color:'white',fontSize:16}}>Entrepreneur Seeking Investment</Text>
                  }

                  
     </View>
    ),
  });

  
  getPackages(id){
    this.setState({loading_status:true})

    var formData = new FormData();
  
    formData.append('upgrade_id',id);


                      let url = urls.base_url +'api/packages'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
               
                      body:formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                                
                               
                                     this.setState({packages:responseJson.data})

                            }
                            else{

                             
                                Platform.OS === 'android' 
                                ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                : Alert.alert(responseJson.message)
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Platform.OS === 'android' 
                      ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                      : Alert.alert("Try Again !")


                          });
  }


  updateInvestor =  () => {
  
    this.props.change({ investor : 1});
   }


   updateEntrepreneur =  () => {
  
    this.props.change({ entrepreneur : 1});
   }

  stripeApi(token_id,obj){


    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {

    



        var formData = new FormData();
        
          formData.append('user_id',item);
          formData.append('stripe_token',token_id);
          formData.append('amt',parseFloat(obj.price))
          formData.append('period',obj.period)
          formData.append('subscription_type',obj.upgrade_id)
          formData.append('package_id',obj.id)
        
       // console.log(JSON.stringify(formData))

        //ToastAndroid.show(JSON.stringify(formData), ToastAndroid.SHORT);
          this.setState({loading_status:true})


          let url = urls.base_url +'api/make_payment'
        fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type':  'multipart/form-data',
        },
        body: formData

      }).then((response) => response.json())
            .then((responseJson) => {
              this.setState({loading_status:false})
              console.log(JSON.stringify(responseJson))
           //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);

           Snackbar.show({
            title: responseJson.message,
            duration: Snackbar.LENGTH_SHORT,
            backgroundColor:'black',
            color:'red',
            action: {
              title: 'OK',
              color: 'green',
              onPress: () => { /* Do something. */ },
            },
          });


            if(!responseJson.error){
                  //success in inserting data

                  let a = this.props.navigation.getParam('result', null)
                  let id = a['upgrade_id']

                  if(id == 2){
                    //means investor 
                         
                    this.setState({modalVisible:true})
                            
                            //this.props.investorUser()
                          // ToastAndroid.show("sdfsdf",ToastAndroid.LONG)

                          try {
                          AsyncStorage.setItem('investor', '1');
                          //ToastAndroid.show("done",ToastAndroid.LONG)
                          } catch (error) {
                            // Error saving data
                            //ToastAndroid.show(error.message,ToastAndroid.LONG)
                          }
                            //add investor to redux
                            this.updateInvestor()
                  }
                  else if (id == 3){
                    //enterpreneur
                    
                          AsyncStorage.setItem('entrepreneur', '1');
                          this.updateEntrepreneur()

                    this.props.navigation.navigate("AddPitch",{route_name:'Upgrades'}) 

                  }

                 

          }else{
            }


            }).catch((error) => {
             // ToastAndroid.show("Connection Error !", ToastAndroid.SHORT);
             
                      Snackbar.show({
                        title: 'Try Again',
                        duration: Snackbar.LENGTH_SHORT,
                        backgroundColor:'black',
                        color:'red',
                        action: {
                          title: 'OK',
                          color: 'green',
                          onPress: () => { /* Do something. */ },
                        },
                      });

            });
      }
      else{
        //ToastAndroid.show("User not found !",ToastAndroid.LONG)
        // Platform.OS === 'android'
        // ?  ToastAndroid.show("User not found !", ToastAndroid.SHORT)
        // : Alert.alert("User not found !")
      }
    });

    //  ToastAndroid.show(price+"...."+bid+"..."+pid+"...", ToastAndroid.SHORT);

    }

  async stripe(item){

    //console.log(JSON.stringify(item))
    const options = {
      requiredBillingAddressFields: 'full',
      prefilledInformation: {
        email:this.props.user.email,
        billingAddress: {
          name: this.props.user.name,
          line1: '',
          line2: '',
          city: '',
          state: '',
          country: '',
          postalCode: '',
        },
      },
    }

    const token = await stripe.paymentRequestWithCardForm(options)
  // ToastAndroid.show(JSON.stringify(token.tokenId),ToastAndroid.LONG)
    var cardId = token.card.cardId
    var tokenId = token.tokenId
    this.stripeApi(tokenId,item)
    console.log("carddd",JSON.stringify(token))
  }




  upgradeNow(){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
  
       
  
        var formData = new FormData();
  
        formData.append('user_id',item);
        formData.append('upgrade_id',1);
        
  
                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/upgrade_as_seller'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData
  
                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
  
             Snackbar.show({
              title: responseJson.message,
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });
  
  
                              if(!responseJson.error){
                               // this.setState({news:p})
                              
                            }
                            else{
                              //this.getNews()
                             
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                      //       Platform.OS === 'android' 
                      // ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                      // : Alert.alert("Try Again !")
  
                      Snackbar.show({
                        title: 'Try Again',
                        duration: Snackbar.LENGTH_SHORT,
                        backgroundColor:'black',
                        color:'red',
                        action: {
                          title: 'OK',
                          color: 'green',
                          onPress: () => { /* Do something. */ },
                        },
                      });
  
  
                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }






  componentWillMount(){
    let a = this.props.navigation.getParam('result', null)
    let id = a['upgrade_id']
      this.getPackages(id)
  
  }

  _checkPeriod(item){
    
    if(item.period == 1){
      return 'One Time'
    }
    else if(item.period == 2){
      return 'Unlimited Access'
    }
    else if(item.period == 3){
      return '12 Months'
    }

  }

renderItem = ({item, index}) => {
    return(
    
     

          <View style={{width:'70%',height:null,
          borderWidth:0.6,borderColor:'grey',borderRadius:15,
          margin:35,justifyContent:'center',alignItems:'center',alignSelf:'center',shadowOpacity:0.3,
          backgroundColor:'#F8F8FF',elevation:5}}>

            <View style={{backgroundColor:'blue',padding:6,borderRadius:35,
            height:70,width:70,alignItems:'center',justifyContent:'center',marginTop:-35,elevation:6,shadowOpacity:0.3}}>
            <Text style={{fontSize:14,fontWeight:'bold',color:'white'}}>${item.price}</Text>

            </View>

            <Text style={{margin:8,fontWeight:'bold',fontSize:16}}>{this._checkPeriod(item)}</Text>


            <TouchableOpacity onPress={()=> this.stripe(item)}>
            <View style={{backgroundColor:'blue',paddingTop:6,
            paddingBottom:6,
            paddingLeft:12,
            paddingRight:12,
            height:null,width:null,alignItems:'center',justifyContent:'center',margin:8,borderRadius:6}}>
            <Text style={{fontSize:14,color:'white'}}>Buy Now</Text>

            </View>
            </TouchableOpacity>

          
          
            


            </View>
         
  
    )
   }


  



   
 

    render() {



      const EnterpreneurModal = ()=>{
        return(
         
          <Overlay
          isVisible={this.state.modalVisible}
          windowBackgroundColor="rgba(0, 0, 0, .5)"
          overlayBackgroundColor="white"
          width={300}
          height='auto'
          
        >
 
          <View style={{alignItems:'center',justifyContent:'center'}}>
                <View style={{padding:15,borderRadius:30,
                borderColor:'grey',borderWidth:1,marginTop:-35,
                backgroundColor:'white',elevation:4,shadowOpacity:0.5,marginBottom:15}}>
                <Image source={require('../assets/investor.png')} 
                  style={{width:30,height:30}} resizeMode='contain'/>
                
                </View>

                <Text style={{textAlign:'center',fontWeight:'bold',margin:6}}>Welcome to the Investment Hub of Cannabis Expressions.</Text>
                <Text style={{textAlign:'center',color:'grey'}}>There is a world of opportunities here for you.</Text>
                <Text style={{textAlign:'center',color:'grey'}}>
                The next million dollar idea could be right here waiting for you to take it forward. 
                Explore now ! </Text>


                <TouchableOpacity onPress={()=> {
                  this.setState({modalVisible:false})
                  this.props.navigation.navigate("HomePage")}
                  }
                style={{backgroundColor:colors.color_primary,
                paddingLeft:20,
                paddingRight:20,
                paddingTop:6,
                paddingBottom:6,
                alignItems:'center',justifyContent:'center',margin:10}}>
                    <Text style={{color:'white',fontWeight:'bold'}}>OK</Text>

                </TouchableOpacity>
          </View>
        </Overlay>
        )
      }
  
      
        return(
          <SafeAreaView style={styles.container}>

          <EnterpreneurModal/>

    
            <ScrollView style={{width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

           {/* content */}

           <View style={{backgroundColor:'#BAD5FF',padding:20,width:'100%'}}>
            {
              this.props.navigation.getParam('result')['upgrade_id'] == 2
              ?
              <Text style={{textAlign:'center',margin:10}}
              >This allows you as an investor to register with 
              our investment hub to seek out the next big thing !</Text>
              :
              <Text style={{textAlign:'center',margin:10}}>List your idea for investment to investors around the world! 
              You can have the next big thing waiting for the right person to invest! 
              </Text>

            }
          

           <Text style={{textAlign:'center',margin:10,fontWeight:'bold',fontSize:17}}
           >Pay one time only access fee.</Text>

           </View>


        {/* content end */}


        <FlatList
          style={{marginBottom:10,width:'100%',padding:4,marginTop:30}}
          data={this.state.packages}
         // ItemSeparatorComponent = { this.FlatListItemSeparator }
          showsVerticalScrollIndicator={false}
          renderItem={(item,index) => this.renderItem(item,index)}
        />
             



         
             
    
            </ScrollView>

    
            {this.state.loading_status &&
              <View pointerEvents="none" style={styles.loading}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
    investorUser: () => dispatch(investorUser()),
    entrepreneurUser: () => dispatch(entrepreneurUser()),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Packages);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });