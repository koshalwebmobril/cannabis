import React from 'react';
import { colors, urls } from '../Globals';
import { Button,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import AsyncStorage from '@react-native-community/async-storage';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,TextInput,PermissionsAndroid,TouchableWithoutFeedback,Linking
} from 'react-native';
import { connect } from 'react-redux';
import Snackbar from 'react-native-snackbar';
import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
//import AddAdvertisements from './AddAdvertisements';



class Advertisements extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
        advertisements:[]
     
    };


  }


  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:10}}>
       
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:10}}>
                  <TouchableOpacity onPress={()=>{ navigation.navigate("HomePage") }} style={{}}>
                  <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <Text style={{marginLeft:20,color:'white',fontSize:16}}>Sponsored Content</Text>
            </View>
    ),
  });


  
  getAdvertisements(){
                  var formData = new FormData();
                
                   formData.append('user_id',this.props.user.user_id);
                   this.setState({loading_status:true})

                 
                      let url = urls.base_url +'api/get_adverts'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                      body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                        this.setState({loading_status:false})


                //  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                                
                               
                                     this.setState({advertisements:responseJson.data})

                            }
                            else{

                             
                               
                              Snackbar.show({
                                title: responseJson.message,
                                duration: Snackbar.LENGTH_SHORT,
                                backgroundColor:'black',
                                color:'red',
                                action: {
                                  title: 'OK',
                                  color: 'green',
                                  onPress: () => { /* Do something. */ },
                                },
                              });

                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Snackbar.show({
                              title: 'Try Again !',
                              duration: Snackbar.LENGTH_SHORT,
                              backgroundColor:'black',
                              color:'red',
                              action: {
                                title: 'OK',
                                color: 'green',
                                onPress: () => { /* Do something. */ },
                              },
                            });


                          });
  }




  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator

    
    return !!pattern.test(str);
  }


  componentWillMount(){
        this.getAdvertisements()
  
  }

  next(user_id){
   
      let obj={
        'user_id':user_id
      }
       // this.props.navigation.navigate("SellerProfile",{result:obj})
    
    
   
 }

 seeURL = (url) => {
   if(url.includes('https://')){

   }
   else  if(url.includes('http://')){
     
  }
  else{
    url ='http://'+ url
  }
  
  let f = Platform.select({
       ios: () => {
           Linking.openURL(url);
       },
       android: () => {
           Linking.openURL(url).catch(err => console.error('An error occurred', err));;
       }
   });

   f();
}

nextPage(item){
  // console.log('sdfsdfsdfsdfsdfsdfsdfsd',item.media_type)
   if(item.media_type == 1){
     //image 
     let obj={
       'image_url':urls.base_url + item.img,
       'item': item,
     }
     this.props.navigation.navigate("ImageView",{result:obj})
   }
   else if(item.media_type == 2){
     //image 
     let obj={
       'video_url':item.video,
       'item': item,
     }
     this.props.navigation.navigate("VideoView",{result:obj})
   }
 }


renderItem = ({item, index}) => {
    return(
      <TouchableWithoutFeedback onPress={()=>{this.nextPage(item)}}>
      <View style={{width:'100%',padding:6,marginBottom:5,marginTop:5,borderColor:'#999',borderWidth:1}}>

        <View style={{width:'100%',height:null}}>

        {
            item.media_type == '2'
              ?
              <Video
               video={{uri:urls.base_url+ item.video}}
               resizeMode='stretch'
              thumbnail={{uri: urls.base_url+ item.video_thumbnail}}
              style={{width:'104%',height:120,alignSelf:'center',marginTop:-7,}} />
              :


              <Image source={{uri:urls.base_url+ item.img}}
               resizeMode='cover'
              style={{alignSelf:'center',height:120,width:'104%',marginTop:-7,
              overflow:'hidden',flex:1}}  />


            }

            <Text style={{fontSize:14,fontWeight:'800'}}>{item.name}</Text>
            <Text style={{fontSize:12,color:'grey'}}>{item.description}</Text>
            {
              item.url && this.validURL(item.url)
              ?  <Text  onPress={()=> this.seeURL(item.url)}
              style={{fontSize:12,color:'blue'}}>{item.url}</Text>
              : null

            }
            

                
    
       </View>

     </View>
     
      </TouchableWithoutFeedback>
    )
   }


  



   FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.1,
          width: "101%",
          backgroundColor: "#aaa9ad",
          margin:3
        }}
      />
    );
  }

   s(){
    new Promise((resolve, reject) => {
			this.props.change({ user_id : 4, name : 'dssdsdfdsf', image:'image', email:'email' })
		})
		.then( success => {
			Alert.alert("Info", "Successfully Saved User Profile Information!");
		//	this.props.navigation.navigate('MainStackNavigation')
		})
		.catch( error => {
		//	console.log(error)
			Alert.alert("Error", 'Failed To Save Profile Information, Please try Again!')
		})
	}
    
   
 

    render()
    {
        return(
          <SafeAreaView style={styles.container}>

    
    
            <ScrollView style={{backgroundColor:'#EBFAFF',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>


           
          <FlatList
          style={{marginBottom:10,width:'100%',padding:4}}
          data={this.state.advertisements}
          ItemSeparatorComponent = { this.FlatListItemSeparator }
          showsVerticalScrollIndicator={false}
          renderItem={(item,index) => this.renderItem(item,index)}
        />
             
            </ScrollView>

    
            {this.state.loading_status &&
              <View style={[
                StyleSheet.absoluteFill,
                { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
              ]}>
              <WaveIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Advertisements);



const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });
