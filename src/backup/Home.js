import React from 'react';
import { colors, urls } from '../Globals';
import {Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import FullView from './FullView';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';
import firebaseSDK from '../config/firebaseSDK';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform,
  AppState
} from 'react-native';
import { DrawerActions } from 'react-navigation';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator
} from 'react-native-indicators';
import Snackbar from 'react-native-snackbar';
import PostComponent from '../components/PostComponent';
import CustomHeader from '../components/CustomHeader';
import {getFCMToken} from '../config/DeviceTokenUtils';
import Geolocation from '@react-native-community/geolocation';


import { fetchPosts } from '../api/apis.js';
import {showMessage} from '../config/snackmsg';

class Home extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      post_text:'',
      //only for text 
      post_text_error:false,
      //for post eroor full
      post_error:false,
      //no image or video error
      image_video_error:false,
      photo:null,
      imageSource:null,
      videoSource:null,
      //dummy is just temp vidoe or image
      imageSourceDummy:null,
      videoSourceDummy:null,
      video:null,
      refreshing:false,
      smiley:false,
      posts:[ ],
      latitude: 0.00,
      longitude: 0.00,
      deviceToken:''
     
    };


  }

  static navigationOptions  = ({ navigation }) => ({
    headerTitleStyle: { alignSelf: 'flex-start' },
    //he back button and title both use this property as their color.
    headerTintColor: 'royalblue',
    headerStyle: {
      backgroundColor: colors.color_primary,
    
          shadowOffset: {
      width: 0,
      height: 3,
  },
  shadowOpacity: 0.5,//for ios
  shadowRadius: 6,
  elevation:10,//for android
    },
   // headerTitle: () => <Text>dssd</Text>,
  //title:"SSSS",
    headerRight: () => (

      <View style={{flexDirection:'row',alignItems:'center',marginRight:7}}>
        {/*  onPress={()=>{ this.props.navigation.navigate("Discover") }}*/}
          <TouchableOpacity onPress={()=>{ navigation.navigate("Discover") }}>
          <Image source={require('../assets/user.png')} style={{width:25,height:25}} resizeMode='contain'/>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=>{ navigation.navigate("Requests") }}>
          <Image source={require('../assets/notification.png')} style={{width:25,height:25,marginLeft:15,marginRight:5}} 
          resizeMode='contain'/>
          {
            navigation.getParam('count') > 0
            ?
            <View style={{position:'absolute',top:-10,height:null,width:null,backgroundColor:'red',left:30,
          borderRadius:15,padding:4}}>
            <Text style={{color:'white'}}>{navigation.getParam('count')}</Text>

          </View>

          : null

            
          }
          


          </TouchableOpacity>

          <TouchableOpacity onPress={()=>{ navigation.navigate("Chats")}}>
          <Image source={require('../assets/chat.png')} style={{width:25,height:25,marginLeft:15}} resizeMode='contain'/>
          </TouchableOpacity>
    
    </View>
    ),
    headerLeft: () => (
      <View style={{flexDirection:'row',alignItems:'center',marginLeft:7}}>
      <TouchableOpacity onPress={()=>{ navigation.toggleDrawer()}}>
      <Image source={require('../assets/menu.png')} style={{width:25,height:25}} resizeMode='contain'/>
      </TouchableOpacity>

      <Text style={{marginLeft:20,color:'white',fontSize:17}}>Feeds</Text>
    
     </View>
    ),
  });

  

// static navigationOptions = ({ navigation }) => {
//     //return header with Custom View which will replace the original header 
//     return {
//       header: props => <CustomHeader {...props} />
//       //header: () => <CustomHeader/>,
//     };
//   };
_handleAppStateChange = (nextAppState) => {
  if (
    //this.state.appState.match(/inactive|background/) &&
    nextAppState === 'active'
  ) {
    //console.log('App has come to the foreground!');
    this.changeStatus(1)
  }
  else{
   // console.log('App has come to the background!');
    this.changeStatus(0)
  }
  //this.setState({appState: nextAppState});
};


async  requestLocationPermission(){
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        'title': 'Cannabis App',
        'message': 'Cannabis App access to your location '
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {

      
      Geolocation.getCurrentPosition(
        position => {
          //ToastAndroid.show(JSON.stringify(position), ToastAndroid.SHORT)
          this.setState({
            
           latitude: position.coords.latitude,
           longitude: position.coords.longitude,
          

          });
          
        },
        error => {
        //  ToastAndroid.show("Check your internet speed connection",ToastAndroid.SHORT);

          Platform.OS === 'android' 
          ? ToastAndroid.show("Check your internet speed connection", ToastAndroid.SHORT)
          : Alert.alert("Check your internet speed connection")


          //this.setState({loading_status:false})
        },
        { enableHighAccuracy: false, timeout: 10000}
      );


    } else {
     // console.log("location permission denied")
     Alert.alert("location permission denied");
      //alert("Location permission denied");
    }
  } catch (err) {
    //console.warn(err)
      Alert.alert(JSON.stringify(err.message));
  }
}

     
  async  requestLocationPermissionIOS(){



  
   
    Geolocation.getCurrentPosition(
        position => {
          this.setState({
           
           latitude: position.coords.latitude,
           longitude: position.coords.longitude,
          

          });
         
        },
        error => {
        //  ToastAndroid.show("Check your internet speed connection",ToastAndroid.SHORT);

          Platform.OS === 'android' 
          ? ToastAndroid.show("Check your internet speed connection", ToastAndroid.SHORT)
          : Alert.alert("Check your internet speed connection")


          this.setState({loading_status:false})
        },
        { enableHighAccuracy: false, timeout: 10000}
      );
}

componentWillUnmount() {
 AppState.removeEventListener('change', this._handleAppStateChange);
}

  componentDidMount(){

    StatusBar.setBackgroundColor(colors.status_bar_color)
    
  }



  componentWillMount = async () =>{

    this.props.navigation.setParams({count:0})

    //{navigation.getParam('count')}

    getFCMToken().then(response => {
        
      this.setState({
        deviceToken : response
      },()=>{
       // console.log("HOME FCM TOKEN----------",this.state.deviceToken)
         //save user data to database for firebase
     this.writeUserData(this.props.user.user_id,
      this.props.user.name,
      this.props.user.image,
      this.props.user.email,
      response)
      })
      //console.log("HOME FCM TOKEN----------",JSON.stringify(response))
  });



    Platform.OS === 'android' 
    ?  await this.requestLocationPermission()
    :  await this.requestLocationPermissionIOS()


    AppState.addEventListener('change', this._handleAppStateChange);


    // this.props.navigation.closeDrawer()
     this.getPosts()
    // firebaseSDK
 
     const {item} = this.props.user
 
 
    
   
     let a = JSON.stringify(this.props.user)
    // console.log("val-------of----",a)
     //console.log("val-----------",this.props.user.name)
 
 
 
 
     // console.log("val-----------",this.props.user[0]['user_image'])
     // console.log("val-----------",this.props.user[0]['user_name'])
     // console.log("val-----------",this.props.user[0]['email'])
   }
 

  
  isValid(){
    const { post_text, imageSource , videoSource } = this.state;

   // console.log(post_text)

      let valid = false;

        if (post_text.toString().trim().length > 0 && 
        (imageSource != null || videoSource != null)
        ) {

         // console.log("True")
            
              valid = true;
              //return true
          }

          if(imageSource != null && videoSource != null){
      
           // console.log("SADFDSF")
            this.ErroSheet.open()
            //open error sheet 
            return false;
          }

          else if (post_text.toString().trim().length === 0) {
  
          // this.setTimeout(this.setState({post_text_error:true}), 5000);
          // this.setState({post_error:true,post_text_error:true})
          this.setState({post_error:true,post_text_error:true})

           setTimeout(() => {

            this.setState({post_text_error:false,post_error:false})
           
      
          }, 2000);
        
      
            
            return false;
          }

          else  if(imageSource == null && videoSource == null){

            this.setState({post_error:true,image_video_error:true})
            //console.log("imagesssssssss")
            return false;
          }

        
        return valid

  }

  makePost(){
   if(this.isValid()){


    // console.log("Making posts")

    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
        var formData = new FormData();

        formData.append('user_id',item);
        formData.append('title','title');
        formData.append('description',this.state.post_text);
        {
          this.state.imageSource != null
          ?    formData.append('image',this.state.photo)
          : formData.append('video',this.state.video)
        }
        {
          this.state.imageSource != null
          ?    formData.append('media_type',1)
          : formData.append('media_type',2)
        }
        

                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/create_post'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                //  console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);

             
            //  Platform.OS === 'android' 
            //  ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
            //  : Alert.alert(responseJson.message)


             Snackbar.show({
              title: responseJson.message,
              duration: Snackbar.LENGTH_SHORT,
              backgroundColor:'black',
              color:'red',
              action: {
                title: 'OK',
                color: 'green',
                onPress: () => { /* Do something. */ },
              },
            });

             
                              if(!responseJson.error){

                                this.setState({imageSource:null,
                                post_text:'',
                                videoSource:null,
                               video:null,photo:null})

                            this.getPosts()
                             
                            }
                            else{

                             
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                         
                        Snackbar.show({
                          title: 'Try Again !',
                          duration: Snackbar.LENGTH_SHORT,
                          backgroundColor:'black',
                          color:'red',
                          action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                          },
                        });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
   }
  }

  likePosts(post_id,like_id){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
        var formData = new FormData();

        formData.append('user_id',item);
        formData.append('post_id',post_id);
        formData.append('like_type',like_id);


        // console.log('formData',formData)
        // console.log('like_type',like_id)

        

                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/like_post'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
         // console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){

                                Platform.OS === 'android' 
                                ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                : Alert.alert(responseJson.message)


                        // Snackbar.show({
                        //   title: responseJson.message,
                        //   duration: Snackbar.LENGTH_SHORT,
                        //   backgroundColor:'black',
                        //   color:'red',
                        //   action: {
                        //     title: 'OK',
                        //     color: 'green',
                        //     onPress: () => { /* Do something. */ },
                        //   },
                        // });





                                var p = this.state.posts
                                p.map(item=>{
                                 if(item.id == post_id){
                                //  if(like_id == 1){
                                //   Object.assign(item,{likes_count:item.likes_count+1,like_flag:})
                                //  }
                                //  else{
                                //   Object.assign(item,{like_flag:like_id})
                                //  }

                                let count = parseInt(item.likes_count) + 1
                              //  console.log("COUNT",count)

                                 Object.assign(item,{like_flag:like_id,likes_count:count})



                                 }
                                })

                                // console.log("PPPPPPPPPP",JSON.stringify(p))


                              //   let ids = [...this.state.posts];  
                              //   let index = ids.findIndex(el => {
                              //     el.id == post_id && like_id == 1
                              //  });
                              //    ids[index] = ;                  
                            

                               this.setState({posts:p},console.log("NEW DATA",JSON.stringify(this.state.posts)))
                            }
                            else{

                             
                               

                        Snackbar.show({
                          title: responseJson.message,
                          duration: Snackbar.LENGTH_SHORT,
                          backgroundColor:'black',
                          color:'red',
                          action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                          },
                        });
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                           
                        Snackbar.show({
                          title: 'Try Again !',
                          duration: Snackbar.LENGTH_SHORT,
                          backgroundColor:'black',
                          color:'red',
                          action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                          },
                        });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }

  getPost(){
    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
        var formData = new FormData();

        formData.append('user_id',item);
        formData.append('lat',this.state.latitude);
        formData.append('long',this.state.longitude);


        //console.log(JSON.stringify(formData))
        

                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/home_screen'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData

                    }).then((response) => response.json())
                          .then((responseJson) => {
                  this.setState({loading_status:false})
                  //console.log(JSON.stringify(responseJson))
             //    ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                              if(!responseJson.error){


                                var requestCount = responseJson.request_count
                                this.props.navigation.setParams({count:requestCount})

                                //report
                                var length = responseJson.posts.length.toString();
                                var temp_arr=[]
                             
                                //attaching a key to all objects 
                                var p = responseJson.posts
                                p.map(item=>{
                                  Object.assign(item,{smiley:false})
                                })

                              // console.log("PPPPPPPPPP",JSON.stringify(p))

                               this.setState({posts:p})

                            }
                            else{

                             
                                // Platform.OS === 'android' 
                                // ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                // : Alert.alert(responseJson.message)

                        Snackbar.show({
                          title: responseJson.message,
                          duration: Snackbar.LENGTH_SHORT,
                          backgroundColor:'black',
                          color:'red',
                          action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                          },
                        });
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                           
                        Snackbar.show({
                          title: 'Try Again !',
                          duration: Snackbar.LENGTH_SHORT,
                          backgroundColor:'black',
                          color:'red',
                          action: {
                            title: 'OK',
                            color: 'green',
                            onPress: () => { /* Do something. */ },
                          },
                        });


                          });
      }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
    });
  }


  getPosts(){

    AsyncStorage.getItem("user_id").then((item) => {
      if (item) {
        var formData = new FormData();

        formData.append('user_id',item);
        formData.append('lat',this.state.latitude);
        formData.append('long',this.state.longitude);

        let obj = {
          'user_id':item,
          'lat':this.state.latitude,
          'long':this.state.longitude
        }

        this.setState({loading_status:true})

        fetchPosts(obj).then((responseJson) => {
          //let results = JSON.stringify(data);
          this.setState({loading_status:false})

          if(!responseJson.error){


            var requestCount = responseJson.request_count
            this.props.navigation.setParams({count:requestCount})

            //report
            var length = responseJson.posts.length.toString();
            var temp_arr=[]
           
            //attaching a key to all objects 
            var p = responseJson.posts
            p.map(item=>{
              Object.assign(item,{smiley:false})
            })

          // console.log("PPPPPPPPPP",JSON.stringify(p))

           this.setState({posts:p})

        }
        else{

          showMessage(responseJson.message)

         
                }
            

        }).catch((error) => {
          this.setState({loading_status:false})
          
         
            showMessage('Try Again')


        });
           


        //console.log(JSON.stringify(formData))
        

                  
      
    }
      else{
        ToastAndroid.show("User not found !",ToastAndroid.LONG)
      }
  });

  }


  checkPermissionPhoto(){
    PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then(response => {
      if (response === true){
        this.addPhoto()
    }
      else if (response === false){
          Alert("Please enable camera permission in device settings.")
      }
    })
  }



 

  addVideo() {
    const options = {
      title: 'Video Picker', 
      mediaType: 'video', 
      videoQuality: 'medium',
      durationLimit: 30,
      takePhotoButtonTitle: 'Take Video...',
      allowsEditing: true,
     // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true
      }
    };
 
    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);
 
      if (response.didCancel) {
        //console.log('User cancelled photo picker');
       // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
      }
      else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);

       // ToastAndroid.show(JSON.stringify(response),ToastAndroid.LONG)
      }
      else if (response.customButton) {
       // console.log('User tapped custom button: ', response.customButton);
      }
      else {
       //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
        let source = { uri: response.uri};
      //  console.log("VIDEO>>>>",JSON.stringify(response))
        var video={
          name: 'name.mp4',
          uri: source.uri,
          type: 'video/mp4'
        }
       

        this.setState({
          videoSourceDummy: source ,
         video:video
 
        });

        this.VideoSheet.open();

   
      

       
      }
    });
 }


 addVideoCamera() {
  const options = {
    title: 'Video Picker', 
    mediaType: 'video', 
    videoQuality: 'medium',
    durationLimit: 30,
    takePhotoButtonTitle: 'Take Video...',
    allowsEditing: true,
   // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true
    }
  };


   //launchCamera
    //showImagePicker
    //launchImageLibrary
  ImagePicker.launchCamera(options, (response) => {
   // console.log('Response = ', response);

    if (response.didCancel) {
     // console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      //console.log('ImagePicker Error: ', response.error);

     // ToastAndroid.show(JSON.stringify(response),ToastAndroid.LONG)
    }
    else if (response.customButton) {
     // console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};
     // console.log("VIDEO>>>>",JSON.stringify(response))
      var video={
        name: 'name.mp4',
        uri: source.uri,
        type: 'video/mp4'
      }
     

      this.setState({
        videoSourceDummy: source ,
       video:video

      });

      this.VideoSheet.open();

 
    

     
    }
  });
}


addVideoGallery() {


  const options = {
    title: 'Video Picker', 
    mediaType: 'video', 
    videoQuality: 'medium',
    durationLimit: 30,
    takePhotoButtonTitle: 'Take Video...',
    allowsEditing: true,
   // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true
    }
  };

   //launchCamera
    //showImagePicker
    //launchImageLibrary

  ImagePicker.launchImageLibrary(options, (response) => {
   // console.log('Response = ', response);

    if (response.didCancel) {
      //console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      //console.log('ImagePicker Error: ', response.error);

     // ToastAndroid.show(JSON.stringify(response),ToastAndroid.LONG)
    }
    else if (response.customButton) {
     // console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};
      //console.log("VIDEO>>>>",JSON.stringify(response))
      var video={
        name: 'name.mp4',
        uri: source.uri,
        type: 'video/mp4'
      }
     

      this.setState({
        videoSourceDummy: source ,
       video:video

      });

      this.VideoSheet.open();

 
    

     
    }
  });
}



  addPhoto() {
    const options = {
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };
 
    //launchCamera
    //showImagePicker
    //launchImageLibrary
    ImagePicker.launchCamera(options, (response) => {
      //console.log('Response = ', response);
 
      if (response.didCancel) {
       // console.log('User cancelled photo picker');
       // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
      }
      else if (response.error) {
       // console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        //console.log('User tapped custom button: ', response.customButton);
      }
      else {
       //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
        let source = { uri: response.uri};

        var photo = {
          uri: response.uri,
          type:"image/jpeg",
          name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
        };
       

        this.setState({
          imageSourceDummy: source ,
          photo:photo,
 
        });
        this.PhotoSheet.open();

        {/*photo is a file object to send to parameters */}
      }
    });
 }

 //By just calling remove() from the reference of database will delete that record.
 // You can even use null with set() or update() to delete that record from the database

 deleteData(){
  firebase.database().ref('Users/').remove();
}


 //To update the data of any of the object, you need to create reference of it and use update() 
 //with the data that you want to change.
 updateSingleData(email){
  firebase.database().ref('Users/').update({
      email,
  });
}

//Read From Database

//It will read data once from the `Users` object and print it on console. 
//If we want to get data whenever there is any change in it, 
//we can use on instead of once
 readUserData() {
  firebase.database().ref('Users/').once('value', function (snapshot) {
     // console.log(snapshot.val())
  });
}

readUserDataOn() {
  firebase.database().ref('Users/').on('value', function (snapshot) {
      //console.log(snapshot.val())
  });
}

 //To push data in any of the object and create array with unique keys
 //push update nahi karta ek extra data add krta hai

 writeUserDataPush(user_id, name,image,email){
  firebase.database().ref('UsersList/' + user_id).push({
    user_id, 
    name,
    image,
    email 
  }).then((data)=>{
      //success callback
      //console.log('data written--- ' , data)
  }).catch((error)=>{
      //error callback
      console.log('error ' , error)
  })
}

changeStatus(status){
  //status == 0 ..offline
  //status == 1 .. online


  // firebase.database().ref('Users/'+ this.props.user.user_id).set({
  //   'active' : status == 1 ? 'online' :'offline',
  // }).then((data)=>{
  //     //success callback
  //     console.log('data written---' , JSON.stringify(data))
  // }).catch((error)=>{
  //     //error callback
  //     console.log('error ' , error)
  // })


  firebase.database().ref('Users/'+ this.props.user.user_id).update({
    'active' : status == 1 ? 'online' :'offline',
});



}
  
 //Writing To Database
 //set update krta hai
 writeUserData( user_id, name,image,email,deviceToken){
//console.log("FIREbase---",deviceToken)
  firebase.database().ref('Users/'+ user_id).set({
    user_id, 
    name,
    image,
    email ,
    device_token:deviceToken,
    'active':'online'
  }).then((data)=>{
      //success callback
      //console.log('data written---' , JSON.stringify(data))
  }).catch((error)=>{
      //error callback
   console.log('error ' , error)
  })

//Add a Completion Callback

//   firebase.database().ref('Users/'+ user_id).set({
//     user_id, 
//     name,
//     image,
//     email 
//   }, function(error) {
//     if (error) {
//       // The write failed...
//     } else {
//       // Data saved successfully!
//     }
//   });
// }
}


 

c(){
  let a = JSON.stringify(this.props.user)
   // console.log("val updateeeeeed-------of----",a)
}
  
  addKey(){
     this.props.enterpreneurUser()
     this.c()
  }


  imageVideoCheck(){
    const { imageSource , videoSource } = this.state
    
    if(imageSource == null && videoSource == null){
      return null
    }
    else if(imageSource != null && videoSource  == null){
      return(
       <View></View>
      )

    }
    else if(videoSource != null && imageSource  == null){
      return(
                      <View style={{height:null,width:"90%",backgroundColor:'red',padding:20}}>
                         
                      </View>
                      

       
      )

    }
  }

  next(item){
    let user_id = item.user_id
    let obj={
      'user_id':user_id,
      
    }
    this.props.navigation.navigate("SellerProfile",{result:obj})
  }

  //1-Like
// 2-Cool
// 3-Awesome
// 3-Fantastic

  renderItems = ({item, index}) => {
    return(
        <View style={{width: Dimensions.get('window').width* 0.95,
        height:null,backgroundColor:'white',
        margin:10,padding:0,borderWidth:0.5,elevation:3}}>

        <TouchableOpacity onPress={()=>{
          if( this.state.posts[index].smiley == true){
            this.state.posts[index].smiley = false
            this.setState({posts:  this.state.posts})
          }
        }}>
        {/* onPress={()=> this.next(item)} */}
        <TouchableOpacity onPress={()=> this.next(item)}>
        <View style={{flexDirection:'row',
        justifyContent:'flex-start',alignItems:'center'}}>


       
             <Image source={{uri:urls.base_url+item.user_pic}}
                style={{height:40,width:40,
                overflow:'hidden',borderRadius:20,margin:10}}  />

                <View style={{marginLeft:8}}>
                <Text style={{marginBottom:5,fontWeight:'bold'}}>{item.user_name}</Text>
                <View style={{flexDirection:'row',width:'90%',alignItems:'center'}}>
                      <Image source={require('../assets/clock.png')}
                                  style={{width:17,height:17,marginRight:6}} resizeMode='contain'/>

                      <Text style={{color:'grey',fontSize:13}}>{item.created_at}</Text>
                
                </View>
                </View>

               
        </View>
        </TouchableOpacity>
        
          
            <Text style={{margin:5
            }}>{item.description}</Text>


            <TouchableOpacity onPress={()=> {}}>
            <View style={[
                { width: '100%'},
             
                { height:Dimensions.get('window').height *0.25,
                  margin: 10,alignSelf:'center'
              }]} >

             
              {
                item.media_type == '2'
                ?
                <Video source={{uri:urls.base_url+item.post_video}} 
                disableBack
                disableFullscreen
                disableVolume
                disableSeekbar
                muted={true}
                autoplay={false}
                paused={false}
                onPause={()=> {}}
                onPlay={()=> {}}
                resizeMode='stretch'
                showOnStart={true}
                seekColor={colors.color_primary}
                style={{width:'93%',height:'100%',alignSelf:'center'}} />
                : 
                <Image source={{uri:urls.base_url+item.post_img}} resizeMode='cover'
                style={{alignSelf:'center',height:'100%',width:'90%',
                overflow:'hidden',flex:1}}  />
  
              }
           

              </View>
              </TouchableOpacity>

              <View style={{width:'100%',backgroundColor:'grey',height:0.5}}></View>

              
            <TouchableOpacity activeOpacity={0.6}
            style={{flexDirection:'row',alignItems:'center'}}
            onPress={()=> this.likePosts(this.state.posts[index].id,1)}
            onLongPress={()=> {
              this.state.posts[index].smiley = true
              this.setState({posts:  this.state.posts})
            }}>
            
               <Image source={require('../assets/p1.png')}
                style={{height:25,width:25,
                overflow:'hidden',borderRadius:12,margin:10}}  />

                <Text>{item.likes_count} likes</Text>

                </TouchableOpacity>



                {
                  item.smiley
                  ?
                  <View style={{position:'absolute',
                  bottom:10,left:10,
                  backgroundColor:'white',
                  width:null,flexDirection:'row',
                   alignItems:'center',borderRadius:10,
                  padding:5,borderWidth:1}}>
    

                  <TouchableOpacity onPress={()=> {
                    this.likePosts(this.state.posts[index].id,1)
                    this.state.posts[index].smiley = false
                    this.setState({posts:  this.state.posts})
                  }}>
                  <Image source={require('../assets/p1.png')}
                  style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                  </TouchableOpacity>
                
                  <TouchableOpacity onPress={()=> {
                    this.likePosts(this.state.posts[index].id,2)
                    this.state.posts[index].smiley = false
                    this.setState({posts:  this.state.posts})
                  }}>
                  <Image source={require('../assets/p2.png')}
                  style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={()=> {
                    this.likePosts(this.state.posts[index].id,3)
                    this.state.posts[index].smiley = false
                    this.setState({posts:  this.state.posts})
                  }}>
                  <Image source={require('../assets/p3.png')}
                  style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                  </TouchableOpacity>


                  <TouchableOpacity onPress={()=> {
                    this.likePosts(this.state.posts[index].id,4)
                    this.state.posts[index].smiley = false
                    this.setState({posts:  this.state.posts})
                  }}>
                  <Image source={require('../assets/p4.png')}
                  style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                  </TouchableOpacity>


                  </View>
                  : null
                }

                
             
                </TouchableOpacity>

            
      </View>
    )
   }



   handler= (post_id,like_id) =>{
    //  console.log("RECT ON POSTS",post_id)
   //console.log("LIKE ",like_id)
    this.likePosts(post_id,like_id)
   }

   setPosts= (post) =>{
    //console.log("SETTTTTTT",JSON.stringify(post))
     //this.setState({posts:posts})
     this.setState({posts:post})
   }

   seeImage= (item) =>{
    //console.log("IMAGEEEE")
    if(item.media_type == '1'){
      let obj ={
        'item':item
      }
      this.props.navigation.navigate("PostImageDetail",{result:obj})
    }
    else{
      let obj ={
        'item':item
      }
      this.props.navigation.navigate("PostImageDetail",{result:obj})
    }
  }


   renderItem = ({item, index}) => {
    return(
      <PostComponent  posts={this.state.posts} index={index}
        handler={this.handler} setPosts={this.setPosts}
        seeImage ={this.seeImage}
      />
    )
   }

   
 

    render()
    {


    const PhotoSheet= () =>{
     
      return (
        <RBSheet
        ref={ref => {
          this.PhotoSheet = ref;
        }}
        height={240}
        duration={0}
        customStyles={{
          container: {
            justifyContent: "center",
            alignItems: "center",
            borderTopLeftRadius:15,
            borderTopRightRadius:15
          },
          // wrapper:{
          //   backgroundColor:'grey'
          // }
        }}
        animationType='fade'
       
        minClosingHeight={10}

        >
          <View style={{width:'60%',height:'80%',
         padding:0,flex:3,flexDirection:'row',
         backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
          
          
          <Image source={this.state.imageSourceDummy}
                                  style={{height:'90%',width:'60%',
                                 }}  />
          
          </View>

          <Text style={{margin:10}}>Do you want to add this image to add to post ?</Text>

          <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',
          flex:2}}>

              <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
              <TouchableOpacity onPress={()=>{
                this.setState({imageSource:this.state.imageSourceDummy})
              }}
              style={{width:'100%',backgroundColor:colors.color_primary,padding:15,
                justifyContent:'center',alignItems:'center',elevation:10}}>
              <Text style={{color:'white'}}>Yes</Text>
              </TouchableOpacity>
              </View>


              <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
              <TouchableOpacity onPress={()=> {this.PhotoSheet.close()}}
              style={{width:'100%',backgroundColor:'white',padding:15,
                justifyContent:'center',alignItems:'center',elevation:10}}>
              <Text style={{color:colors.color_primary}}>No</Text>
              </TouchableOpacity>
              </View>
          
          </View>

    </RBSheet>
      )
    }

    const VideoSheet= () =>{
     
      return (
        <RBSheet
        ref={ref => {
          this.VideoSheet = ref;
        }}
        height={250}
        duration={0}
        customStyles={{
          container: {
            justifyContent: "center",
            alignItems: "center",
            borderTopLeftRadius:15,
            borderTopRightRadius:15
          },
          // wrapper:{
          //   backgroundColor:'grey'
          // }
        }}
        animationType='fade'
       
        minClosingHeight={10}

        >
          <View style={{width:Dimensions.get('window').width * 0.7,
          height:'60%',
          backgroundColor:'white',padding:10,
          flexDirection:'row',backgroundColor:'white'}}>
          
          
          <Video source={this.state.videoSourceDummy}
              disableBack
                disableFullscreen
                disableVolume
                disableSeekbar
                muted={true}
                autoplay={false}
                paused={false}
                onPause={()=> {}}
                onPlay={()=> {}}
               
                showOnStart={false}
                seekColor={colors.color_primary}
        
                                  style={{height:'100%',width:'90%'}}  />
          
          </View>


          <Text style={{margin:10}}>Do you want to add this video to add to post ?</Text>

          <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',
          flex:2,marginBottom:10}}>

              <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
              <TouchableOpacity onPress={()=>{this.setState({videoSource:this.state.videoSourceDummy})}}
              style={{width:'100%',backgroundColor:colors.color_primary,padding:15,
                justifyContent:'center',alignItems:'center',elevation:10}}>
              <Text style={{color:'white'}}>Yes</Text>
              </TouchableOpacity>
              </View>


              <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
              <TouchableOpacity onPress={()=> {this.VideoSheet.close()}}
              style={{width:'100%',backgroundColor:'white',padding:15,
                justifyContent:'center',alignItems:'center',elevation:10}}>
              <Text style={{color:colors.color_primary}}>No</Text>
              </TouchableOpacity>
              </View>
          
          </View>




    </RBSheet>
      )
    }

    const ErroSheet= () =>{
     
      return (
        <RBSheet
        ref={ref => {
          this.ErroSheet = ref;
        }}
        height={180}
        duration={0}
        customStyles={{
          container: {
            justifyContent: "center",
            alignItems: "center",
            borderTopLeftRadius:15,
            borderTopRightRadius:15
          },
          // wrapper:{
          //   backgroundColor:'grey'
          // }
        }}
        animationType='fade'
       
        minClosingHeight={10}

        >
        <View style={{width:Dimensions.get('window').width * 0.94,
        height:'20%',
        backgroundColor:'white',padding:10,
        flexDirection:'column',backgroundColor:'white'}}>
        
       <Text>Looks like you have added both image and video in your post.You can post with only one image or video. </Text>
       
    
        
        </View>

        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',
        flex:2,marginBottom:10}}>

            <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
            <TouchableOpacity onPress={()=>{ this.setState({imageSource:null,imageSourceDummy:null})}}
            style={{width:'100%',backgroundColor:colors.color_primary,padding:15,
              justifyContent:'center',alignItems:'center',elevation:10}}>
            <Text style={{color:'white'}}>Remove Image</Text>
            </TouchableOpacity>
            </View>


            <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
            <TouchableOpacity onPress={()=> {this.setState({videoSource:null,videoSourceDummy:null})}}
            style={{width:'100%',backgroundColor:'white',padding:15,
              justifyContent:'center',alignItems:'center',elevation:10}}>
            <Text style={{color:colors.color_primary}}>Remove Video</Text>
            </TouchableOpacity>
            </View>
        
        </View>



    </RBSheet>
      )
    }


    const VideoSheetChoose = () =>{
     
      return (
        <RBSheet
        ref={ref => {
          this.VideoSheetChoose = ref;
        }}
        height={200}
        duration={0}
        customStyles={{
          container: {
            justifyContent: "center",
            alignItems: "center",
            borderTopLeftRadius:15,
            borderTopRightRadius:15
          },
          // wrapper:{
          //   backgroundColor:'grey'
          // }
        }}
        animationType='fade'
        closeOnDragDown={true}
        minClosingHeight={10}

>
          <View style={{width:'80%',height:'80%',backgroundColor:'white',padding:10}}>

              <View style={{flexDirection:'row',justifyContent:'space-around',alignItems:'center',marginBottom:20}}>
                    <Image style={{ height: 40, width: 40}}
                    resizeMode='contain'
                    source={require('../assets/logo.png')}>
                    </Image>

                    <Text style={{fontWeight:'bold',fontSize:16}}>Choose Video Source ! </Text>
              
              </View>


              <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',
            flex:2}}>

                <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                <TouchableOpacity onPress={()=> this.addVideoCamera()}
                style={{width:'100%',backgroundColor:colors.color_primary,padding:20,
                  justifyContent:'center',alignItems:'center',elevation:10}}>
                <Text style={{color:'white'}}>Camera</Text>
                </TouchableOpacity>
                </View>


                <View style={{flex:1,margin:10,elevation:10,shadowColor: '#000000',
                            shadowOffset: {
                              width: 0,
                              height: 3
                            },
                          shadowRadius: 10,
                          shadowOpacity: 0.6}}>
                <TouchableOpacity onPress={()=> this.addVideoGallery()}
                style={{width:'100%',backgroundColor:'white',padding:20,
                  justifyContent:'center',alignItems:'center',elevation:10}}>
                <Text style={{color:colors.color_primary}}>Gallery</Text>
                </TouchableOpacity>
                </View>
            
            </View>
          </View>
</RBSheet>

      )
    }
  
      
        return(
          <SafeAreaView style={styles.container} forceInset={{ top: 'never' }}>
          

          <PhotoSheet></PhotoSheet>
          <VideoSheet></VideoSheet>
          <ErroSheet></ErroSheet>
          <VideoSheetChoose/>

          {/* 


      */}



    
    
            <ScrollView style={{backgroundColor:'white',width:'100%',flex:1}}
            showsVerticalScrollIndicator={false}
             refreshControl={
            <RefreshControl style={{backgroundColor: 'transparent'}} 
            refreshing={this.state.loading_status} onRefresh={()=> this.getPosts()} />
          }
           contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>

           {/* top item */}
           <View style={{backgroundColor:'white',
            height:Dimensions.get('window').height * 0.13,width:'100%',flexDirection:'row',
           alignItems:'center',padding:10,flex:2,marginBottom:0}}>

           <View style={{flex:1.2,height:'100%',flexDirection:'row',alignItems:'center'}}>

                  <View style={{flex:0.3,height:'100%',alignItems:'center',justifyContent:'center'}}>


                  <Image style={{ height: 50, width: 50,borderRadius:65,overflow:'hidden'}}
                  source={{uri:urls.base_url + this.props.user.image}}
                  ></Image>
                  </View>

                  <View style={{flex:0.9,height:'100%'}}>
                  <TextInput
                  style={{margin:10,width:'85%',height:'75%',borderColor:'grey',
                  borderWidth:1,borderRadius:10,padding:10,textAlignVertical:'top'}}
                  multiline={true}
                  placeholder={'Write Something here ..'}
                  value={this.state.post_text}
                  onChangeText={(post_text) => this.setState({ post_text })}>
                  
                  </TextInput>
                  </View>
           </View>

           <View style={{flex:0.8,height:'100%',flexDirection:'row',alignItems:'center'}}>

           <View style={{flex:0.7,height:'100%',justifyContent:'center',alignItems:'center'}}>
          
           <TouchableOpacity  onPress={()=> this.addPhoto()}>
           <Image 
          
          source={require('../assets/photo.png')} style={{width:20,height:20}} resizeMode='contain'/>
           </TouchableOpacity>

           <Text style={{fontSize:12}}>Photo</Text>
          
           </View>

           <View style={{flex:0.7,height:'100%',justifyContent:'center',alignItems:'center'}}>
           <TouchableOpacity  onPress={()=> this.VideoSheetChoose.open()}>
           <Image source={require('../assets/video.png')}
            style={{width:20,height:20}} resizeMode='contain'/>
            </TouchableOpacity>
           <Text style={{fontSize:12}}>Video</Text>
          
           </View>


           <View style={{flex:1,height:'100%',justifyContent:'center',alignItems:'center'}}>
           <TouchableOpacity onPress={()=> this.makePost()}>
           <View style={{marginLeft:5,backgroundColor:'blue',height:35,justifyContent:'center',
           alignItems:'center',padding:8,borderRadius:5}}>
           <Text style={{color:'white',fontSize:12}}>Post</Text>
           </View>
           </TouchableOpacity>
           </View>


           </View>
          
          
          </View>
          <View style={{width:'100%',height:0.5,backgroundColor:'grey',marginBottom:15}}>
          </View>
          

         


            {/* top item  end*/}

           {/*   <Text onPress={()=> this.addKey()}>pree</Text> */}

         
            

        {/* post error */}

        {
          this.state.post_error
          ?
          <View style={{height:null,width:'90%',
          backgroundColor:'white',borderColor:'red',borderWidth:0.5,elevation:6,
          padding:15,margin:5,shadowOpacity:0.3}}>
            {
              this.state.post_text_error
              ?   <Text style={{color:colors.color_primary,
                fontWeight:'bold',margin:5}}>Please Enter Post Description.</Text>
              : <Text style={{color:colors.color_primary,
                fontWeight:'bold',margin:5}}>Please Enter one image/video .</Text>
            }
          

            <View style={{flexDirection:'row',
          flex:4,justifyContent:'space-around',alignItems:'center'}}>

            <Text style={{flex:3,fontSize:13}}>Your Post should contain description with only one Image / Video .Kindly input all to upload your post.</Text>
              <TouchableOpacity onPress={()=> this.setState({post_error:false})}>
                  <View style={{padding:10,justifyContent:'center',alignItems:'center',
                  backgroundColor:colors.color_primary,margin:5,elevation:3,borderRadius:10,marginLeft:9}}>
                  <Text style={{color:'white'}}>OK</Text>
                  </View>
              </TouchableOpacity>
          </View>
         

          </View>
          : null

        }





      

          

           {/* post error  end*/}


               {/* image video preview */}
               
           
       
         



                {/* image video preview  end*/}

               {/*
                <View style={{height:null,width:"90%",backgroundColor:'red',padding:20}}>
                          <VideoPlayer source={this.state.videoSource} 
                            style={{height:200,width:400}} />
                      </View>
            */}

         

            <FlatList
            style={{marginBottom:10,width:'100%'}}
            data={this.state.posts}
            ref={(ref) => this.homeList = ref}
            refreshing={this.state.refreshing}
            onRefresh={async () => {
             // await this.setState({refreshing: true, postsId: [0], radiusId: 0, followingId: 0});
              //this.getPosts();
              //console.log("Refreshing !!") 
            }}
            onEndReached={async () => {
             //ToastAndroid.show("No more posts found!!",ToastAndroid.SHORT)
            }}
           
            showsVerticalScrollIndicator={false}
            scrollEnabled={false}
            renderItem={(item,index) => this.renderItem(item,index)}
            keyExtractor={item => item.id}
          
           // onEndReachedThreshold={0.5}
            contentContainerStyle={{paddingBottom: 20}}
          />



    
            <Text>No more Post Found</Text>
          
           
                 
                 
                 
    
    
              
    
             
    
            </ScrollView>
    
            {this.state.loading_status &&
              <View style={[
                StyleSheet.absoluteFill,
                { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
              ]}>
              <PacmanIndicator color={colors.color_primary}/>
              </View>
            }
           
          </SafeAreaView>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
    enterpreneurUser: () => dispatch(enterpreneurUser()),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Home);



const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:colors.status_bar_color
    },
   
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(128,128,128,0.5)'
  }
  });


