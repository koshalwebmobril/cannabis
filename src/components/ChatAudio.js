import React, { useState,useEffect } from 'react';
import { colors, urls } from '../Globals';
import { withNavigation } from 'react-navigation';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { DrawerActions } from 'react-navigation';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import Snackbar from 'react-native-snackbar';

import Sound from 'react-native-sound';
import { Header } from 'react-navigation-stack';




export default class ChatAudio extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      selectedIndex:-1
    }
  }




     play(){
   

    this.props.playSound(this.props.item.audio,this.props.index)
  
   
  }
  

    stop(){
    //setSelectedIndex(-1)
    this.setState({selectedIndex:-1},()=>{

      this.props.stopSound(this.props.item.audio)
    })
   
  }

    resetIndex (){

    // console.log("called afrom parent component ",this.state.selectedIndex)
      
    
   }

  checkImage(){

    if(this.props.selectedAudio == this.props.index){
      return (
        <View style={{flexDirection:'row',alignItems:'center'}}>
        <TouchableOpacity onPress={()=>{ this.stop()}}>
        <Image source={require('../assets/pause-button.png')} style={{width:20,height:20}} resizeMode='contain'/>
        </TouchableOpacity>

        <Image source={require('../assets/audioplay.gif')} style={{width:90,height:50}} />


        </View>
      )
    }
    else{
      return(
        <View style={{flexDirection:'row',alignItems:'center'}}>
        <TouchableOpacity onPress={()=>{ this.play()}}>
        <Image source={require('../assets/play-button.png')} style={{width:20,height:20}} resizeMode='contain'/>
        </TouchableOpacity>

        <Image source={require('../assets/wave.png')} style={{width:60,height:50,marginLeft:5}} />

       

        </View>
    
      )
    }
  }


  
  render() {
     return (
        this.checkImage()
  
     )
  }
}

 const Chatd: () => React$Node = (props) => {

 let _sound = null
 const [selectedIndex, setSelectedIndex] = useState(-1);

  useEffect(() => {
  //  console.log("component did mount  ")
   

    return () => {
  //    console.log("componentWillUnmount")
     
        if(_sound != null){
          _sound.stop()
          _sound.release();
        }
   }
  },[])

  function play(){
   
    setSelectedIndex(props.index)
    props.playSound(props.item.audio)
   
  }
  

  function stop(){
    setSelectedIndex(-1)
    props.stopSound(props.item.audio)
  }

  function resetIndex(){
   // console.log("Function called frrom parent component")
    setSelectedIndex(-1)
  }



  return (
   
    
     
    this.state.selectedIndex == props.index 
    ?
    <TouchableOpacity onPress={()=>{ stop()}}>
    <Image source={require('../assets/pause-button.png')} style={{width:40,height:50}} resizeMode='contain'/>
    </TouchableOpacity>
    :
    <TouchableOpacity onPress={()=>{ play()}}>
    <Image source={require('../assets/play-button.png')} style={{width:40,height:50}} resizeMode='contain'/>
    </TouchableOpacity>

    




  

                            
);
};

// export default withNavigation(ChatAudio);


const styles = StyleSheet.create({

    container:{
       
        backgroundColor:'red',
        height:Header.HEIGHT,
        justifyContent:'center'
    }
 
});



