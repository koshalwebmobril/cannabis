import React from 'react';
import { colors, urls } from '../Globals';
import {Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform,TouchableWithoutFeedback
} from 'react-native';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
//import Video from 'react-native-video-controls';
import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import Snackbar from 'react-native-snackbar';
import { createImageProgress } from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import FastImage from 'react-native-fast-image'
import { ActivityIndicator } from 'react-native';
const MyImage = createImageProgress(FastImage);
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

 class PostNewsComponent extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      image_height:0,
      image_width:0,
      video_height:0,
      video_width:0,
      loadingImage:false,
      imageLoaded:false,
      key:false,
      news:'',
    }
  }


    componentDidMount(){
   
    }
    checkCondition(item){
        let img;
        if(item.like_flag == 1){
            img = require('../assets/p1.png')
        }else if (item.like_flag  == 2){
          img = require('../assets/p2.png')
        }else if (item.like_flag  == 3){
          img = require('../assets/p3.png')
        }
        else if (item.like_flag  == 4){
          img = require('../assets/p4.png')
        }else{
          img = require('../assets/p1.png')
        }
        return img;   
    };
       

    render(){ 
    return(
        <TouchableWithoutFeedback onPress={()=>{this.props.next()}} style={{}}style={{alignSelf:'center'}}>
       
            <View style={{width:'100%',height:170 ,marginLeft:6,flex:1,
           marginRight:6,
           backgroundColor:'black',
           marginVertical:20,
            borderColor:'black'}}>
  
            <Text style={{color:'white',fontWeight:'bold',margin:3}}>Title : 
                  <Text style={{color:'white',margin:3}}> {this.props.item().title.substring(0,45)}</Text>
            </Text>
  
  
             <ImageBackground
              source={{uri : urls.base_url + this.props.item().img}}
              resizeMode='cover'
              style={{height:'100%'}}>
              <View style={{flexDirection:'row',
              justifyContent:'space-between',
              alignItems:'center',margin:4,height:null}}>
  
              </View>
  
              <View style={{width:'55%',flexDirection:'row',alignItems:'center',
              justifyContent:'flex-start',marginLeft:6,top:-5}}>
  
  
  
                    <View style={{flexDirection:'row',width:'90%',alignItems:'center'}}>
                    <Image source={require('../assets/clock.png')}
                                style={{width:17,height:17,marginRight:6}} resizeMode='contain'/>
  
                    <Text style={{color:'white',fontSize:13}}>{this.props.item().created_at}</Text>
              
                  </View>
  
           </View>
  
  
             <Text style={{margin:5,color:'white',flex:1,fontWeight:'bold'
             }}></Text>
  
             <View style={{width:'100%',backgroundColor:'white',
             height:1,marginTop:5,marginBottom:5}}></View>
  
  
             <View style={{width:'100%',flexDirection:'row',alignItems:'center',
             justifyContent:'space-between',margin:6,flex:0.3}}>
                    <TouchableOpacity 
                    activeOpacity={0.6}
                    style={{flexDirection:'row',alignItems:'center',marginTop:0,width:80,height:35}}
                    onPress={()=> this.props.likePosts(this.props.newsid,1)}
                    onLongPress={()=> {
                    this.setState({news:  true})
                    }}
                    >
                   
                        <Image source={this.checkCondition(this.props.item())}
                        style={{height:25,width:25,
                        overflow:'hidden',borderRadius:12,marginLeft:10,marginBottom:5}}  />
  
                        <Text style={{color:'white',marginBottom:5}}>{this.props.item().likes_count} likes</Text>
 
                        </TouchableOpacity>
  {/* <Text style={{color:'black',zIndex:999,position:'absolute',fontSize:20}}onPress={()=> this.props.likePosts(this.props.newsid,1)} onLongPress={()=> {this.setState({news:  true})}}>hihid</Text> */}

                        <TouchableOpacity activeOpacity={0.6}
                     style={{flexDirection:'row',alignItems:'center'}}
                     onPress={()=> {}}
                    onLongPress={()=> {
                    }}>
                    
                    <Text style={{color:'white'}}>{this.props.item().comment_count} comments</Text>
  
                        <Image source={require('../assets/comment.png')}
                        style={{height:25,width:25,
                        overflow:'hidden',borderRadius:12,margin:15}}  />
  
                        
  
                        </TouchableOpacity>
  
                       
  
             </View>
  
                {
                    this.state.news
                    ?
                    <View style={{position:'absolute',
                    bottom:10,left:10,
                    backgroundColor:'white',
                    width:null,flexDirection:'row',
                     alignItems:'center',borderRadius:10,
                    padding:5,borderWidth:1}}>
      
                    <TouchableOpacity onPress={()=> {
                    //  this.likePosts(this.state.news[index].id,1)
                    this.props.likePosts(this.props.newsid,1)
                      this.setState({news:  false})
                    }}>
                    <Image source={require('../assets/p1.png')}
                    style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                    </TouchableOpacity>
                  
                    <TouchableOpacity onPress={()=> {
                    //   this.likePosts(this.state.news[index].id,2)
                    this.props.likePosts(this.props.newsid,2)
                      this.setState({news:  false})
                    }}>
                    <Image source={require('../assets/p2.png')}
                    style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                    </TouchableOpacity>
  
                    <TouchableOpacity onPress={()=> {
                    //   this.likePosts(this.state.news[index].id,3)
                    this.props.likePosts(this.props.newsid,3)
                      this.setState({news:  false})
                    }}>
                    <Image source={require('../assets/p3.png')}
                    style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                    </TouchableOpacity>
  
  
                    <TouchableOpacity onPress={()=> {
                    //   this.likePosts(this.state.news[index].id,4)
                    this.props.likePosts(this.props.newsid,4)
                      this.setState({news:  false})
                    }}>
                    <Image source={require('../assets/p4.png')}
                    style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>
                    </TouchableOpacity>
  
  
                    </View>
                    : null
                  }
             </ImageBackground>
  
              </View>
            
        </TouchableWithoutFeedback>
      )
    }
}



// withNavigation returns a component that wraps ChildComponent and passes in the
// navigation prop
export default withNavigation(PostNewsComponent);
const styles = StyleSheet.create({

  mainContainer: {
      width: Dimensions.get('window').width* 0.96,
      height:null,backgroundColor:'white',
      padding:0,borderWidth:0.1,elevation:9,
      alignSelf:'center',marginBottom:5,
      marginTop:5,marginLeft:2,marginRight:2,
      shadowOpacity:0.2
  },
  rowOne:{flexDirection:'row',justifyContent:'space-between'},
  rowTwo:{
    flexDirection:'row',
    justifyContent:'flex-start',alignItems:'center'
  },
  rowThree:{flexDirection:'row',width:'90%',alignItems:'center'},
  userPic:{
    height:40,width:40,
    overflow:'hidden',borderRadius:20,margin:10
  },
  nameText:{marginBottom:5,fontWeight:'bold'},
  dateImage:{width:17,height:17,marginRight:6},
  dateText:{color:'grey',fontSize:13},
  menuImage:{width:17,height:17,marginRight:6,flex:1},
  videoStyle:{width:'99%',height:'100%',alignSelf:'center'},
  playImageContainer:{position:'absolute',top:10,
  right:10,backgroundColor:colors.color_primary,padding:5
   },
   postImageStyle:{alignSelf:'center',width:'100%',
   overflow:'hidden',flex:1},
   playImage:{width:20,height:20},
divider:{width:'100%',backgroundColor:'grey',height:0.1},
rowFour:{flexDirection:'row',padding:3,alignItems:'center'},
rowFive:{flexDirection:'row',alignItems:'center'},
smileyImage:{height:25,width:25,
  overflow:'hidden',borderRadius:19,marginRight:5},
  dividerTwo:{width:'100%',backgroundColor:'grey',height:0.4},
rowSix:{padding:3,flexDirection:'row',alignItems:'center',width:'100%',justifyContent:'space-between'},
thumbUpStyle:{height:25,width:25,
  overflow:'hidden',borderRadius:12,margin:0},
  smileyContainer:{position:'absolute',
  bottom:30,left:5,
  backgroundColor:'white',
  width:null,flexDirection:'row',
   alignItems:'center',borderRadius:15,
  padding:5,borderWidth:0.4},
  rowSeven:{justifyContent:'center',alignItems:'center',marginLeft:5},
  likeText:{color:'grey',fontSize:9,margin:3},
  smileyImageStyle:{width:30,height:30},
  coolText:{color:'grey',fontSize:9,margin:3},

});
