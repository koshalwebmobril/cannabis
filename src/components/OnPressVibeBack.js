import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {StatusBar,Image,Text,Platform,ToastAndroid,Button} from 'react-native';

const OnPressVibeBack = (props) => {
    return(
        <>
<TouchableOpacity style={{width:50,height:40}} onPress={props.backs}>
            <Image style={{width: 25, height: 25,margin:10}} 
            source={require('../assets/left-arrow-white.png')}/>
</TouchableOpacity>
        </>
    );
}
export default OnPressVibeBack;