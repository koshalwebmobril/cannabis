import React from 'react';
import { colors, urls } from '../Globals';
import {Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { DrawerActions } from 'react-navigation';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
import Video from 'react-native-video-controls';
//import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import Snackbar from 'react-native-snackbar';




export default class MaterialTextInput extends React.Component {
  state = {
    text: this.props.defaultValue ? this.props.defaultValue : '',
    isFocus: false,
  };

	componentWillReceiveProps(nextProps) {
		// You don't have to do this check first, but it can help prevent an unneeded render
		if (nextProps.defaultValue !== this.props.defaultValue) {
			this.setState({ text: nextProps.defaultValue.toString() });
		}
	}

  getInputValue = () => this.state.text;

  render() {
    const { isFocus, text } = this.state;
    const showPlaceHolderTop = isFocus || text !== '';


    return (
    
    
        <TextField
          style={styles.inputText}
          autoCapitalize={this.props.autoCapitalize}
          ref={this.props.ref}
          autoCorrect={false}
          onFocus={() => this.setState({isFocus:true})}
          onBlur={() => this.setState({isFocus:false})}
          value={this.state.text}
          label={this.props.placeholder}
          secureTextEntry={this.props.secureTextEntry}
          blurOnSubmit={this.props.blurOnSubmit}
          keyboardType={this.props.keyboardType}
          returnKeyType={this.props.returnKeyType}
          textContentType={this.props.textContentType}
        //  onSubmitEditing={this.props.focus(this.props.textInput)}
          onSubmitEditing={this.props.onSubmit}
          placeholderTextColor='grey'
          onChangeText={this.props.onChangeText ? this.props.onChangeText : (text) => this.setState({ text })}
          editable={this.props.editable}
        />
     
    );

    // placeholder="Name"
    // keyboardType="default"
    // style={styles.nameInput}
    // error={false}
    // // focus={this.onChangeInputFocus}
    // ref={ref => this.nameInput = ref}
    // returnKeyType="done"
    // defaultValue={name ? name : ''}
    // onChangeText={(text) => this.handleNameChange(text)}

      //
    	// handleNameChange = (text) => {
    	// 	this.nameInput.setState({text});
    	// 	this.setState({name:text});
    	// }


  }
}

MaterialTextInput.defaultProps = {
  focus: () => {},
  style: {},
  placeholder: '',
  blurOnSubmit: false,
  returnKeyType: 'next',
  keyboardType: null,
  secureTextEntry: false,
  autoCapitalize: "none",
  textContentType: "none",
  defaultValue: '',
  editable: true,
};

const styles = StyleSheet.create({
  container: {
   
  },

  inputText: {
    color: 'grey',
    paddingVertical: 0,
    marginVertical: 3,
    fontSize: 15,
  },
});




// export const MaterialTextInput = React.forwardRef((props, ref) => {
//   return (
//     <View style={[styles.container, this.props.style]}>
//       <TextField
//         style={styles.inputText}
//         autoCapitalize={this.props.autoCapitalize}
//         ref={ref} // Change Here also
//         autoCorrect={false}
//         onFocus={() => this.setState({isFocus:true})}
//         onBlur={() => this.setState({isFocus:false})}
//         value={this.state.text}
//         label={this.props.placeholder}
//         secureTextEntry={this.props.secureTextEntry}
//         blurOnSubmit={this.props.blurOnSubmit}
//         keyboardType={this.props.keyboardType}
//         returnKeyType={this.props.returnKeyType}
//         textContentType={this.props.textContentType}
//       //  onSubmitEditing={this.props.focus(this.props.textInput)}
//         onSubmitEditing={this.props.onSubmit}
//         placeholderTextColor='grey'
//         onChangeText={this.props.onChangeText ? this.props.onChangeText : (text) => this.setState({ text })}
//         editable={this.props.editable}
//       />
//     </View>
//   )
// })
