import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { colors, urls } from '../Globals';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import FastImage from 'react-native-fast-image';
import Video from 'react-native-video-player';
import AsyncStorage from '@react-native-community/async-storage';
import AdvertiesmentImg from './AvertiesmentImg';

class PostAdvertiesment extends React.Component{
    constructor(props) {
      super(props)
      this.state = {
        image_height:0,
        image_width:0,
        video_height:0,
        video_width:0,
        loadingImage:false,
        user_id:''
      }
    }
_menu = null;

setMenuRef = ref => {
  this._menu = ref;
};

hideMenu = () => {
  this._menu.hide();
};

showMenu = () => {
  this._menu.show();
};

componentDidMount(){
    AsyncStorage.getItem('user_id').then((item) => {
        if (item) {
          this.setState({user_id:item})
          if(this.props.item.media_type == '1'){
            Image.getSize(urls.base_url + this.props.item.img, (width, height) => {
                var h_percentage = (windowWidth*100)/width
                var h_img = (height*h_percentage)/100
                if(width == 0){
                    this.setState({image_height:400,image_width:width})
                  } else {
          var h_percentage = (windowWidth*100)/width
          var h_img = (height*h_percentage)/100
          this.setState({image_height:h_img,image_width:width})
         }
         }, (error) => {
             //   console.log(`Couldn't get the image size: ${error.message}`);
                this.setState({image_height:400,image_width:'100%'})
               });
          }
        }
    })
}
myPostDelete=(item)=>{
    this._menu.hide();
                     var formData = new FormData();
                      formData.append('user_id',this.state.user_id);
                      formData.append('advert_id',item.id);
                       fetch(urls.base_url +'api/delete_advert', {
                       method: 'POST',
                       headers: {
                         'Accept': 'application/json',
                         'Content-Type': 'multipart/form-data',
                       },
                      body: formData
                     }).then((response) => response.json())
                           .then((responseJson) => {
                 
                    if(!responseJson.error){
                                    }
                  
                           }).catch((error) => {
                              console.log(error)
                           
                                  });
  }
  
  myPostEdit=(item)=>{
    this._menu.hide();
    var data={'post':item}
   
    this.props.navigation.navigate('EditAdvertiesement',{result:data})
  
  }
  nextPage(item) {
    if (item.media_type == 1) {
      let obj = {
        'image_url': urls.base_url + item.img,
        'item': item,
      };
      this.props.navigation.navigate("ImageView", {result: obj});
    } else if (item.media_type == 2) {
      let obj = {
        'video_url': item.video,
        'item': item,
      };
      this.props.navigation.navigate("VideoView", {result: obj});
    }
  }
  
  validURL(str) {
    var pattern = new RegExp(
      '^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$',
      'i',
    ); // fragment locator

    return !!pattern.test(str);
  }
  seeURL = (url) => {
    if (url.includes('https://')) {
    } else if (url.includes('http://')) {
    } else {
      url = 'http://' + url;
    }

    let f = Platform.select({
      ios: () => {
        Linking.openURL(url);
      },
      android: () => {
        Linking.openURL(url).catch((err) =>
          console.error('An error occurred', err),
        );
      },
    });

    f();
  };
myediteadvertiesement(){
     this._menu.hide();
}


render(){
  return (
 
    <View
      style={{
      width: '95%',
      padding: 6,
      marginBottom: 5,
      marginTop: 5,
      borderColor: '#999',
      borderWidth: 1,
      alignSelf: 'center',
    }}> 
     
     {
      this.state.user_id==this.props.item.user_id
      ?
    <View style={{zIndex:999,alignSelf:'flex-end',right:-10}}>
    <Menu
    ref={this.setMenuRef}
    button={
    <TouchableOpacity onPress={this.showMenu} style={{width:30,height:30,top:7}}>
    <Image source={require('../assets/menu-options.png')}
                    style={[styles.menuImage,{alignSelf:'center'}]} resizeMode='contain'/>
    </TouchableOpacity>
    } >
    <MenuItem onPress={ ()=>{
              Alert.alert(
              "Alert", 
              'Are you sure want to delete ?',
              [
                { text: "OK", onPress: this.props.mydelete},
                { text: "CANCEL", onPress: () => console.log("cancel Pressed") }
              ],
              { cancelable: false } 
               ) }
            } >Delete</MenuItem>
    <MenuItem onPress={() => {
         this.props.myedit()
         this.myediteadvertiesement()
    }
        }>Edit</MenuItem> 
    </Menu>
    </View>
      : null 
     } 

        <TouchableOpacity
       onPress={
        this.props.next
     } >
    <Text style={{marginBottom: 10, color: 'blue', fontWeight: 'bold'}}>
      Sponsored Content{' '}
    </Text>

    <View style={{width: '100%', height: null}}>
      {this.props.item.media_type == '2' ? (
        <Video
          video={{uri: urls.base_url + this.props.item.video}}
          resizeMode="stretch"
          thumbnail={{uri: urls.base_url + this.props.item.video_thumbnail}}
          style={{
            width: '100%',
            height: 170,
            alignSelf: 'center',
            marginTop: -7,
          }}
        />
      ) : (
           
    // <AdvertiesmentImg  myitem={urls.base_url + this.props.item.img}/>

        <View style={{width:'100%',height:this.state.image_height,marginHorizontal:50,borderColor:'black',borderWidth:2,alignSelf:'center'}}> 
        <Image source={{ uri: urls.base_url + this.props.item.img}} resizeMode="stretch" style={{height:'100%',width:'100%',alignSelf:'center'}}></Image> 
       </View>
       
      )}

      <Text style={{fontSize: 14, fontWeight: '800'}}>{this.props.item.name}</Text>
      <Text style={{fontSize: 12, color: 'grey'}}>
        {this.props.item.description}
      </Text>
      {this.props.item.url && this.validURL(this.props.item.url) ? (
        <Text
          onPress={() => this.seeURL(this.props.item.url)}
          style={{fontSize: 10, color: 'blue'}}>
          {this.props.item.url}
        </Text>
      ) : null}
    </View>
     </TouchableOpacity>
  </View>

 
  )
      }
}
export default PostAdvertiesment;

const styles = StyleSheet.create({
    
    menuImage:{width:17,height:17,marginRight:6,flex:1},
  });
  