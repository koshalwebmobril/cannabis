import React, { Component } from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity } from 'react-native';
import Markdown from 'react-native-easy-markdown';

export default class ChatTextInput extends Component {

  state = { text: 'type here ...' };
  onClick = e => {
    this.textInput.focus();
  };


  render() {
    return (
      <View style={styles.container}>
        <TextInput
          ref={ref => (this.textInput = ref)}
          style={{ position: 'absolute', left: -1000, top: -1000 }}
          onChangeText={text => this.setState({ text })}
        />
        <TouchableOpacity onPress={this.onClick}>
          <Markdown>{this.state.text}</Markdown>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});