import React from 'react';
import { colors, urls } from '../Globals';
import {Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { addUser, changeUser } from '../actions/actions';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
//import Video from 'react-native-video';
//import Video from 'react-native-video-controls';
import Video from 'react-native-video-player'
import RBSheet from "react-native-raw-bottom-sheet";
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import Snackbar from 'react-native-snackbar';
import { Item } from 'native-base';
import { createImageProgress } from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import FastImage from 'react-native-fast-image'
import { ActivityIndicator } from 'react-native';
const MyImage = createImageProgress(FastImage);
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

 class PostComponent extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      image_height:0,
      image_width:0,
      video_height:0,
      video_width:0,
      loadingImage:false,
      imageLoaded:false,
      key:false
    }
  }


  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };

  // shouldComponentUpdate(nextProps) {
  //   //const { isSelected } = this.props;
  //     //console.log(nextProps)
  //     //return this.props.posts.likes_count !== nextProps.posts.likes_count;
  // }

    showReaction(index){
        //this.props.posts[index].smiley = true
                 // this.setState({posts:  this.state.posts})

                 let ids = [...this.props.posts];     // create the copy of state array
                  ids[index].smiley  = true                  //new value
                this.props.setPosts(ids)          //update the value
      }

      hideReaction(index){
                 let ids = [...this.props.posts];     // create the copy of state array
                  ids[index].smiley  = false                  //new value
                this.props.setPosts(ids)
      }

      next(){
         //this.props.navigation.navigate("SellerProfile")
          let user_id = this.props.posts[this.props.index].user_id

          let obj={
            'user_id':user_id
          }
          if(user_id == this.props.userProps.user.user_id){
            
            this.props.navigation.navigate("MyProfile")

          }
          else{
            this.props.navigation.navigate("SellerProfile",{result:obj})
          }
         
      }

      edit = () => {

        if( this.props.posts[this.props.index].user_id == this.props.userProps.user.user_id){
            this._menu.hide();
            let post = this.props.posts[this.props.index]
            let obj={
              'post':post
            }
              this.props.navigation.navigate("EditPost",{result:obj})

       }
       else{
         this._menu.hide();
         ToastAndroid.show("This Post had been Posted By Another User",ToastAndroid.SHORT)
       }

      }


      change(like_id){



       //// console.log("Click")
       // this.props.posts[this.props.index].likes_count = 6
        this.props.handler(this.props.posts[this.props.index].id,like_id)
      }


      deletePost =()=>{
        //console.log(this.props.userProps.user.user_id)

        if( this.props.posts[this.props.index].user_id == this.props.userProps.user.user_id){
           this._menu.hide();

        this.props.deletePost(this.props.posts[this.props.index].id)

        }
        else{
          this._menu.hide();
          ToastAndroid.show("This Post had been Posted By Another User",ToastAndroid.SHORT)

        }

      }

     _checkCondition(){
        let img;
        if(this.props.posts[this.props.index].like_flag == 1){
            img = require('../assets/p1.png')
        }else if (this.props.posts[this.props.index].like_flag == 2){
          img = require('../assets/p2.png')
        }else if (this.props.posts[this.props.index].like_flag == 3){
          img = require('../assets/p3.png')
        }
        else if (this.props.posts[this.props.index].like_flag == 4){
        img = require('../assets/p4.png')
      }
      else if (this.props.posts[this.props.index].like_flag == 5){
        img = require('../assets/p5.png')
      }else{
          img = require('../assets/p1.png')
        }


        return img;
    };


    _setSmileyText = () =>{

      if(this.props.posts[this.props.index].like_flag == 1){
         return (
           <Text style={{fontSize:11,marginLeft:-4,marginRight:4,color:'blue'}}>(Like)</Text>
         )
      }else if (this.props.posts[this.props.index].like_flag == 2){
        return (
          <Text style={{fontSize:11,marginLeft:-4,marginRight:4,color:'red'}}>(Cool)</Text>
        )
      }else if (this.props.posts[this.props.index].like_flag == 3){
        return (
          <Text style={{fontSize:11,marginLeft:-4,marginRight:4,color:'#CDCD00'}}>(Awesome)</Text>
        )
      }
      else if (this.props.posts[this.props.index].like_flag == 4){
        return (
          <Text style={{fontSize:11,marginLeft:-4,marginRight:4,color:'#CDCD00'}}>(Rock)</Text>
        )
     }
     else if (this.props.posts[this.props.index].like_flag == 5){
      return (
        <Text style={{fontSize:11,marginLeft:-4,marginRight:4,color:'#CDCD00'}}>( Fantastic)</Text>
      )
    } else{
          return (
            <Text style={{fontSize:11,marginLeft:-4,marginRight:4,color:'blue'}}>(Like)</Text>
          )
      }




    }


    _setLikeText(item){



      let text = '';
      if(item.likes_count == 0){
        text = '0 Likes'
      }
      else if(item.likes_count >= 2){
        if(item.like_flag >= 1){
          //means i liked this
            text = "You and " + (parseInt(item.likes_count) - 1) + ' others liked this'
        }
        else{
          //means i haven't reacted to this
          text = item.liked_by+ " and " + (parseInt(item.likes_count) - 1) + ' others liked this'
        }
      }

      // else if(item.likes_count == 1){
      //     //means only one reacted
      //     if(item.like_flag > 1){
      //       //means i liked this
      //         text = "You liked this"
      //     }
      //     else{
      //       //means i haven't reacted to this
      //       text = item.liked_by+ ' liked this'
      //     }
      // }
      else if(item.likes_count == 1){
        //means only one reacted
        if(item.like_flag >= 1){
          //means i liked this
            text = "You liked this"
        }
        else{
          //means i haven't reacted to this
          text = item.liked_by+ ' liked this'
        }
    }

      return text

    }


    checkImageLoading = () =>{
      return(
        
        <View   style={[styles.postImageStyle,{height:this.state.image_height + 0 }]}>
          <FastImage 
          source={{uri: (urls.base_url+ this.props.posts[this.props.index].post_img)}}
          resizeMode={FastImage.resizeMode.stretch }
          onLoadEnd={()=> {
           // console.log('___________&&&&&&$$$$$$$$$$@@@@@@@@@@@@@@@')
            this.setState({imageLoaded:true})
            }}
          onLoadStart={e => this.setState({ imageLoaded: false })}
         style={[styles.postImageStyle,{height:this.state.image_height + 0 }]}
         />
         {
           this.state.imageLoaded == false ?
           <View   style={{alignItems:'center',justifyContent:'center',backgroundColor:'#eee',position:'absolute',top:0,bottom:0,left:0,right:0}}>
             <View> 
               <ActivityIndicator />
               <Text > Please wait while image loads</Text>
             </View>
           </View>
           : null

         }
         </View>


        
      )
     
    }


    componentDidMount(){
      if(this.props.posts[this.props.index].media_type == '1'){
      // // console.log('Setting height width of the image')
        // Image.getSize(urls.base_url+ this.props.posts[this.props.index].post_img, (width, height) => {console.log(height+'----'+width)});
         Image.getSize(urls.base_url+ this.props.posts[this.props.index].post_img, (width, height) => {
           var h_percentage = (windowWidth*100)/width
           var h_img = (height*h_percentage)/100
           if(width == 0){
            this.setState({image_width:width, image_height:400}) 
     
          } else {
     
     var h_percentage = (windowWidth*100)/width
     var h_img = (height*h_percentage)/100
    // console.log(`height is ${h_percentage}`);
     this.setState({image_width:width, image_height:h_img}) 
    }

         }, (error) => {
          // console.log(`Couldn't get the image size: ${error.message}`);
           this.setState({image_width:0, image_height:0})
         });
        
      }

     
    }
 
  

    render(){ 
    // console.log(this.props.posts[this.props.index])
        return(
            <View style={styles.mainContainer}>
            <TouchableOpacity onPress={()=>{
              if( this.props.posts[this.props.index].smiley == true){
               this.hideReaction(this.props.index)
               }
             }}>
            <View style={styles.rowOne}>
            <View style={styles.rowTwo}>
                <TouchableOpacity style={{width:'auto'}} onPress={()=>{ this.next() }} >
                    <FastImage source={{uri:urls.base_url+this.props.posts[this.props.index].user_pic}}
                    style={styles.userPic}  />
                    </TouchableOpacity>

                    <View style={{marginLeft:8}}>
                    <Text onPress={()=>{  this.next()}} 
                    style={styles.nameText}>{this.props.posts[this.props.index].user_name}</Text>
                    <View style={styles.rowThree}>
                    <FastImage source={require('../assets/clock.png')}
                      style={styles.dateImage} resizeMode={FastImage.resizeMode.contain}/>
                          <Text style={styles.dateText}>{this.props.posts[this.props.index].created_at}</Text>
                    </View>
                    </View>
            </View>


            {
              this.props.posts[this.props.index].user_id == this.props.userProps.user.user_id
              ?
             <View style={{marginTop:10}}>
                  <Menu
                  ref={this.setMenuRef}
                  button={
                  <TouchableOpacity onPress={this.showMenu}>
                  <Image source={require('../assets/menu-options.png')}
                                  style={styles.menuImage} resizeMode='contain'/>
                  </TouchableOpacity>
                  }>
                  <MenuItem onPress={this.deletePost}>Delete</MenuItem>
                  <MenuItem onPress={this.edit}>Edit</MenuItem>
                  </Menu>
                </View>
              : null
               }
            </View>
            <Text style={{margin:5}}>{this.props.posts[this.props.index].description}</Text>
                <TouchableOpacity onPress={() => this.props.seeImage(this.props.posts[this.props.index])}>
                <View style={[
                    { width: '100%'},
                    { height:this.props.posts[this.props.index].media_type == '2' ?
                      Dimensions.get('window').height *0.25
                     : 'auto',
                      margin: 10,alignSelf:'center'
                  }]} >
                 {
                  this.props.posts[this.props.index].media_type == '2'
                    ?
                    <Video
                     video={{uri:urls.base_url+ this.props.posts[this.props.index].post_video}}
                     resizeMode='cover'
                     selectedVideoTrack={{
                      type: "resolution",
                      value: 480
                    }}
                    thumbnail={{uri: this.props.posts[this.props.index].video_thumbnail == null? '':urls.base_url+ this.props.posts[this.props.index].video_thumbnail}}
                    style={styles.videoStyle} />
                    :
                    // <FastImage source={{uri:urls.base_url+ this.props.posts[this.props.index].post_img}}
                    //  resizeMode = {FastImage.resizeMode.stretch}
                    //  style={[styles.postImageStyle,{height:this.state.image_height + 0 }]} 
                    //  />
                    this.checkImageLoading()
                  }

                  {
                    this.props.posts[this.props.index].media_type == '2'
                    ?
                    <View style={styles.playImageContainer}>
                    <Image source={require('../assets/play.png')}
                    style={styles.playImage} resizeMode='stretch'/>
                    </View>
                    :null
                  }
                  </View>
                  </TouchableOpacity>

                  <View style={styles.divider}></View>
                   <View style={styles.rowFour}>
                    {
                    this.props.posts[this.props.index].likes_count == 0
                      ?  null
                      :
                      <View style={styles.rowFive}>
                      <FastImage source={this._checkCondition()}
                        style={styles.smileyImage}  />
                          {this._setSmileyText() }
                      </View>
                    }
                    <Text>{this._setLikeText(this.props.posts[this.props.index])}</Text>
                   </View>
                    </TouchableOpacity>
                    <View style={styles.dividerTwo}></View>
                    <View style={styles.rowSix}>
                    <TouchableOpacity activeOpacity={0.6}
                        onLongPress={()=> {this.showReaction(this.props.index)}}
                        onPress={()=> this.change(1)}>
                      <FastImage source={require('../assets/thumb-up.png')}
                        resizeMode={FastImage.resizeMode.contain}
                        style={styles.thumbUpStyle}  />
                    </TouchableOpacity>
                    <Text style={{color:'grey'}}>{this.props.posts[this.props.index].post_comment_count} {
                      this.props.posts[this.props.index].post_comment_count > 1 ? 'comments' : 'comment'
                     }</Text>
                    </View>

                    {
                      this.props.posts[this.props.index].smiley
                      ?
                      <View style={styles.smileyContainer}>
                      <TouchableOpacity style={styles.rowSeven}
                       onPress={()=> {
                        this.change(1)
                        this.hideReaction(this.props.index)
                      }}>
                      <FastImage source={require('../assets/p1.png')}
                      style={styles.smileyImageStyle} resizeMode={FastImage.resizeMode.contain}/>
                      <Text style={styles.likeText}>Like</Text>
                      </TouchableOpacity>

                      <TouchableOpacity style={{justifyContent:'center',alignItems:'center',marginLeft:10}}
                      onPress={()=> {
                       this.change(2)
                        this.hideReaction(this.props.index)
                      }}>
                    <FastImage source={require('../assets/p2.png')}
                      style={styles.smileyImageStyle} resizeMode={FastImage.resizeMode.contain}/>
                      <Text style={styles.coolText}>Cool</Text>
                      </TouchableOpacity>

                      <TouchableOpacity style={{justifyContent:'center',alignItems:'center',marginLeft:5}}
                       onPress={()=> {
                      this.change(3)
                        this.hideReaction(this.props.index)
                      }}>
                     <FastImage source={require('../assets/p3.png')}
                      style={styles.smileyImageStyle} resizeMode={FastImage.resizeMode.contain}/>
                      <Text style={styles.coolText}>Awesome</Text>
                      </TouchableOpacity>


                      <TouchableOpacity style={{justifyContent:'center',alignItems:'center',marginLeft:5}}
                      onPress={()=> {
                       this.change(4)
                        this.hideReaction(this.props.index)
                      }}>
                     <FastImage source={require('../assets/p4.png')}
                      style={styles.smileyImageStyle} resizeMode={FastImage.resizeMode.contain}/>
                      <Text style={styles.coolText}>Rock</Text>
                      </TouchableOpacity>

                      <TouchableOpacity style={{justifyContent:'center',alignItems:'center',marginLeft:5}}
                      onPress={()=> {
                        this.change(5)
                         this.hideReaction(this.props.index)
                       }}>
                      <FastImage source={require('../assets/p5.png')}
                       style={styles.smileyImageStyle} resizeMode={FastImage.resizeMode.contain}/>
                       <Text style={styles.coolText}>Fantastic</Text>
                       </TouchableOpacity>
                      </View>
                        : null
                    }
          </View>
        )
    }
}



// withNavigation returns a component that wraps ChildComponent and passes in the
// navigation prop
export default withNavigation(PostComponent);
const styles = StyleSheet.create({

  mainContainer: {
      width: Dimensions.get('window').width* 0.96,
      height:null,backgroundColor:'white',
      padding:0,borderWidth:0.1,elevation:9,
      alignSelf:'center',marginBottom:5,
      marginTop:5,marginLeft:2,marginRight:2,
      shadowOpacity:0.2
  },
  rowOne:{flexDirection:'row',justifyContent:'space-between'},
  rowTwo:{
    flexDirection:'row',
    justifyContent:'flex-start',alignItems:'center'
  },
  rowThree:{flexDirection:'row',width:'90%',alignItems:'center'},
  userPic:{
    height:40,width:40,
    overflow:'hidden',borderRadius:20,margin:10
  },
  nameText:{marginBottom:5,fontWeight:'bold'},
  dateImage:{width:17,height:17,marginRight:6},
  dateText:{color:'grey',fontSize:13},
  menuImage:{width:17,height:17,marginRight:6,flex:1},
  videoStyle:{width:'99%',height:'100%',alignSelf:'center'},
  playImageContainer:{position:'absolute',top:10,
  right:10,backgroundColor:colors.color_primary,padding:5
   },
   postImageStyle:{alignSelf:'center',width:'100%',
   overflow:'hidden',flex:1},
   playImage:{width:20,height:20},
divider:{width:'100%',backgroundColor:'grey',height:0.1},
rowFour:{flexDirection:'row',padding:3,alignItems:'center'},
rowFive:{flexDirection:'row',alignItems:'center'},
smileyImage:{height:25,width:25,
  overflow:'hidden',borderRadius:19,marginRight:5},
  dividerTwo:{width:'100%',backgroundColor:'grey',height:0.4},
rowSix:{padding:3,flexDirection:'row',alignItems:'center',width:'100%',justifyContent:'space-between'},
thumbUpStyle:{height:25,width:25,
  overflow:'hidden',borderRadius:12,margin:0},
  smileyContainer:{position:'absolute',
  bottom:30,left:5,
  backgroundColor:'white',
  width:null,flexDirection:'row',
   alignItems:'center',borderRadius:15,
  padding:5,borderWidth:0.4},
  rowSeven:{justifyContent:'center',alignItems:'center',marginLeft:5},
  likeText:{color:'grey',fontSize:9,margin:3},
  smileyImageStyle:{width:30,height:30},
  coolText:{color:'grey',fontSize:9,margin:3},












});
