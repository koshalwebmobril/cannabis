import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const AdvertiesmentImg =(props)=>{
const [img_height,setImag_height]=useState(0)
const [img_width,setImg_width] = useState(0);

useEffect(() => {
    Image.getSize(props.myitem, (width, height) => {
       
        var h_percentage = (windowWidth*100)/width
        var h_img = (height*h_percentage)/100
        if(width == 0){
            setImag_height(400)
            setImg_width(width)
       } else {
  
  var h_percentage = (windowWidth*100)/width
  var h_img = (height*h_percentage)/100
//  console.log(`height is ${h_percentage}`);
  setImag_height(h_img)
  setImg_width(width)
 
 }

      }, (error) => {
       // console.log(`Couldn't get the image size: ${error.message}`);
        setImag_height(0)
        setImg_width(0)
      });
     }, []);

  return (
  
       <TouchableOpacity style={{width:'100%',height:img_height,marginHorizontal:50,borderColor:'black',borderWidth:2,alignSelf:'center'}}> 
           <Image source={{ uri: props.myitem }} resizeMode="stretch" style={{height:'100%',width:'100%',alignSelf:'center'}}></Image> 
       </TouchableOpacity>

  );
}
export default AdvertiesmentImg;
